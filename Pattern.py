import pandas as pd
import re # amazing text cleaning for decode issues..
from ftfy import fix_text
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import time
from scipy.sparse import csr_matrix
import sparse_dot_topn.sparse_dot_topn as ct
import pyspark
from deltalake import DeltaTable


def ngrams(string, n=3):
    string = str(string)
    string = fix_text(string) # fix text
    string = string.encode("ascii", errors="ignore").decode() #remove non ascii chars
    string = string.lower()
    chars_to_remove = [")","(",".","|","[","]","{","}","'"]
    rx = '[' + re.escape(''.join(chars_to_remove)) + ']'
    string = re.sub(rx, '', string)
    string = string.replace('&', 'and')
    string = string.replace(',', ' ')
    string = string.replace('-', ' ')
    string = string.title() # normalise case - capital at start of each word
    string = re.sub(' +',' ',string).strip() # get rid of multiple spaces and replace with a single
    string = ' '+ string +' ' # pad names for ngrams...
    string = re.sub(r'[,-./]|\sBD',r'', string)
    ngrams = zip(*[string[i:] for i in range(n)])
    return [''.join(ngram) for ngram in ngrams]


def awesome_cossim_top(A, B, ntop, lower_bound=0):
    # force A and B as a CSR matrix.
    # If they have already been CSR, there is no overhead
    A = A.tocsr()
    B = B.tocsr()
    M, _ = A.shape
    _, N = B.shape

    idx_dtype = np.int32

    nnz_max = M * ntop

    indptr = np.zeros(M + 1, dtype=idx_dtype)
    indices = np.zeros(nnz_max, dtype=idx_dtype)
    data = np.zeros(nnz_max, dtype=A.dtype)

    ct.sparse_dot_topn(
        M, N, np.asarray(A.indptr, dtype=idx_dtype),
        np.asarray(A.indices, dtype=idx_dtype),
        A.data,
        np.asarray(B.indptr, dtype=idx_dtype),
        np.asarray(B.indices, dtype=idx_dtype),
        B.data,
        ntop,
        lower_bound,
        indptr, indices, data)

    return csr_matrix((data, indices, indptr), shape=(M, N))

def get_matches_df(sparse_matrix, name_vector, top):
    non_zeros = sparse_matrix.nonzero()

    sparserows = non_zeros[0]
    sparsecols = non_zeros[1]

    if top:
        nr_matches = top
    else:
        nr_matches = sparsecols.size

    left_side = np.empty([nr_matches], dtype=object)
    right_side = np.empty([nr_matches], dtype=object)
    similairity = np.zeros(nr_matches)

    for index in range(0, nr_matches):
        left_side[index] = name_vector[sparserows[index]]
        right_side[index] = name_vector[sparsecols[index]]
        similairity[index] = sparse_matrix.data[index]

    return pd.DataFrame({'left_side': left_side,
                         'right_side': right_side,
                         'similairity': similairity})
from deltalake import DeltaTable
def tfidf_dedupe(inputDeltaTablepath, columnName):
    #PandasDF = DeltaTable(inputDeltaTablepath).to_pandas()
    DF = DeltaTable(inputDeltaTablepath).to_pandas()
    # PandasDF.insert(0, 'Row_ID', range(1, 1 + len(PandasDF)))
    # PandasDF.set_index('Row_ID')
    # print(PandasDF)
    PandasDF = DF[[columnName]]
    # Data = spark.read.format("delta").load(inputDatasetLocation)
    # PandasDF = Data.toPandas()
    a = len(PandasDF)
    #print(a)
    #print(PandasDF)
    #The code to generate the matrix of TF-IDF values for each is shown below.
    org_names = PandasDF[columnName].unique()
    #print(len(org_names))
    vectorizer = TfidfVectorizer(min_df=1, analyzer=ngrams)
    #print('Vecorizing completed...')
    tf_idf_matrix = vectorizer.fit_transform(org_names)

    matches = awesome_cossim_top(tf_idf_matrix, tf_idf_matrix.transpose(), 10, 0.7)
    #print(matches)
    matches_df = get_matches_df(matches, org_names, len(org_names))
    #print(matches_df)
    matches_df = matches_df[matches_df['similairity'] < 0.99999] # Remove all exact matches

    matches_df1 = matches_df.sort_values(['similairity'], ascending=False)

    matchedcount = matches_df1['left_side'].value_counts()
    #print("matchedcount",matchedcount)
    #print(type(matchedcount))
    import json
    if matchedcount.empty == False:
        top10matchedcount1 = matchedcount.nlargest(5, keep='all')
        top10matchedcount = top10matchedcount1.head()
        top10matchedcountDF = pd.DataFrame({'List': top10matchedcount.index, 'Count': top10matchedcount.values})
        result = top10matchedcountDF.to_json(orient="values")
        parsed = json.loads(result)
        patternJson = {"Pattern": parsed}
        patternJson = json.dumps(patternJson)  # , indent=4)
        print("patternJson", patternJson)

    else:
        patternJson = {"Pattern": "N/A"}
        patternJson = json.dumps(patternJson)
        print("patternJson", patternJson)
    return patternJson
    #     parsed = json.loads(result)
    #     patternJson = json.dumps(parsed, indent=4)
    #     print("patternJson",patternJson)
    # else:
    #     print("[Patterns is not applicable to selected column]")

#tfidf_dedupe("C:\\Users\\kisho\\PycharmProjects\\DataWrangling\\Sampling_DT\\R_1_newPSNAN_s01_3_1", "NAME")