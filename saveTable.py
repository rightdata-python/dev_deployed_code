#importing required files and packages
from fastapi import FastAPI
import uvicorn
from typing import List, Optional
from pydantic import BaseModel
from properties import path

app = FastAPI()

class saveTableRequest(BaseModel):
    inputTable: str
    outputTable: str

@app.post("/saveTableAPI")
#calling the functions with given inputs
async def saveTableAPI(sr:saveTableRequest):
    Result = saveTable(sr.inputTable,sr.outputTable)
    return {f"{Result}"}


import pyspark
from pyspark.sql.functions import lit, col, isnan, count
#from properties import path
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def saveTable(inputTable, outputTable):
    print("Reading Universal data")
    #InputTableUniverse = path + "/" + inputTable
    Data = spark.read.format("delta").load(inputTable)
    #columnNames = Data.columns
    print("Column names in Delta table: ", Data.columns)
    #path ="/home/ubuntu/warehouse/DELTA_WAREHOUSE/uat.db"
    outputDeltaTablepath = path + "/" + outputTable
    Data.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
    return "Successfully saved the new Delta Table"




