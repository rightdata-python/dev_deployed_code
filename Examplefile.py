# import concurrent.futures
# import numpy as np
#
# # with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
# #     from itertools import repeat
# #
# #     cols = [1,2,3,4,5,6]
# #     for result in executor.map(np.power, repeat(cols)):
# #         print(result)
# #
#
# #def main():
# with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
#
#         cols = [1,2,3,4,5,6]
#         #with tqdm(total=len(cols)) as progress:
#         results = executor.map(np.power, cols)
#         for result in results:
#             print(result)
#         # imputed_df6 = pd.concat([imputed_df6,result], axis=1)
#         # imputed_df6.head()
#         # print(imputed_df6.columns.tolist())
#         # #print( 'Total features imputed ' + str(len(imputed_df6.columns.tolist())) + 'out of ' + str(len(remaining_cols)))
#         # print( 'Total features imputed ' + str(len(imputed_df6.iloc[:,3:].columns.tolist())))
#
# # if __name__ == '__main__':
# #     result = main()
# #     print(result)
#
# import concurrent.futures
#
# nums = [1,2,3,4,5,6,7,8,9,10]
#
# def f(x):
#     return x * x
# def main():
#     # Make sure the map and function are working
#     print([val for val in map(f, nums)])
#
#     # Test to make sure concurrent map is working
#     with concurrent.futures.ProcessPoolExecutor() as executor:
#         print([val for val in executor.map(f, nums)])
#
# if __name__ == '__main__':
#     main()
#
#
import pandas as pd
# from sklearn.preprocessing import OrdinalEncoder
# import numpy as np
# from pyspark.sql.functions import lit, rand, when
# import pyspark
# import json
# #from properties import path
# path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
# from pyspark.sql.functions import lit, col,isnan, when, count, mean as _mean, stddev as _stddev
# spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
#      .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
#      .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
#      .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
#      .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
#      .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
#      .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
#      .enableHiveSupport() \
#      .getOrCreate()
# print("spark session Started")

def iqr_for_all_columns_pandas(fileLocation,columnvalues):
        #reading csv from fileLocation as pandas dataframe
        df = pd.read_csv(fileLocation)

        df1 = df.copy()
        df = df._get_numeric_data()
        columnvalues = []
        # q1 = df[columnname].quantile(0.25)
        # q3 = df[columnname].quantile(0.75)
        q1 = df.quantile(0.25)
        q3 = df.quantile(0.75)

        iqr = q3 - q1

        lower_bound = q1 - (1.5 * iqr)
        upper_bound = q3 + (1.5 * iqr)

        print(lower_bound)
        print(upper_bound)
        # df_no_outlier = iqr[(iqr.col_vals>lower_bound)&(iqr.col_vals<upper_bound)]
        # df.loc[df[columnname] < lower_bound, columnname] = lower_bound
        # df.loc[df[columnname] > upper_bound, columnname] = upper_bound
        for col in columnvalues:
            for i in range(0, len(df[col])):
                if df[col][i] < lower_bound[col]:
                    df[col][i] = lower_bound[col]

                if df[col][i] > upper_bound[col]:
                    df[col][i] = upper_bound[col]

        for col in columnvalues:
            df1[col] = df[col]
        print("-------------------", df1)
        print(df1.columns)
        print("finaldf:", df1)

iqr_for_all_columns_pandas("C:\\Users\\publicschoolsample.csv", "FT_TEACHER")
