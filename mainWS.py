#importing required files and packages
from fastapi import FastAPI
import uvicorn
from typing import List, Optional
from pydantic import BaseModel
from fastapi.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware
from starlette.websockets import WebSocket
#####APIS#######
from samplingDTRequest import samplingDTRequestdef
from sampleValidationRequest import samplingValidationRequestdef
from columnCategoryCountPaginationRequest_1 import columnCategoryCountPaginationdef
from listOfCategories import listOfCategoriesdef
from deleteColumn import deleteColumndef
from customRemove import removeMissingdef
from changeDataType import changeDataTypedef
from RenameColumnName import renameColumnNamedef
from splitColumn import SplitbyDelimiterdef
from replaceMissing import replaceMissmatchdef
from CountOfMissingRequest import CountOfMissingdef
from columnDetails import columnDetailsdef
from masking import maskingreq
from capping_IQR import iqr_for_all_columns
from ordinal_encoding import ordinal_function
#assigning FastAPI as app
ws_clients = []
app = FastAPI()

def get_ws_clients():
    return ws_clients

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"]
)

#path = "/home/ubuntu/warehouse/DELTA_WAREHOUSE/uat.db"
################Sampling################
##################################3####
class SamplingRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    samplingMethod: str
    sampleSize: int
@app.post("/SamplingDTRequestAPI")
#calling the functions with given inputs
async def SamplingRequestAPI(sr:SamplingRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("Sampling process started") for ws in clients])
    Result = samplingDTRequestdef(sr.projectId,sr.reciepeId,sr.sessionId,sr.inputStepId, sr.currentStepId,sr.inputResultId, sr.samplingMethod, sr.sampleSize)
    await asyncio.wait([ws.send_text("Sampling process ended!") for ws in clients])
    return {f"{Result}"}
######SampleValidataion#############
####################################
class SamplingValidationRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    universeInputStepId: str
    sampleInputStepId: str
    currentStepId: Optional[str]
    universeInputResultId: str
    sampleInputResultId: str
@app.post("/SamplingValidationRequestAPI")
#calling the functions with given inputs
async def SamplingValidationRequestAPI(svr:SamplingValidationRequest):
    Result = samplingValidationRequestdef(svr.projectId,svr.reciepeId,svr.sessionId,svr.universeInputStepId, svr.sampleInputStepId, svr.currentStepId,svr.universeInputResultId, svr.sampleInputResultId)
    return {f"{Result}"}
###################columnCategoryCountPagination#############
#############################################################
class columnCategoryCountPaginationRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str

@app.post("/ColumnCategoriesCountPaginationRequestAPI")
# calling the functions with given inputs
async def ColumnCategoriesCountPaginationRequestAPI(ccp:columnCategoryCountPaginationRequest):
    Result = columnCategoryCountPaginationdef(ccp.projectId,ccp.reciepeId,ccp.sessionId,ccp.inputStepId, ccp.currentStepId,ccp.inputResultId)
    return {f"{Result}"}
################List of Categories############################
##############################################################
class listOfCategoriesRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str

@app.post("/listOfCategoriesRequestAPI")
# calling the functions with given inputs
async def listOfCategoriesRequestAPI(ccr:listOfCategoriesRequest):
    Result = listOfCategoriesdef(ccr.projectId,ccr.reciepeId,ccr.sessionId,ccr.inputStepId, ccr.currentStepId,ccr.inputResultId)
    return {f"{Result}"}
###########DeleteColumn############################
#################################################
class deleteColumnRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnNames: List[dict]
@app.post("/deleteColumnRequestAPI")
# calling the functions with given inputs
async def deleteColumnRequestAPI(dcr:deleteColumnRequest):
    Result = deleteColumndef(dcr.projectId,dcr.reciepeId,dcr.sessionId,dcr.inputStepId, dcr.currentStepId,dcr.inputResultId,dcr.columnNames)
    return {f"{Result}"}
################Count of missing values##########
################################################
class CountOfMissingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
@app.post("/CountOfMissingRequestAPI")
# calling the functions with given inputs
async def CountOfMissingRequestAPI(cmr:CountOfMissingRequest):
    Result = CountOfMissingdef(cmr.projectId,cmr.reciepeId,cmr.sessionId,cmr.inputStepId, cmr.currentStepId,cmr.inputResultId)
    return {f"{Result}"}
#################RemoveMissMatch##############
#############################################
class removeMissingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
@app.post("/removeMissingRequestAPI")
# calling the functions with given inputs
async def removeMissingRequestAPI(rmm:removeMissingRequest):
    Result = removeMissingdef(rmm.projectId,rmm.reciepeId,rmm.sessionId,rmm.inputStepId, rmm.currentStepId,rmm.inputResultId,rmm.columnName)
    return {f"{Result}"}
##################replaceMissing################
################################################
class replaceMissmatchRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    customValue: str
@app.post("/replaceMissmatchRequestAPI")
# calling the functions with given inputs
async def replaceMissmatchRequestAPI(rmm:replaceMissmatchRequest):
    Result = replaceMissmatchdef(rmm.projectId,rmm.reciepeId,rmm.sessionId,rmm.inputStepId, rmm.currentStepId,rmm.inputResultId,rmm.columnName,rmm.customValue)
    return {f"{Result}"}
###############ChangeDatatype################
#############################################
class changeDataTypeRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    dataType: str
@app.post("/changeDataTypeRequestAPI")
# calling the functions with given inputs
async def changeDataTypeRequestAPI(cdr:changeDataTypeRequest):
    Result = changeDataTypedef(cdr.projectId,cdr.reciepeId,cdr.sessionId,cdr.inputStepId, cdr.currentStepId,cdr.inputResultId,cdr.columnName,cdr.dataType)
    return {f"{Result}"}
############ChangeColumnname###############
######################################
class renameColumnNameRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    NewColumnName: str
@app.post("/renameColumnNameRequestAPI")
# calling the functions with given inputs
async def renameColumnNameRequestAPI(ccn:renameColumnNameRequest):
    Result = renameColumnNamedef(ccn.projectId,ccn.reciepeId,ccn.sessionId,ccn.inputStepId, ccn.currentStepId,ccn.inputResultId,ccn.columnName,ccn.NewColumnName)
    return {f"{Result}"}
###############SplitbyDelimiter#################
##################################################
class SplitbyDelimiterRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    delimiters: List[dict]
    newColumnNames: List[dict]


@app.post("/SplitbyDelimiterRequestAPI")
# calling the functions with given inputs
async def SplitbyDelimiterRequestAPI(sdr: SplitbyDelimiterRequest):
    Result = SplitbyDelimiterdef(sdr.projectId,sdr.reciepeId,sdr.sessionId,sdr.inputStepId, sdr.currentStepId,sdr.inputResultId,sdr.columnName,sdr.delimiters,sdr.newColumnNames)
    return {f"{Result}"}
#############ColumnDetails#####################
class columnDetailsRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    columnName: str
@app.post("/columnDetailsRequestAPI")
# calling the functions with given inputs
async def columnDetailsRequestAPI(cdd: columnDetailsRequest):
    Result = columnDetailsdef(cdd.projectId,cdd.reciepeId,cdd.sessionId,cdd.inputStepId, cdd.currentStepId,cdd.inputResultId,cdd.columnName)
    return {f"{Result}"}
###############################################
###############Masking#########################
class MaskingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnname: str
    value: int
    maskvalue: str

@app.post("/MaskingRequestApi")
# calling the functions with given inputs
async def MaskingRequestApi(rm: MaskingRequest):
    Result = maskingreq(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnname, rm.value,rm.maskvalue)
    return {f"{Result}"}
###################################################
################OrdinalEncoding####################
class OrdinalEncodingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    col_name: str
    col_name_ordinal: str
    scale_mapper : dict
@app.post("/OrdinalRequestApi")
# calling the functions with given inputs
async def OrdinalRequestApi(oer:OrdinalEncodingRequest):
    Result = ordinal_function(oer.projectId,oer.reciepeId,oer.sessionId,oer.inputStepId, oer.currentStepId,oer.inputResultId,oer.col_name,oer.col_name_ordinal,oer.scale_mapper)
    return {f"{Result}"}
###################################################
####################IQR Capping####################
class IQRRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnvalues: list

@app.post("/IQRRequestApi")
# calling the functions with given inputs
async def IQRRequestApi(rm: IQRRequest):
    Result = iqr_for_all_columns(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnvalues)
    return {f"{Result}"}

#########################################
###########WEBSOCKET##########################
@app.websocket("/notify")
async def websocket_endpoint(websocket: WebSocket, clients=Depends(get_ws_clients)):
    await websocket.accept()
    clients.append(websocket)
    try:
        while True:
            _ = await websocket.receive_text()
    finally:
        ws_clients.remove(websocket)
