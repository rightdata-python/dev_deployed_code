#importing the required libraries and packages
import fuzzymatcher
import pandas
import pyspark
from properties import path

#path = "C:/Users/kisho/PycharmProjects/DevDeployedCode"
import json
from deltalake import DeltaTable
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

#defining coltomove function to show the input columns as first columns in output
def coltomove(df, cols_move_list=[], ref_col=''):
    cols = df.columns.tolist()
    a1 = cols[:list(cols).index(ref_col) + 1]
    a2 = cols_move_list

    a1 = [i for i in a1 if i not in a2]
    a3 = [i for i in cols if i not in a1 + a2]

    return (df[a1 + a2 + a3])

#defining fuzzymatcher fuction to take inputs, perform the actions and gives the output
def fuzzyMatch_Def(projectId, reciepeId, sessionId, leftStepID, leftResultID, rightStepID,rightResultID, currentStepId, columnMapping, threshold):
    if sessionId == None:
        leftDeltaTable = "R_" + str( projectId) + "_" + reciepeId + "_" + leftStepID + "_" + leftResultID
        rightDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + rightStepID + "_" + rightResultID
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        leftDeltaTable = "R_" + str( projectId) + "_" + reciepeId + "_" + sessionId + "_" + leftStepID + "_" + leftResultID
        rightDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + rightStepID + "_" + rightResultID
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    leftDeltaTablepath = path + "/" + leftDeltaTable
    rightDeltaTablepath = path + "/" + rightDeltaTable
    try:
        print("Reading Data")
        #Read Driver dataset or left dataset as dataframe
        DriverDataset = DeltaTable(leftDeltaTablepath).to_pandas()
        print("No of Rows, Columns and Column names in DriverDataset: ", len(DriverDataset), ";",len(DriverDataset.columns), ";", DriverDataset.columns)
        #Read Reference dataset or right dataset as dataframe
        ReferenceData = DeltaTable(rightDeltaTablepath).to_pandas()
        print("No of Rows, Columns and Column names in ReferenceData: ", len(ReferenceData),";",len(ReferenceData.columns), ";", ReferenceData.columns)
        #Extracting the columnnames of driver dataset which came as inputs to drivercolumnnames list
        drivercolumnnames = []
        for i,x in enumerate(columnMapping):
            #DriverDataset[x['driverColumn']] = DriverDataset[x['driverColumn']].astype(str)
            drivercolumnnames.append(x['driverColumn'])
            numofinputs = i+1
        #Extracting the columnnames of reference dataset which came as inputs to referencecolumnnames list
        referencecolumnnames = []
        for y in columnMapping:
            #ReferenceData[x['referenceColumn']] = ReferenceData[x['referenceColumn']].astype(str)
            referencecolumnnames.append(y['referenceColumn'])
        #appending _x for input columns of reference dataset in the orginal dataset
        #i is the number of input column came from each dataset.
        for i in range(numofinputs):
            ReferenceData = ReferenceData.rename(columns=lambda z: z + '_x' if z in referencecolumnnames else z)

        # appending _x for input columns of reference dataset in the input columnnames list
        append_str = '_x'
        # Append suffix to strings in list
        referencecolumnnames = [sub + append_str for sub in referencecolumnnames]
        print("Received columns")

        #run the fuzzy matcher algorithm
        Join_FuzzyMatched = fuzzymatcher.fuzzy_left_join(DriverDataset, ReferenceData, drivercolumnnames, referencecolumnnames)
        print("fuzzy matcher completed")

        # copy the data and drop __id_left and __id_right which are the columns generated when fuzzy matcher algorithm is run
        Join_FuzzyMatched_copy = Join_FuzzyMatched.drop('__id_left', 1)
        Join_FuzzyMatched_copy = Join_FuzzyMatched_copy.drop('__id_right', 1)
        #combining all the input column names into Mathcedcolumns list to move these columns to front of the dataset. For visual appeal
        MatchedColumns = []
        for j in columnMapping:
            append_ref_columnnames = '_x'
            MatchedColumns.extend([j['driverColumn'], j['referenceColumn'] + append_ref_columnnames])
        Join_FuzzyMatched_copy = coltomove(Join_FuzzyMatched_copy, cols_move_list=MatchedColumns, ref_col='best_match_score')
        print(Join_FuzzyMatched_copy.head())
        # Join_FuzzyMatched_copy.reset_index(inplace=True)
        # print(Join_FuzzyMatched_copy.head())
        # apply normalization techniques on Column best_match_score so that the values are in range 1 to -1
        Join_FuzzyMatched_copy['best_match_score'] = Join_FuzzyMatched_copy['best_match_score'] / Join_FuzzyMatched_copy['best_match_score'].abs().max()
        #sorting the dataset by best_match_score scores
        Join_FuzzyMatched_copy = Join_FuzzyMatched_copy.sort_values(by=['best_match_score'], ascending=False)
        #setting the best_match_score as index column for the fuzzy dataset
        #Join_FuzzyMatched_copy = Join_FuzzyMatched_copy.set_index('best_match_score')

        print("output is ready")
        bestmatch = Join_FuzzyMatched_copy.query(f'best_match_score >= {threshold}')
        print(bestmatch.head())
        print("No of Rows, Columns and Column names in Fuzzy Join Data: ", len(bestmatch), ";",
              len(bestmatch.columns), ";", bestmatch.columns)
        #Threshold from input if none all the dataset is saved in the output location else only the data above the threshold of best_match_score is saved in the output location.
        df1 = spark.createDataFrame(bestmatch)
        df1.show()
        outputDeltaTablepath = path + "/" + outputDeltaTable
        df1.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Fuzzy matcher is completed",
                "data": {"inputsteps": len(bestmatch), "outputsteps": df1.count()}}
        return json.dumps(list)
    # except OSError as err:
    #     print("OS error: {0}".format(err))
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Some thing went wrong please check the inputs"}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)


#fuzzyMatch_Def("1", "FL", None, "3", "1", "234","1", "456", [{"driverColumn": "City", "referenceColumn": "ProviderCity"}, {"driverColumn": "FacilityName","referenceColumn": "ProviderName"},{"driverColumn": "Address","referenceColumn": "ProviderAddress"}], "0")




