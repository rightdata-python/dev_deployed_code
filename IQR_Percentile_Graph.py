
import pandas as pd
import numpy as np

from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
import json
from deltalake import DeltaTable

def IQR_graph_data(projectId,reciepeId,sessionId,inputStepId,inputResultId,columnName):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Reading data")
    try:
        #df = spark.read.format("delta").load(DeltaTable)
        pandasDF = DeltaTable(inputDeltaTablepath).to_pandas()
        # columnNames = Data.columns
        print("No of Columns and Column names in Delta table: ", len(pandasDF.columns),";",pandasDF.columns)
        print("No. of records in Data:", len(pandasDF))
        print(pandasDF)

        ###return a list containig
        result_dict = { 'data': [pandasDF[columnName].quantile(0), pandasDF[columnName].quantile(.25), pandasDF[columnName].quantile(.5), pandasDF[columnName].quantile(.75), pandasDF[columnName].quantile(1)]}
        result_dict['name'] = 'Observations'
        print(result_dict)

        json_object = json.dumps(result_dict)#, default=np_encoder)
        list = {"status": "Success", "message": "Successfully completed IQR Graph request",
                "data": json_object}
        print(list)
        return json.dumps(list)

    except:
        FailureMessage = {"status": "Failure",
                          "message": "Some thing went wrong, please check the inputs!"}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

def Percentile_graph_data(projectId,reciepeId,sessionId,inputStepId,inputResultId,col):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
    inputDeltaTablepath = path + "/" + inputDeltaTable
    try:
        print("Reading data")

        #df = spark.read.format("delta").load(DeltaTable)
        data = DeltaTable(inputDeltaTablepath).to_pandas()
        result_dict = {}
        Q1 = data[col].quantile(.25)
        Q3 = data[col].quantile(.75)
        range = data[col].quantile(.75) - data[col].quantile(.25)

        obs_list= [data[col].quantile(0),data[col].quantile(.25),data[col].quantile(.5),data[col].quantile(.75),data[col].quantile(1)]
        observations_dict= {'name': 'Observations', 'data': obs_list}

        upper_threshold= Q3+1.5* range
        lower_threshold= Q1-1.5* range

        upper_outlier_list= data[col][data[col]> upper_threshold].to_list()
        lower_outlier_list= data[col][data[col]< lower_threshold].to_list()
        outlier_list= upper_outlier_list+ lower_outlier_list
        outlier_list_result = []
        for i in outlier_list:
            outlier_list_result.append([0, i])
        outlier_list_result

        outlier_dict= {'name': 'Outlier', 'data': outlier_list_result}
        result_dict= {'observations_dict':observations_dict,'outlier_dict': outlier_dict }
        print(result_dict)

        json_object = json.dumps(result_dict)#, default=np_encoder)
        list = {"status": "Success", "message": "Successfully completed IQR Graph request",
                    "data": json_object}
        print(list)
        return json.dumps(list)

    except:
        FailureMessage = {"status": "Failure",
                          "message": "Some thing went wrong, please check the inputs!"}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

#IQR_graph_data("1","newPSNAN","s01","3", "222","1","ENROLLMENT")
#Percentile_graph_data("1","newPSNAN","s01","3", "222","1","ENROLLMENT")
