#importing required files and packages

import uvicorn
import asyncio
from fastapi import FastAPI, Depends
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from fastapi import FastAPI, Response
from starlette.websockets import WebSocket
from typing import List, Optional
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware

ws_clients = []
app = FastAPI()

def get_ws_clients():
    return ws_clients

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"]
)

#########################################
@app.websocket("/notify")
async def websocket_endpoint(websocket: WebSocket, clients=Depends(get_ws_clients)):
    await websocket.accept()
    clients.append(websocket)
    try:
        while True:
            _ = await websocket.receive_text()
    finally:
        ws_clients.remove(websocket)
############################################
class renameColumnNameRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    NewColumnName: str
@app.post("/renameColumnNameRequestAPI")
# calling the functions with given inputs
async def renameColumnNameRequestAPI(ccn:renameColumnNameRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("RENAME_COLUMN_NAME_STARTED") for ws in clients])
    Result = renameColumnNamedef(ccn.projectId,ccn.reciepeId,ccn.sessionId,ccn.inputStepId, ccn.currentStepId,ccn.inputResultId,ccn.columnName,ccn.NewColumnName)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("RENAME_COLUMN_NAME_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###################################

import pyspark
import json
from properties import path
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .getOrCreate()
print("spark session Started")

def renameColumnNamedef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnName,NewColumnName):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    DeltaTable = path + "/" + inputDeltaTable
    print("Reading Universal data")
    print(DeltaTable)
    try:
        #Reading delta table as spark df
        Data = spark.read.format("delta").load(DeltaTable)
        #Reading delta table as pandas df
        #pandasDF = DeltaTable(inputDeltaTablepath).to_pandas()
        print("Column names in Delta table: ", Data.columns)
        #rename the column selected
        df = Data.withColumnRenamed(columnName, NewColumnName)

        print("Column names of new Delta table: ", df.columns)
        outputDeltaTablepath = path + "/" + outputDeltaTable
        #if we are going with pandas df to work on, we need to change the pandas df to spark df using below code
        #df = spark.createDataFrame(pandasDF)
        #saving spark df as delta table

        df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully renamed the selected column",
                "data": {"inputsteps": Data.count(), "outputsteps": df.count()}}
        return json.dumps(list)

    except:
        #exception message
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

