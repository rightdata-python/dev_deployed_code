import pyspark
from pyspark.sql.functions import lit, col, isnan, count, first, split
from properties import path
import json
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def firstCellTransformationdef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,transformation, columnName):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
    print("Input Table:", inputDeltaTable)
    DeltaTable = path + "/" + inputDeltaTable
    print("Reading Universal data")
    try:
        Data = spark.read.format("delta").load(DeltaTable)
        Data.show()

        df = Data.select(col(columnName))
        firstvalidcell = df.select(
            [first(x, ignorenulls=True).alias(x) for x in df.columns]).first()  # df.first_valid_index()
        print(firstvalidcell[0])
        if transformation == "mask":
            firstvalidcell = str(firstvalidcell[0])
            firstMask = 'X' * len(firstvalidcell)
            print(firstMask)
            dict = {"mask": {firstvalidcell: firstMask}}
            dictJSON = json.dumps(dict)
            print(dictJSON)
        elif transformation == "splitByDelimiter":
            import re
            # re.search('123', firstvalidcell[0])
            firstvalidcell = str(firstvalidcell[0])
            a = re.search('[#:^,/\ .]', firstvalidcell)
            print("delimiter: ", a[0])
            # print(re.findall(r" :,\/{}()!.", firstvalidcell[0]))
            splitvalues = firstvalidcell.split(a[0], 1)

            # firstMask = 'X' * len(firstvalidcell[0])
            print(splitvalues)
            dict = {"splitByDelimiter": {firstvalidcell: splitvalues}}
            dictJSON = json.dumps(dict)
            print(dictJSON)
        # else:
        #     dictJson = "Enter appropriate value for transformation"
        list = {"status": "Success", "message": "Successfully completed first cell transformation Request",
                "data": dictJSON}
        return json.dumps(list)
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Delta Table entered doesnt exists, Please enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)


#firstCellTransformationdef("1","newPSNAN","s01","3","300","1","splitByDelimiter","LATITUDE")

#firstCellTransformationdef("1","c0d1f274abd54571b7452389d81a3ee0","STP2","1","3","1","mask","Period")