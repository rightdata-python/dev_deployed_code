import pandas as pd
import json
import pyspark
import numpy as np
from deltalake import DeltaTable
#from properties import path
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def change_type(value):
    if value == 'string':
        return 'StringType'
    elif value == 'long':
        return 'LongType'
    elif value == 'integer':
        return "IntegerType"
    elif value == 'short':
        return "ShortType"
    elif value == 'float':
        return "FloatType"
    elif value == 'double':
        return "DoubleType"
    elif value == 'bool':
        return "BooleanType"
    elif value == 'date':
        return "DateType"
    elif value == 'timestamp':
        return "TimestampType"
    elif value == 'time':
        return "TimestampType"
    elif value == 'binary':
        return "BinaryType"
    else:
        return value

def pivot_def(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId, column, rows, values):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
    print("Input Table:", inputDeltaTable)
    inputDeltaTable = path + "/" + inputDeltaTable
    try:
        print("Reading data")
        #Data = spark.read.format("delta").load(inputDeltaTable)

        # pandaDF = Data.toPandas()
        pandaDF = DeltaTable(inputDeltaTable).to_pandas()
        print(pandaDF.nunique())
        indexcolumns = rows
        # {values: len}
        # aggfunction expected values : Sum, Count, Average, Max, Min, Median, Std
        # np.sum ; np.mean; np.min; max; len;
        pivotedDF = pandaDF.pivot_table(columns=column, index=indexcolumns, aggfunc=values,
                                        fill_value=0)  # , columns=['NAME','ZIP'], values = values, aggfunc={values:len},fill_value=0)#,'STATE'
        # Use the list comprehension to make a list of new column names and assign it back
        # to the DataFrame columns attribute.
        print("pivoted before indexing:")
        print(pivotedDF)

        pivotedDF.columns = ["_".join((str(j),str(k))) for j, k in pivotedDF.columns]
        pivotedDF.reset_index(inplace=True)
        # pivotedDF.index.names = ["index"]
        # indexColumn = pivotedDF.columns[0]
        # print(indexColumn)
        # pivotedDF.set_index(indexColumn, inplace=True)
        print("pandas df: ",pivotedDF)
        #print("printSchema : ", pivotedDF.printSchema)
        #pivotedDF = pivotedDF.astype(str)
        spark_df = spark.createDataFrame(pivotedDF)
        #print("printSchema : ", spark_df.schema)

        #print(schemaJson)
        print("spark df: ", spark_df)
        if currentStepId == None:

            schemaJson = spark_df.schema.json()
            print(type(schemaJson))
            #print(dict(schemaJson))
            schemaJsonload = json.loads(schemaJson)
            for dic in schemaJsonload['fields']:
                dic['type'] = change_type(dic['type'])
            print(schemaJsonload)
            #print("printSchema : ", spark_df.printSchema())
            final_dictionary = json.loads(schemaJson)
            columnDict = {}
            columnDict["columns"] = final_dictionary["fields"]
            columnDict_final = columnDict.pop('columns', None)
            new_list = [{k: v for k, v in d.items() if k != 'metadata'} for d in columnDict_final]
            #print(new_list)
            # print(type(columnDict_final))
            Data_Json = pivotedDF.to_dict(orient='records')
            print("Json Pivoted: ",Data_Json)
            list = {"status": "Success", "columns": new_list,
                    "data": Data_Json}
        else:
            if sessionId == None:
                outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
            else:
                outputDeltaTable = "R_" + str(
                    projectId) + "_" + reciepeId + "_" + sessionId + "_" + currentStepId + "_1"
            outputDeltaTablepath = path + "/" + outputDeltaTable
            print("Output Table with path:", outputDeltaTablepath)

            spark_df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                outputDeltaTablepath)
            print("saved in given location")
            list = {"status": "Success", "message": "Successfully completed pivot on selected data",
                    "data": {"inputsteps": len(pandaDF), "outputsteps": spark_df.count()}}
        print(list)
        return json.dumps(list)
    except OSError as err:
        print("OS error: {0}".format(err))
    # except:
    #     FailureMessage = {"status": "Failure",
    #                       "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
    #     FailureMessageJson = json.dumps(FailureMessage)
    #     print(FailureMessageJson)
    #     return json.dumps(FailureMessage)

# def datatypedef(df, columnName):
#     dictData = {}
#     dictData["Name"] = columnName
#     dictData["DataType"] = df.schema[columnName].dataType
#     return dictData




pivot_def("1","newPSNAN","s01","3", None,"1", "STATE", ["SOURCEDATE","STATUS"],{"TYPE": "mean"})#, "POPULATION":'sum'})#, "END_GRADE": 'sum'})
#{"TYPE": "std", "POPULATION":'sum'}#sum, count, mean, median, std, min, max