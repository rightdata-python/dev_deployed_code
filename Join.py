import json
import pyspark
from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
from sklearn.model_selection import StratifiedShuffleSplit
from pyspark.sql.functions import lit, rand, when
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def JoinDatasets(projectId, reciepeId, sessionId, stepId, leftInputDetails, rightInputDetails, columnMapping, currentStepId, mergeType):
    #stepid and resultid
    leftStepID_ResultId = stepId[0]
    rightStepID_ResultId = stepId[1]#print("leftStepID_ResultId",leftStepID_ResultId)#print("rightStepID_ResultId",rightStepID_ResultId)

    leftStepID = leftStepID_ResultId["inputStepId"]
    rightStepID = rightStepID_ResultId["inputStepId"]#print("leftStepID", leftStepID)#print("rightStepID", rightStepID)

    leftResultID = leftStepID_ResultId["inputResultId"]
    rightResultID = rightStepID_ResultId["inputResultId"]#print("leftResultID", leftResultID)#print("rightResultID", rightResultID)
    #leftinputdetails step id
    if leftInputDetails["inputStepId"] == leftStepID:
        leftblendcolumns = leftInputDetails["blendColumns"]
        print("leftblendcolumns", leftblendcolumns)
    elif leftInputDetails["inputStepId"] == rightStepID:
        leftblendcolumns = leftInputDetails["blendColumns"]
        print("leftblendcolumns", leftblendcolumns)
    else:
        return "Please enter a valid StepId"
    # rightinputdetails step id
    if rightInputDetails["inputStepId"] == rightStepID:
        rightblendcolumns = rightInputDetails["blendColumns"]
        print("rightblendcolumns", rightblendcolumns)
    elif rightInputDetails["inputStepId"] == leftStepID:
        rightblendcolumns = rightInputDetails["blendColumns"]
        print("rightblendcolumns", rightblendcolumns)
    else:
        return "Please enter a valid StepId"

            # columnname = DcolumnNames[i]
            # print(columnname)
            # newcolumn = columnname + str(1)
            # print(newcolumn)
            # RData = RData.withColumnRenamed(RcolumnNames[i], newcolumn)
            # RcolumnNames[i] = newcolumn
    #column mapping values
    DcolumnNames = []
    RcolumnNames = []
    for x in columnMapping:
        DcolumnNames.append(x['leftColumn'])
    for y in columnMapping:
        RcolumnNames.append(y['rightColumn'])
    print("DcolumnNames", DcolumnNames)
    print("RcolumnNames", RcolumnNames)

    if sessionId == None:
        dDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + leftStepID + "_" + leftResultID
        rDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + rightStepID + "_" + rightResultID
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        dDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + sessionId + "_" + leftStepID + "_" + leftResultID
        rDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + sessionId + "_" + rightStepID + "_" + rightResultID
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    InputTableD = path + "/" + dDeltaTable
    InputTableR = path + "/" + rDeltaTable
    outputDeltaTablepath = path + "/" + outputDeltaTable
    print("InputTableDriver:",InputTableD)
    print("InputTableReference:", InputTableR)
    print("outputDeltaTablepath:", outputDeltaTablepath)
    try:
        print("Reading Driver data")
        DData_sp = spark.read.format("delta").load(InputTableD)
        print("Reading Reference data")
        RData_sp = spark.read.format("delta").load(InputTableR)
        print("No of records and column names of Driver dataset:", DData_sp.count(), DData_sp.columns)
        print("No of records and column names of Reference dataset:", RData_sp.count(), RData_sp.columns)
        # column name and alias
        leftcolumntoinclude = []
        rightcolumntoinclude = []
        for x in leftblendcolumns:
            leftcolumntoinclude.append(x['name'])
        for y in rightblendcolumns:
            rightcolumntoinclude.append(y['name'])

        print("leftcolumntoinclude", leftcolumntoinclude)
        print("rightcolumntoinclude", rightcolumntoinclude)
        #drop column not include
        DData = DData_sp.select(*leftcolumntoinclude)
        print("No of records and column names in new Driver dataset:", DData.count(), DData.columns)
        RData = RData_sp.select(*rightcolumntoinclude)
        print("No of records and column names in new Reference dataset:", RData.count(), RData.columns)
        for i in range(0, len(leftblendcolumns)):
            #print(i)
            a = leftblendcolumns[i]['name']
            b = leftblendcolumns[i]['alias']
            print(leftblendcolumns[i]['name'])
            print(leftblendcolumns[i]['alias'])
            if a == b:
                print("columnnames is same as alias")
            else:
                DData = DData.withColumnRenamed(a, b)
                print("columnnames are change to alias")
        print("New column names of driver dataset", DData.columns)
        for i in range(0, len(rightblendcolumns)):
            #print(i)
            c = rightblendcolumns[i]['name']
            d = rightblendcolumns[i]['alias']
            print(rightblendcolumns[i]['name'])
            print(rightblendcolumns[i]['alias'])
            if c == d:
                print("columnnames is same as alias")
            else:
                RData = RData.withColumnRenamed(c, d)
                print("columnnames are change to alias")
        print("New column names of Reference dataset", RData.columns)



        #mappingcolumnsset = [df.name == df3.name, df.age == df3.age]
        # mappingcolumnsset = []
        # for i in DcolumnNames:
        #     # cond =[DData[DcolumnNames[i]] == RData[RcolumnNames[i]]]
        #     # print(cond)
        #     mappingcolumnsset.append(i[DData[DcolumnNames[i]] == RData[RcolumnNames[i]]])
        # print(mappingcolumnsset)
        #if len(DcolumnNames)== 1:
        print("len(DcolumnNames)",len(DcolumnNames))
        if len(DcolumnNames)== 1:
            mappingcolumnsset = [DData[DcolumnNames[0]] == RData[RcolumnNames[0]]]
        elif len(DcolumnNames)== 2:
            mappingcolumnsset = [DData[DcolumnNames[0]] == RData[RcolumnNames[0]],DData[DcolumnNames[1]] == RData[RcolumnNames[1]]]
        elif len(DcolumnNames)== 3:
            mappingcolumnsset = [DData[DcolumnNames[0]] == RData[RcolumnNames[0]],DData[DcolumnNames[1]] == RData[RcolumnNames[1]],DData[DcolumnNames[2]] == RData[RcolumnNames[2]]]
        elif len(DcolumnNames)== 4:
            mappingcolumnsset = [DData[DcolumnNames[0]] == RData[RcolumnNames[0]],DData[DcolumnNames[1]] == RData[RcolumnNames[1]],DData[DcolumnNames[2]] == RData[RcolumnNames[2]],DData[DcolumnNames[3]] == RData[RcolumnNames[3]]]
        elif len(DcolumnNames)== 5:
            mappingcolumnsset = [DData[DcolumnNames[0]] == RData[RcolumnNames[0]],DData[DcolumnNames[1]] == RData[RcolumnNames[1]],DData[DcolumnNames[2]] == RData[RcolumnNames[2]],DData[DcolumnNames[3]] == RData[RcolumnNames[3]],DData[DcolumnNames[4]] == RData[RcolumnNames[4]]]
        elif len(DcolumnNames)== 6:
            mappingcolumnsset = [DData[DcolumnNames[0]] == RData[RcolumnNames[0]],DData[DcolumnNames[1]] == RData[RcolumnNames[1]],DData[DcolumnNames[2]] == RData[RcolumnNames[2]],DData[DcolumnNames[3]] == RData[RcolumnNames[3]],DData[DcolumnNames[4]] == RData[RcolumnNames[4]],DData[DcolumnNames[5]] == RData[RcolumnNames[5]]]

        print(mappingcolumnsset)
        print("About to Join")
        #JoinDF = DData.join(RData, mappingcolumnsset, mergeType)#.drop(RcolumnNames[0])
        JoinDF = DData.join(RData,
            [col(f) == col(s) for (f, s) in zip(DcolumnNames, RcolumnNames)],mergeType)
        JoinDF_final = JoinDF.drop(*RcolumnNames)
        JoinDF_final.show()
        print("Join complete")

        JoinDF_final.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully completed Join Request",
                "data": {"inputsteps": DData_sp.count(), "outputsteps": JoinDF_final.count()}}
        print(list)
        return json.dumps(list)

    except:
        FailureMessage = {"status": "Failure",
                          "message": "Some thing went wrong please check the inputs!"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)
#
# result = JoinDatasets("1","PublicSchoolNan","s01",[{"inputStepId": "1","inputResultId": "2"},{"inputStepId": "3","inputResultId": "1"}],{"inputStepId": "1","blendColumns": [
#     {"name": "LEVEL","alias": "LEVEL"},{"name": "NAME","alias": "NAME"},{"name": "ADDRESS","alias": "ADDRESS"}]},{"inputStepId": "3","blendColumns": [
#     {"name": "LEVEL","alias": "LEVEL1"},{"name": "NAME","alias": "NAME1"},{"name": "ZIP","alias": "ZIP"}]},[{"leftColumn": "NAME","rightColumn": "NAME1"},{"leftColumn": "LEVEL","rightColumn": "LEVEL1"}],"999","outer")
# print(result)
