#importing required files and packages
from fastapi import FastAPI
import uvicorn
from typing import List, Optional
from pydantic import BaseModel
from samplingDTRequest import samplingDTRequestdef
from sampleValidationRequest import samplingValidationRequestdef
from columnCategoryCountPaginationRequest_1 import columnCategoryCountPaginationdef
from listOfCategories import listOfCategoriesdef
from deleteColumn import deleteColumndef
from customRemove import RemoveMissmatchdef
from changeDataType import changeDataTypedef
from RenameColumnName import changeColumnNamedef
from splitColumn import SplitbyDelimiterdef
#assigning FastAPI as app
from fastapi.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware
from masking import maskingreq
from capping_IQR import iqr_for_all_columns

app = FastAPI()
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"]
)
#origins = [
#    "http://demo.divami.com/wrangler/",
#    "http://18.144.30.185:5000",
#    "http://localhost:5000",
#    "http://localhost:3000"
#]
#middleware =[
#        Middleware(CORSMiddleware, allow_origins=origins)
#        ]
#app = FastAPI(middleware=middleware)
   # "http://18.144.30.185:8085",
   # ]
#origins = ['*']
#app.add_middleware(GZipMiddleware)
#app.add_middleware(
#    CORSMiddleware,
#    allow_origins=origins,
#    allow_credentials=True,
#    allow_methods=["*"],
#    allow_headers=["*"],
#)
#path = "/home/ubuntu/warehouse/DELTA_WAREHOUSE/uat.db"
################Sampling################
##################################3####
class SamplingRequest(BaseModel):
    #dataset location
    # path: str
    inputDeltaTable: str
    samplingMethod: str
    sampleSize: int
    outputDeltaTable: str

@app.post("/SamplingDTRequestAPI")
#calling the functions with given inputs
async def SamplingRequestAPI(sr:SamplingRequest):
    Result = samplingDTRequestdef(sr.inputDeltaTable, sr.samplingMethod, sr.sampleSize, sr.outputDeltaTable)
    # ...
    return {f"{Result}"}

######SampleValidataion#############
####################################
class SamplingValidationRequest(BaseModel):
    #dataset location
    UniversalDeltaTable: str
    SampleDeltaTable: str

@app.post("/SamplingValidationRequestAPI")
#calling the functions with given inputs
async def SamplingValidationRequestAPI(sv:SamplingValidationRequest):
    Result = samplingValidationRequestdef(sv.UniversalDeltaTable, sv.SampleDeltaTable)
    # ...
    return {f"{Result}"}
###################columnCategoryCountPagination#############
#############################################################
class columnCategoryCountPaginationRequest(BaseModel):
    # dataset location
    inputDeltaTable: str

@app.post("/ColumnCategoriesCountPaginationRequestAPI")
# calling the functions with given inputs
async def ColumnCategoriesCountPaginationRequestAPI(cc:columnCategoryCountPaginationRequest):
    Result = columnCategoryCountPaginationdef(cc.inputDeltaTable)
    return {f"{Result}"}
################List of Categories############################
##############################################################
class listOfCategoriesRequest(BaseModel):
    # dataset location
    inputDeltaTable: str

@app.post("/listOfCategoriesRequestAPI")
# calling the functions with given inputs
async def listOfCategoriesRequestAPI(cc:listOfCategoriesRequest):
    Result = listOfCategoriesdef(cc.inputDeltaTable)
    return {f"{Result}"}
###########DeleteColumn############################
#################################################
class deleteColumnRequest(BaseModel):
    # dataset location
    inputDeltaTable: str
    columnNames: List[dict]
    outputDeltaTable: str

@app.post("/deleteColumnRequestAPI")
# calling the functions with given inputs
async def deleteColumnRequestAPI(dc:deleteColumnRequest):
    Result = deleteColumndef(dc.inputDeltaTable,dc.columnNames,dc.outputDeltaTable)
    # ...
    # ...
    return {f"{Result}"}
#################RemoveMissMatch##############
#############################################
class RemoveMissmatchRequest(BaseModel):
    # dataset location
    inputDeltaTable: str
    columnName: str
    missMatch: str
    outputDeltaTable: str


@app.post("/RemoveMissmatchRequestAPI")
# calling the functions with given inputs
async def RemoveMissmatchRequestAPI(rm:RemoveMissmatchRequest):
    Result = RemoveMissmatchdef(rm.inputDeltaTable,rm.columnName,rm.missMatch, rm.outputDeltaTable)
    # ...
    # ...
    return {f"{Result}"}
###############ChangeDatatype################
#############################################
class changeDataTypeRequest(BaseModel):
    # dataset location
    inputDeltaTable: str
    columnName: str
    dataType: str
    outputDeltaTable: str


@app.post("/changeDataTypeRequestAPI")
# calling the functions with given inputs
async def changeDataTypeRequestAPI(cd:changeDataTypeRequest):
    Result = changeDataTypedef(cd.inputDeltaTable,cd.columnName,cd.dataType,cd.outputDeltaTable)
    # ...
    # ...
    return {f"{Result}"}
############ChangeColumnname###############
######################################
class changeColumnNameRequest(BaseModel):
    # dataset location
    inputDeltaTable: str
    columnName: str
    NewColumnName: str
    outputDeltaTable: str


@app.post("/changeColumnNameRequestAPI")
# calling the functions with given inputs
async def changeColumnNameRequestAPI(cc:changeColumnNameRequest):
    Result = changeColumnNamedef(cc.inputDeltaTable,cc.columnName,cc.NewColumnName,cc.outputDeltaTable)
    return {f"{Result}"}
###############SplitbyDelimiter#################
##################################################
class SplitbyDelimiterRequest(BaseModel):
    # dataset location
    inputDeltaTable: str
    columnName: str
    delimiters: List[dict]
    newColumnNames: List[dict]
    outputDeltaTable: str


@app.post("/SplitbyDelimiterRequestAPI")
# calling the functions with given inputs
async def SplitbyDelimiterRequestAPI(rm: SplitbyDelimiterRequest):
    Result = SplitbyDelimiterdef(rm.inputDeltaTable, rm.columnName, rm.delimiters, rm.newColumnNames, rm.outputDeltaTable)
    return {f"{Result}"}
###############################################
###############Masking#########################
class MaskingRequest(BaseModel):
    # dataset location
    inputDeltaTable: str
    columnname: str
    value: int
    outputDeltaTable: str


@app.post("/MaskingRequestApi")
# calling the functions with given inputs
async def MaskingRequestApi(rm: MaskingRequest):
    Result = maskingreq(rm.inputDeltaTable, rm.columnname, rm.value, rm.outputDeltaTable)
    return {f"{Result}"}
###################################################
################OrdinalEncoding####################
class OrdinalEncodingRequest(BaseModel):
    # dataset location
    inputDeltaTable: str
    col_name: str
    col_name_ordinal: str
    scale_mapper : str
    outputDeltaTable: str


@app.post("/OrdinalRequestApi")
# calling the functions with given inputs
async def OrdinalRequestApi(rm: OrdinalEncodingRequest):
    Result = ordinal_function(rm.inputDeltaTable, rm.col_name, rm.col_name_ordinal,rm.scale_mapper, rm.outputDeltaTable)
    return {f"{Result}"}
###################################################
####################IQR Capping####################
class IQRRequest(BaseModel):
    # dataset location
    inputDeltaTable: str
    columnvalues: list
    outputDeltaTable: str


@app.post("/IQRRequestApi")
# calling the functions with given inputs
async def IQRRequestApi(rm: IQRRequest):
    Result = iqr_for_all_columns(rm.inputDeltaTable, rm.columnvalues, rm.outputDeltaTable)
    return {f"{Result}"}
###################################################
