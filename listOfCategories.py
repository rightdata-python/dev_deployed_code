import json
import pandas as pd
import pyspark
from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
from sklearn.model_selection import StratifiedShuffleSplit
from pyspark.sql.functions import lit, rand, when
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
from deltalake import DeltaTable
# spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
#      .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
#      .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
#      .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
#      .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
#      .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
#      .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
#      .enableHiveSupport() \
#      .getOrCreate()
#
# print("spark session Started")
# to return list of categories
def listOfCategoriesdef(projectId, reciepeId, sessionId, inputStepId, currentStepId, inputResultId):
    if sessionId == None:
            inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
            inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId

    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Reading Universal data")
    try:
        df = DeltaTable(inputDeltaTablepath).to_pandas()
        #unique values of all the columns in data
        data = df.apply(lambda col: col.unique().tolist())

        print(data)
        print(type(data))
        df1 = pd.DataFrame(data).reset_index()

        df1.columns = ['column_name', 'list of Categories']
        print(df1)
        # df1 = df1.set_index("column_name")
        # Data_Json = df1["list of Categories"].to_dict()
        Data_Json= df1.to_dict('records')
        #Data_Json = df1.to_json(orient='records')
        #print(Data_Json)
        #json_object = json.dumps(FinalDict, indent=4)
        FinalDict = {"List Of Categories by column": Data_Json}
        #print(FinalDict)
        list = {"status": "Success", "message": "Successfully Completed list of categories Request", "data": FinalDict}
        print(list)
        return json.dumps(list)
    except:
        FailureMessage = {"status": "Failure",
                                  "message": "Some thing went wrong, please check the input parameters"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)


        # Data = spark.read.format("delta").load(DeltaTable)
        #
        # columnNames = Data.columns
        # print("Column names: ", columnNames)
        # try:
        #     FinalDict = {"List Of Categories by column": []}
        #     for i in columnNames:
        #         dictData = {}
        #         dictData["column_name"] = i
        #         distinct_column_val = Data.select(i).distinct().collect()
        #         distinct_column_vals = [v[i] for v in distinct_column_val]
        #         # print(distinct_column_vals)
        #         dictData["list of Categories"] = distinct_column_vals
        #         print("list of categories of column ", i, " is :", dictData)
        #         FinalDict["List Of Categories by column"].append(dictData)
        #     print(FinalDict)
    #         json_object = json.dumps(FinalDict, indent=4)
    #         list = {"status": "Success", "message": "Successfully Completed list of categories Request",
    #                 "data": json_object}
    #         return json.dumps(list)
    #     except:
    #         FailureMessage = {"status": "Failure",
    #                           "message": "Some thing went wrong, please check the input parameters"}
    #         FailureMessageJson = json.dumps(FailureMessage)
    #         print(FailureMessageJson)
    #         return json.dumps(FailureMessage)
    #
    #
    # except:
    #     FailureMessage = {"status": "Failure","message": "Delta Table entered doesnt exists, Please enter a valid Delta Table"}
    #     FailureMessageJson = json.dumps(FailureMessage)
    #     print(FailureMessageJson)
    #     return json.dumps(FailureMessage)

#listOfCategoriesdef("1", "PublicSchoolNan","s01","115", "222","1")
