import json
import pandas as pd
from deltalake import DeltaTable
# import pyspark
# from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
# from sklearn.model_selection import StratifiedShuffleSplit
# from pyspark.sql.functions import lit, rand, when
#from properties import path
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
# spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
#      .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
#      .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
#      .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
#      .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
#      .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
#      .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
#      .enableHiveSupport() \
#      .getOrCreate()
# print("spark session Started")
def previewdefpandas(projectId, reciepeId, sessionId, inputStepId, currentStepId, inputResultId):
    if sessionId == None:
        UniverseDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        UniverseDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId
    InputTableUniverse = path + "/" + UniverseDeltaTable
    print("Reading Universal data")
    uData = DeltaTable(InputTableUniverse).to_pandas()


    if sessionId == None:
        inputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId
    DeltaTable = path + "/" + inputDeltaTable
    print("Input Table with path:", DeltaTable)
    pandaDF = DeltaTable(DeltaTable).to_pandas()
    #DeltaDF = spark.read.format("delta").load(DeltaTable)
    #pandaDF = DeltaDF.toPandas()
    print(pandaDF.head())
    #columnNames = pandaDF.columns.tolist()
    dictData = pandaDF.apply(lambda x: previewdef(x, x.name))

    # for i in columnNames:
    #     dictData["data_type"] = DeltaDF.schema[i].dataType
    # print(dictData)
    #previewdef(lambda x:x.index))
    #print(dictData)
    #print(type(dictData))
    #FinalDict.loc[columnNames]
    final_Dict = [dic for dic in dictData]
    print(final_Dict)
    json_object = json.dumps(final_Dict, indent=4)
    list = {"status": "Success", "message": "Successfully Completed Sample Validation Request",
            "data": json_object}
    return json.dumps(list)
    #
    #
    # dtypeFinal = {"data_type":[]}
    # dtypeFinal = {"dtype":[]}
    # for i in columnNames:
    #     dictu = {}
    #     dictu["data_type"] = DeltaDF.schema[i].dataType
    #     dtypeFinal["dtype"].append(dictu)
    # print(dtypeFinal)
    # d = dtypeFinal.values()
    # print(d)
    #
    # # for field in DeltaDF.schema.fields:
    # #     #dtypedic = {}
    # #     #dtypedic["data_type"] = str(field.dataType)
    # #
    # #     dtypeFinal["data_type"].append(str(field.dataType))
    # # print(dtypeFinal)
    #     #print(field.name + " , " + str(field.dataType))
    # #print(datatypeDict)
    #
    # combined_list = list(zip(d, map(str, final_Dict)))
    # print(combined_list)

def previewdef(Data_DeltaDF, columnName):
    dictData = {}
    dictData["column_name"] = columnName
    # # datatype = Data_DeltaDF.schema[columnName].dataType
    #dictData["data_type"] = Data_DeltaDF.dtypes
    # categoryCount = Data_DeltaDF[columnName].nunique()
    # dictData["category_count"] = categoryCount
    categoryCount = Data_DeltaDF.nunique()
    dictData["category_count"] = categoryCount
    countdata = len(Data_DeltaDF)
    # null
    nullcount = Data_DeltaDF.isnull().sum()
    if nullcount == 0:
         nullpercentage = 0
    else:
         nullpercentage = (nullcount / countdata) * 100

    dictData["null_count"] = nullcount
    dictData["null_percentage"] = nullpercentage
    # # empty
    empty = Data_DeltaDF[Data_DeltaDF == '']
    if len(empty) == 0:
         emptypercentage = 0
    else:
        emptypercentage = (len(empty) / countdata) * 100
    # #emptycount = 0
    dictData["empty_count"] = len(empty)
    dictData["empty_percentage"] = emptypercentage
    # datatypeMissmatch
    datatypemismatchcount = 0
    datatypemismatchpercentage = 0
    dictData["mismatched_datatype_count"] = datatypemismatchcount
    dictData["mismatched_datatype_percentage"] = datatypemismatchpercentage
    # domain missmatch
    domainmismatchcount = 0
    domainmismatchpercentage = 0
    dictData["mismatched_domian_count"] = domainmismatchcount
    dictData["mismatched_domian_percentage"] = domainmismatchpercentage
    # validpercentage
    validvaluescount = countdata - nullcount - len(empty) - datatypemismatchcount - domainmismatchcount
    dictData["valid_data_count"] = validvaluescount
    validvaluespercentage = 100 - nullpercentage - emptypercentage - datatypemismatchpercentage - domainmismatchpercentage
    dictData["valid_data_percentage"] = validvaluespercentage
    print("Details of column ", columnName, " is :", dictData)
    return dictData

previewdefpandas("1","newPSNAN","s01","3", "4","1")

