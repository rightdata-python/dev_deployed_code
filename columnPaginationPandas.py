import json
import numpy as np
from deltalake import DeltaTable
import multiprocessing as mp
import concurrent.futures
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
def columnPaginationPandasDef(projectId,reciepeId,sessionId, inputStepId, currentStepId, inputResultId):
        print("sampling validatiaon started")
        if sessionId == None:
            UniverseDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        else:
            UniverseDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+ inputStepId + "_" + inputResultId
        InputTableUniverse = path +"/" + UniverseDeltaTable
        print("Reading Universal data")
        pandaDF = DeltaTable(InputTableUniverse).to_pandas()
        print(pandaDF.head())
        # fetching the number of rows and columns
        rows = len(pandaDF.index)
        cols = len(pandaDF.columns)
        # displaying the number of rows and columns
        print("Rows: " + str(rows))
        print("Columns: " + str(cols))
        # columnNames = pandaDF.columns.tolist()
        df_types = pandaDF.dtypes
        print(df_types)
        df_types_1 = df_types.replace({int: 'Integer', float: 'Float', object: 'String'})
        print(df_types_1)
        # pool = mp.Pool(mp.cpu_count())
        # results = [pool.apply(previewdef, args=(pandaDF))]
        # pool.close()
        # print(results)
        # import dask.dataframe as dd
        # from dask.multiprocessing import get
        # ddata = dd.from_pandas(pandaDF, npartitions=8)
        # #result = ddata.map_partitions(lambda df: df.apply(lambda x: previewdef(x, x.name))).compute()
        # #result = ddata.apply(previewdef, axis=1, args=(pandaDF))
        # #result = ddata.apply(lambda x: previewdef(x), axis=1)
        # result = ddata.map_partitions(lambda df: df.apply(lambda x: previewdef(x, x.name))).compute(scheduler = get)
        # print(result)
        # full = ddata.merge(result,left_index=True, right_index=True)
        # print(full)
        # final_Dict = [dic for dic in full]
        # print(final_Dict)
        # with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
        #     result =  list(executor.map(previewdef, pandaDF))
        #     print(result)
            # cols = list(pandaDF.columns)
            # result = list(executor.map(previewdef, pandaDF, cols))
            # # from itertools import repeat
            # #
            # # for result in executor.map(previewdef, repeat(cols)):
            # print(result)
            # for dictData in zip(pandaDF, executor.map(previewdef, pandaDF)):
            #     print(dictData)

        #     cols = list(pandaDF.columns)#remaining_cols
        #     result = executor.map(previewdef, cols)
        # #
        # print(result)
        dictData = pandaDF.apply(lambda x: previewdef(x, x.name))
        print(dictData)
        final_Dict = [dic for dic in dictData]
        print(final_Dict)
        json_object = json.dumps(final_Dict, default=np_encoder)
        list = {"status": "Success", "message": "Successfully Completed Sample Validation Request",
                "data": json_object}
        return json.dumps(list)
def np_encoder(object):
    if isinstance(object, np.generic):
        return object.item()

def previewdef(Data_DeltaDF, columnName):#Data_DeltaDF, columnName):
    #Data_DeltaDF = DeltaTable(InputTableUniverse).to_pandas()
    print("type of Data:",type(Data_DeltaDF))
    #dictData_column = {columnName: []}
    dictData = {}
    #dictData["column_name"] = columnName
    # # datatype = Data_DeltaDF.schema[columnName].dataType
    dataTypeObj = Data_DeltaDF.dtypes
    # if dataTypeObj == 'int64' or dataTypeObj == 'int32' or dataTypeObj == 'int' :
    #     dictData["data_type"] = "Integer"
    # elif dataTypeObj == 'str' or dataTypeObj == 'object' :
    #     dictData["data_type"] = "String"
    # elif dataTypeObj == 'float64' or dataTypeObj ==  'float32' or dataTypeObj ==  'float' :
    #     dictData["data_type"] = "Float"
    # else: #dtype('float64')

    #dictData["data_type"] = df_types_1
    categoryCount = Data_DeltaDF.nunique()
    dictData["category_count"] = categoryCount
    countdata = len(Data_DeltaDF)
    # null
    nullcount = Data_DeltaDF.isnull().sum()
    if nullcount == 0:
         nullpercentage = 0
    else:
         nullpercentage = (nullcount / countdata) * 100

    dictData["null_count"] = nullcount
    dictData["null_percentage"] = nullpercentage
    # # empty
    empty = Data_DeltaDF[Data_DeltaDF == '']
    if len(empty) == 0:
         emptypercentage = 0
    else:
        emptypercentage = (len(empty) / countdata) * 100
    # #emptycount = 0
    dictData["empty_count"] = len(empty)
    dictData["empty_percentage"] = emptypercentage
    # datatypeMissmatch
    datatypemismatchcount = 0
    datatypemismatchpercentage = 0
    dictData["mismatched_datatype_count"] = datatypemismatchcount
    dictData["mismatched_datatype_percentage"] = datatypemismatchpercentage
    # domain missmatch
    domainmismatchcount = 0
    domainmismatchpercentage = 0
    dictData["mismatched_domian_count"] = domainmismatchcount
    dictData["mismatched_domian_percentage"] = domainmismatchpercentage
    # validpercentage
    validvaluescount = countdata - nullcount - len(empty) - datatypemismatchcount - domainmismatchcount
    dictData["valid_data_count"] = validvaluescount
    validvaluespercentage = 100 - nullpercentage - emptypercentage - datatypemismatchpercentage - domainmismatchpercentage
    dictData["valid_data_percentage"] = validvaluespercentage
    #print("Details of column ", columnName, " is :", dictData)

    dictData_column = {columnName : [dictData]}
    return dictData_column

