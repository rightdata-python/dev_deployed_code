import math
import pandas as pd
import numpy as np
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
import json
from deltalake import DeltaTable
from pandas.api.types import is_numeric_dtype
from Regex import replace_regex
from data_quality_index import technical_quality_score


def np_encoder(object):
    if isinstance(object, np.generic):
        return object.item()

def countRemovedef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,customValue,columnName,value):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Reading data")
    try:
        #df = spark.read.format("delta").load(DeltaTable)
        df = DeltaTable(inputDeltaTablepath).to_pandas()
        # columnNames = Data.columns
        print("No of Columns and Column names in Delta table: ", len(df.columns),";",df.columns)
        print("No. of records in Data:", len(df))
        print(df)
        print("Datatype of column:",df.dtypes[columnName])
        print("column values: ", (df[columnName].tolist()))
        
        if customValue =="lessthanWithValue":
            print("customValue",customValue)
            print("value to find and count",value[0])

            if is_numeric_dtype(df[columnName]) != True:
                raise SystemExit({"status": "Failure",
                                  "message": "Less Than is not applicable for the column selected"})
            else:
                try:
                    count = df[df[columnName] <= value[0]].count()
                    print(count[0])
                    outputCount = {"Custom Remove Count":count[0]}
                except:
                    FailureMessage = {"status": "Failure",
                                          "message": "Some"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)
        elif customValue =="lessthanWithColumn":
            print("customValue",customValue)
            print("value to find and count",value[0])
            try:
                if is_numeric_dtype(df[columnName]):
                    count = df[df[columnName] <= df[value[0]]].count()
                    print(count[0])
                    outputCount = {"Custom Remove Count":count[0]}
                else:
                    raise SystemExit({"status": "Failure",
                                      "message": "Less Than is not applicable for the column selected"})
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Less Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)
                
        elif customValue == "greaterthanWithValue":
            print("customValue",customValue)
            print(value[0])
            try:
                if is_numeric_dtype(df[columnName]):
                #df3 = df[~(df[columnName] <= value[0])]
                    count = df[df[columnName] >= value[0]].count()
                    print(count[0])
                    outputCount = {"Custom Remove Count":count[0]}
                else:
                    raise SystemExit({"status": "Failure",
                                      "message": "Greater Than is not applicable for the column selected"})
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Greater Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "greaterthanWithColumn":
            print("customValue",customValue)
            print(value[0])
            try:
                if is_numeric_dtype(df[columnName]):
                #df3 = df[~(df[columnName] <= value[0])]
                    count = df[df[columnName] >= df[value[0]]].count()
                    print(count[0])
                    outputCount = {"Custom Remove Count":count[0]}
                else:
                    raise SystemExit({"status": "Failure",
                                      "message": "Greater Than is not applicable for the column selected"})
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Greater Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "inbetweenWithValue":
            print("customValue",customValue)
            print(value[0], value[1])
            try:
                if is_numeric_dtype(df[columnName]):
                #df3 = df[~(df[columnName] <= value[0])]
                    count = sum((df[columnName] <= value[1]) & (df[columnName] >= value[0]))#df[df[columnName] < value[0] and df[columnName] > value[1]].count()
                    print(count)
                    outputCount = {"Custom Remove Count":count}
                else:
                    raise SystemExit({"status": "Failure",
                                      "message": "In Between is not applicable for the column selected"})
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "In Between is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)
        # elif customValue == "inbetweenWithColumn":
        #     print("customValue",customValue)
        #     print(value[0], value[1])
        #     try:
        #         if is_numeric_dtype(df[columnName]):
        #             #df3 = df[~(df[columnName] <= value[0])]
        #             count = sum((df[columnName] <= df[value[0]]) & (df[columnName] >= df[value[0]]))#df[df[columnName] < value[0] and df[columnName] > value[1]].count()
        #             print(count)
        #             outputCount = {"Custom Remove Count":count}
        #         else:
        #             raise SystemExit({"status": "Failure",
        #                               "message": "In Between is not applicable for the column selected"})
        #     except:
        #         FailureMessage = {"status": "Failure",
        #                               "message": "In Between is not applicable for the column selected"}
        #         FailureMessageJson = json.dumps(FailureMessage)
        #         print(FailureMessageJson)
        #         return json.dumps(FailureMessage)

        elif customValue == "missing":
            print("customValue",customValue)

            try:
                #if is_numeric_dtype(df[columnName]):
                #df3 = df[~(df[columnName] <= value[0])]
                #df.loc[(df[columnName] == '') & (df[columnName] == ' ') &(df[columnName] == 'nan') &(df[columnName] == 'NAN') &(df[columnName] == 'Null') &(df[columnName] == 'NULL')]
                count1 = (df[columnName].values == ' ').sum()
                count2 = (df[columnName].values == 'nan').sum()
                count3 = (df[columnName].values == 'NULL').sum()
                count4 = (df[columnName].values == 'NAN').sum()
                count5 = (df[columnName].values == 'Null').sum()
                count6 = (df[columnName].values == '').sum()
                count7 = (df[columnName].values == None).sum()
                count8 = (df[columnName].values == 'None').sum()
                #count = df[columnName].isna().sum()#df[df[columnName] < value[0] and df[columnName] > value[1]].count()
                count = count1+count2+count3+count4+count5+count6+count7+count8
                print(count)
                outputCount = {"Custom Remove Count":count}
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

                
        elif customValue == "startswith":
            print("customValue",customValue)
            print(value[0])
            try:
                count = df.loc[df[columnName].astype(str).str.startswith(value[0]), columnName].count()
                print(count)
                #print(count[0])
                outputCount = {"Custom Remove Count":count}
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "REstartswith" or customValue == "REendswith" or customValue == "REcontains":
            print("customValue",customValue)
            print(value[0])
            regex_str = value[0]
            try:
                if ('{' in regex_str) | ('}' in regex_str):
                    print("pattern: ",regex_str)
                #if ('{' in regex_str) | ('}' in regex_str):
                    df_mask = replace_regex(columnName, regex_str, "valueToReplace", df, method='remove', pattern=True)
                else:
                    print("regex: ", regex_str)
                    df_mask = replace_regex(columnName, regex_str, "valueToReplace", df, method='remove', pattern=False)
                print(df_mask)
                count = len(df)-len(df_mask)
                print(count)
                #print(count[0])
                outputCount = {"Custom Remove Count":count}
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "endswith":
            print("customValue",customValue)
            print(value[0])
            #if is_numeric_dtype(df[columnName]):
            try:
                count = df.loc[df[columnName].astype(str).str.endswith(value[0]), columnName].count()
                #df[columnName] = df[columnName].str.count(value)
                print(count)
                # print(count[0])
                outputCount = {"Custom Remove Count": count}
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)
                    
                    
        elif customValue == "contains":
            print("customValue",customValue)
            print(value[0])
            #if is_numeric_dtype(df[columnName]):
            try:
                #df3 = df[~(df[columnName] <= value[0])]
                count = df.loc[df[columnName].astype(str).str.contains(value[0]), columnName].count()
                # df[columnName] = df[columnName].str.count(value)
                print(count)
                # print(count[0])
                outputCount = {"Custom Remove Count": count}
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

                    
        elif customValue == "distinct":
            print("customValue",customValue)
            print(value[0])
            occurances = value[0]
            #if is_numeric_dtype(df[columnName]):
            try:
                v = df[columnName].value_counts()
                df1 = df[df[columnName].isin(v.index[v == occurances])]
                count = len(df1)
                # #df3 = df[~(df[columnName] <= value[0])]
                # df1 = df.copy()
                # df1[columnName] = df1[columnName].astype(str)
                # count = len(df1[df1[columnName] == value[0]])#df[columnName].nunique()
                print(count)
                outputCount = {"Custom Remove Count":count}
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "missmatchedDatatype":
            print("customValue",customValue)
            print(value[0])
            datatypeDict = {columnName: value[0]}
            #if is_numeric_dtype(df[columnName]):
            try:
                invalid_serie = technical_quality_score(df[[columnName]], datatypeDict,method='count')

                count = invalid_serie[columnName]
                print(count)
                outputCount = {"Custom Remove Count":count}
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        json_object = json.dumps(outputCount, default=np_encoder)
        list = {"status": "Success", "message": "Successfully completed count of custom remove request",
                "data": json_object}
        print(list)

        return json.dumps(list)
    # except OSError as err:
    #     print("OS error: {0}".format(err))
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Delta Table entered doesnt exists, Please enter a valid Delta Table."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)


#countRemovedef("1", "newPSNAN", "s01", "3", "222", "1","lessthan", "DISTRICTID", [1100030])