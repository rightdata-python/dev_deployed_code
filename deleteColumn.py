import json
import pyspark
from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
from sklearn.model_selection import StratifiedShuffleSplit
from pyspark.sql.functions import lit, rand, when
#from properties import path
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def deleteColumndef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnNames):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    DeltaTable = path + "/" + inputDeltaTable
    print("Reading Universal data")
    # try:
    #     Data = spark.read.format("delta").load(DeltaTable)
    #     # columnNames = Data.columns
    #     print("Column names in Delta table: ", Data.columns)
    #     drop_list = []
    #     for x in columnNames:
    #         drop_list.append(x['columnName'])
    #
    #     print("Drop column list", drop_list)
    #     # cols = ("firstname", "middlename", "lastname")
    #
    #     df = Data.drop(*drop_list)
    #     # df = Data.select([column for column in Data.columns if column not in drop_list])
    #     print("Column names of new Delta table: ", df.columns)
    #     outputDeltaTablepath = path + "/" + outputDeltaTable
    #     df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
    #     list = {"status": "Success", "message": "Successfully deleted selected column",
    #             "data": {"inputsteps": Data.count(), "outputsteps": df.count()}}
    #     return json.dumps(list)
    #
    # except:
    #     FailureMessage = {"status": "Failure",
    #                       "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
    #     FailureMessageJson = json.dumps(FailureMessage)
    #     print(FailureMessageJson)
    #     return json.dumps(FailureMessage)
    Data = spark.read.format("delta").load(DeltaTable)
    # columnNames = Data.columns
    print("Column names in Delta table: ", Data.columns)
    drop_list = [columnNames]
    # for x in columnNames:
    #     drop_list.append(x['columnName'])

    print("Drop column list", drop_list)
    # cols = ("firstname", "middlename", "lastname")

    df = Data.drop(*drop_list)
    # df = Data.select([column for column in Data.columns if column not in drop_list])
    print("Column names of new Delta table: ", df.columns)
    outputDeltaTablepath = path + "/" + outputDeltaTable
    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
    list = {"status": "Success", "message": "Successfully deleted selected column",
            "data": {"inputsteps": Data.count(), "outputsteps": df.count()}}
    return json.dumps(list)

deleteColumndef("1","newPSNAN","s01","3","444", "1","values")

