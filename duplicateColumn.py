import pyspark
from properties import path
from deltalake import DeltaTable
import json
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def duplicateColumndef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnName,newColumnName):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    inputDeltaTablepath = path + "/" + inputDeltaTable

    try:
        print("Reading data")
        #df = spark.read.format("delta").load(DeltaTable)
        # Data = spark.read.format("delta").load(inputDeltaTablepath)
        # Data.withColumn(newColumnName, Data[columnName])
        # print("Column names in Delta table: ", Data.columns)
        pandasDF = DeltaTable(inputDeltaTablepath).to_pandas()
        print("No of Rows, Columns and Column names in input Data: ", len(pandasDF), ";",
              len(pandasDF.columns), ";", pandasDF.columns)
        pandasDF[newColumnName] = pandasDF[columnName]
        #pandasDF[newColumnName] = pandasDF.loc[:, columnName]
        print("No of Rows, Columns and Column names in output Data: ", len(pandasDF), ";",
              len(pandasDF.columns), ";", pandasDF.columns)
        Data = spark.createDataFrame(pandasDF)
        Data.show()
        outputDeltaTablepath = path + "/" + outputDeltaTable
        Data.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully created a duplicate column",
                "data": {"inputsteps": len(pandasDF), "outputsteps": Data.count()}}
        return json.dumps(list)

    # except OSError as err:
    #      print("OS error: {0}".format(err))
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Some thing went wrong, Please check the inputs!"}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)