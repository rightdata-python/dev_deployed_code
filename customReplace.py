import pyspark
import pandas as pd
import numpy as np
from pandas.api.types import is_numeric_dtype
from properties import path
import re
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
import json
from deltalake import DeltaTable
from Regex import replace_regex
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def customReplacedef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,condition,columnName, parameter,value,valueToReplace, replaceColumn):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Reading data")
    try:
        #df = spark.read.format("delta").load(DeltaTable)
        pandasDF = DeltaTable(inputDeltaTablepath).to_pandas()
        # columnNames = Data.columns
        print("No of Columns and Column names in Delta table: ", len(pandasDF.columns),";",pandasDF.columns)
        print("No. of records in Data:", len(pandasDF))
        print(pandasDF)
        #Covered :  Less than equal to, Greater than equal to,Distinct, Contains, Starts with, Ends with,Missing, In between
        #mismatch datatype,
        print("Datatype of column:",pandasDF.dtypes[columnName])
        if condition == "lessthanWithValue":
            print("condition",condition, " ", value[0])
            try:
                if is_numeric_dtype(pandasDF[columnName]) == True:
                    if parameter == "customValue":
                        # a = np.array(pandasDF[columnName].values.tolist())
                        # pandasDF[columnName] = np.where(a <= value[0], valueToReplace, a).tolist()
                        #value ={int}{5}
                        pandasDF.loc[pandasDF[columnName] <= value[0], columnName] = int(valueToReplace)
                        #valueToReplace = {int}{10}
                    elif parameter == "predefinedValue":
                        if valueToReplace == "mean":
                            mean = pandasDF[columnName].mean()
                            pandasDF.loc[pandasDF[columnName] <= value[0], columnName] = mean
                        elif valueToReplace == "median":
                            median = pandasDF[columnName].median()
                            pandasDF.loc[pandasDF[columnName] <= value[0], columnName] = median
                        elif valueToReplace == "mode":
                            mode = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                            pandasDF.loc[pandasDF[columnName] <= value[0], columnName] = mode
                        elif valueToReplace == "stdDeviation":
                            std = pandasDF[columnName].std()
                            pandasDF.loc[pandasDF[columnName] <= value[0], columnName] = std
                        elif valueToReplace == "copyFromColumn":
                            pandasDF[columnName] = np.where(pandasDF[columnName] < value[0], pandasDF[replaceColumn], pandasDF[columnName])
                    elif parameter == "regularExpression":
                        pandasDF.loc[pandasDF[columnName] <= value[0], columnName] = int(valueToReplace)
                    print(pandasDF)

                    print("Datatype of column:",pandasDF.dtypes[columnName])
                    print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                    print("No. of records in Dataframe:", len(pandasDF))
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Less Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        if condition == "lessthanWithColumn":
            print("condition",condition, " ", value[0])
            try:
                if is_numeric_dtype(pandasDF[columnName]) == True:
                    if parameter == "customValue":
                        # a = np.array(pandasDF[columnName].values.tolist())
                        # pandasDF[columnName] = np.where(a <= value[0], valueToReplace, a).tolist()
                        #value ={int}{5}
                        pandasDF[columnName] = np.where(pandasDF[columnName] <= pandasDF[value[0]],
                                                        int(valueToReplace) , pandasDF[columnName])
                        #pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = int(valueToReplace)
                        #valueToReplace = {int}{10}
                    elif parameter == "predefinedValue":
                        if valueToReplace == "mean":
                            mean = pandasDF[columnName].mean()
                            pandasDF[columnName] = np.where(pandasDF[columnName] <= pandasDF[value[0]],
                                                            mean, pandasDF[columnName])
                            #pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = mean
                        elif valueToReplace == "median":
                            median = pandasDF[columnName].median()
                            pandasDF[columnName] = np.where(pandasDF[columnName] <= pandasDF[value[0]],
                                                            median, pandasDF[columnName])
                            #pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = median
                        elif valueToReplace == "mode":
                            mode = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                            pandasDF[columnName] = np.where(pandasDF[columnName] <= pandasDF[value[0]],
                                                            mode, pandasDF[columnName])
                            #pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = mode
                        elif valueToReplace == "stdDeviation":
                            std = pandasDF[columnName].std()
                            pandasDF[columnName] = np.where(pandasDF[columnName] <= pandasDF[value[0]],
                                                            std, pandasDF[columnName])
                            #pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = std
                        elif valueToReplace == "copyFromColumn":
                            pandasDF[columnName] = np.where(pandasDF[columnName] <= pandasDF[value[0]], pandasDF[replaceColumn], pandasDF[columnName])

                    print(pandasDF)

                    print("Datatype of column:",pandasDF.dtypes[columnName])
                    print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                    print("No. of records in Dataframe:", len(pandasDF))
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Less Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif condition == "greaterthanWithValue":
            print("condition", condition)
            print(value[0])
            try:
                if is_numeric_dtype(pandasDF[columnName]):
                    if parameter == "customValue":
                        # a = np.array(pandasDF[columnName].values.tolist())
                        # pandasDF[columnName] = np.where(a >= value[0], valueToReplace, a).tolist()
                        pandasDF.loc[pandasDF[columnName] >= value[0], columnName] = int(valueToReplace)
                    elif parameter == "predefinedValue":
                        if valueToReplace == "mean":
                            pandasDF.loc[pandasDF[columnName] >= value[0], columnName] = pandasDF[columnName].mean()
                        elif valueToReplace == "median":
                            pandasDF.loc[pandasDF[columnName] >= value[0], columnName] = pandasDF[columnName].median()
                        elif valueToReplace == "mode":
                            mode = pandasDF[columnName].mode().iat[0]  # pandasDF[columnName].mode()
                            pandasDF.loc[pandasDF[columnName] >= value[0], columnName] = mode
                        elif valueToReplace == "stdDeviation":
                            pandasDF.loc[pandasDF[columnName] >= value[0], columnName] = pandasDF[columnName].std()
                        elif valueToReplace == "copyFromColumn":
                            pandasDF[columnName] = np.where(pandasDF[columnName] > value[0], pandasDF[replaceColumn], pandasDF[columnName])

                    # df3.loc[(df3[columnName] < value[0]), columnName] = valueToReplace# not applying on data
                    print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                    print("No. of records in Dataframe:", len(pandasDF))
                    print(pandasDF)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Greater than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)
        elif condition == "greaterthanWithColumn":
            print("condition", condition)
            print(value[0])
            try:
                if is_numeric_dtype(pandasDF[columnName]):
                    if is_numeric_dtype(pandasDF[columnName]) == True:
                        if parameter == "customValue":
                            pandasDF[columnName] = np.where(pandasDF[columnName] >= pandasDF[value[0]],
                                                            int(valueToReplace), pandasDF[columnName])
                            # pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = int(valueToReplace)
                            # valueToReplace = {int}{10}
                        elif parameter == "predefinedValue":
                            if valueToReplace == "mean":
                                mean = pandasDF[columnName].mean()
                                pandasDF[columnName] = np.where(pandasDF[columnName] >= pandasDF[value[0]],
                                                                mean, pandasDF[columnName])
                                # pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = mean
                            elif valueToReplace == "median":
                                median = pandasDF[columnName].median()
                                pandasDF[columnName] = np.where(pandasDF[columnName] >= pandasDF[value[0]],
                                                                median, pandasDF[columnName])
                                # pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = median
                            elif valueToReplace == "mode":
                                mode = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                                pandasDF[columnName] = np.where(pandasDF[columnName] >= pandasDF[value[0]],
                                                                mode, pandasDF[columnName])
                                # pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = mode
                            elif valueToReplace == "stdDeviation":
                                std = pandasDF[columnName].std()
                                pandasDF[columnName] = np.where(pandasDF[columnName] >= pandasDF[value[0]],
                                                                std, pandasDF[columnName])
                                # pandasDF.loc[pandasDF[columnName] <= pandasDF[value[0]], columnName] = std
                            elif valueToReplace == "copyFromColumn":
                                pandasDF[columnName] = np.where(pandasDF[columnName] >= pandasDF[value[0]],
                                                                pandasDF[replaceColumn], pandasDF[columnName])

                    # df3.loc[(df3[columnName] < value[0]), columnName] = valueToReplace# not applying on data
                    print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                    print("No. of records in Dataframe:", len(pandasDF))
                    print(pandasDF)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Greater than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif condition == "inbetweenWithValue":
            print("condition", condition)
            print(value[0])
            try:
                if is_numeric_dtype(pandasDF[columnName]):
                    if parameter == "customValue":
                        print(condition,parameter)
                        pandasDF[columnName] = np.where((pandasDF[columnName] > value[0]) & (pandasDF[columnName] < value[1]),
                                                        int(valueToReplace), pandasDF[columnName])
                    elif parameter == "predefinedValue":
                        if valueToReplace == "mean":
                            mean1 = pandasDF[columnName].mean()
                            pandasDF[columnName] = np.where(
                                (pandasDF[columnName] > value[0]) & (pandasDF[columnName] < value[1]),
                                mean1, pandasDF[columnName])
                        elif valueToReplace == "median":
                            median1 = pandasDF[columnName].median()
                            pandasDF[columnName] = np.where(
                                (pandasDF[columnName] > value[0]) & (pandasDF[columnName] < value[1]),
                                median1, pandasDF[columnName])
                        elif valueToReplace == "mode":
                            mode1 = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                            pandasDF[columnName] = np.where(
                                (pandasDF[columnName] > value[0]) & (pandasDF[columnName] < value[1]),
                                mode1, pandasDF[columnName])
                        elif valueToReplace == "stdDeviation":
                            std1 = pandasDF[columnName].std()
                            pandasDF[columnName] = np.where(
                                (pandasDF[columnName] > value[0]) & (pandasDF[columnName] < value[1]),
                                std1, pandasDF[columnName])
                        elif valueToReplace == "copyFromColumn":
                            pandasDF[columnName] = np.where(
                                (pandasDF[columnName] > value[0]) & (pandasDF[columnName] < value[1]),
                                pandasDF[replaceColumn], pandasDF[columnName])
                print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                print("No. of records in Dataframe:", len(pandasDF))
                print(pandasDF)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "In between is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif condition == "inbetweenWithColumn":
            print("condition", condition)
            print(value[0],value[1])
            try:
                if is_numeric_dtype(pandasDF[columnName]):
                                if parameter == "customValue":
                                    print(condition, parameter)
                                    pandasDF[columnName] = np.where(
                                        (pandasDF[columnName] > pandasDF[value[0]]) & (pandasDF[columnName] < pandasDF[value[1]]),
                                        int(valueToReplace), pandasDF[columnName])
                                elif parameter == "predefinedValue":
                                    if valueToReplace == "mean":
                                        mean1 = pandasDF[columnName].mean()
                                        pandasDF[columnName] = np.where(
                                            (pandasDF[columnName] > pandasDF[value[0]]) & (pandasDF[columnName] < pandasDF[value[1]]),
                                            mean1, pandasDF[columnName])
                                    elif valueToReplace == "median":
                                        median1 = pandasDF[columnName].median()
                                        pandasDF[columnName] = np.where(
                                            (pandasDF[columnName] > pandasDF[value[0]]) & (pandasDF[columnName] < pandasDF[value[1]]),
                                            median1, pandasDF[columnName])
                                    elif valueToReplace == "mode":
                                        mode1 = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                                        pandasDF[columnName] = np.where(
                                            (pandasDF[columnName] > pandasDF[value[0]]) & (pandasDF[columnName] < pandasDF[value[1]]),
                                            mode1, pandasDF[columnName])
                                    elif valueToReplace == "stdDeviation":
                                        std1 = pandasDF[columnName].std()
                                        pandasDF[columnName] = np.where(
                                            (pandasDF[columnName] > pandasDF[value[0]]) & (pandasDF[columnName] < pandasDF[value[1]]),
                                            std1, pandasDF[columnName])
                                    elif valueToReplace == "copyFromColumn":
                                        pandasDF[columnName] = np.where(
                                            (pandasDF[columnName] > pandasDF[value[0]]) & (pandasDF[columnName] < pandasDF[value[1]]),
                                            pandasDF[replaceColumn], pandasDF[columnName])
                                    # print(condition)
                                    # pandasDF.loc[pandasDF[columnName] < pandasDF[value[0]], columnName] = int(valueToReplace)
                                    # pandasDF.loc[pandasDF[columnName] > pandasDF[value[1]], columnName] = int(valueToReplace)
                                    # a = np.array(pandasDF[columnName].values.tolist())
                                    # pandasDF[columnName] = np.where(a < value[0], valueToReplace, a).tolist()
                                    # pandasDF[columnName] = np.where(a > value[1], valueToReplace, a).tolist()
                                # elif parameter == "predefinedValue":
                                #     if valueToReplace == "mean":
                                #         mean1 = pandasDF[columnName].mean()
                                #         pandasDF.loc[pandasDF[columnName] < pandasDF[value[0]], columnName] = mean1
                                #         pandasDF.loc[pandasDF[columnName] > pandasDF[value[1]], columnName] = mean1
                                #     elif valueToReplace == "median":
                                #         median1 = pandasDF[columnName].median()
                                #         pandasDF.loc[pandasDF[columnName] > pandasDF[value[0]], columnName] = median1
                                #         pandasDF.loc[pandasDF[columnName] < pandasDF[value[1]], columnName] = median1
                                #     elif valueToReplace == "mode":
                                #         mode1 = pandasDF[columnName].mode()
                                #         pandasDF.loc[pandasDF[columnName] > pandasDF[value[0]], columnName] = mode1
                                #         pandasDF.loc[pandasDF[columnName] < pandasDF[value[1]], columnName] = mode1
                                #     elif valueToReplace == "stdDeviation":
                                #         std1 = pandasDF[columnName].std()
                                #         pandasDF.loc[pandasDF[columnName] > pandasDF[value[0]], columnName] = std1
                                #         pandasDF.loc[pandasDF[columnName] < pandasDF[value[1]], columnName] = std1
                                #     elif valueToReplace == "copyFromColumn":
                                #         pandasDF[columnName] = np.where(pandasDF[columnName] > pandasDF[value[0]],pandasDF[replaceColumn])
                                #         pandasDF[columnName] = np.where(pandasDF[columnName] > pandasDF[value[1]],pandasDF[replaceColumn])

                print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                print("No. of records in Dataframe:", len(pandasDF))
                print(pandasDF)
            # except OSError as err:
            #     print("OS error: {0}".format(err))
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "In between is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif condition == "contains":  # value should always be in form ["3"] even for the int columns
            print("condition", condition)
            # df[columnName] = df[columnName].astype(str)
            containsvalue = value[0]
            print("containsvalue", containsvalue)
            try:
                if parameter == "customValue":
                    pandasDF[columnName] = pandasDF[columnName].str.replace(value[0], valueToReplace)## 99999883748573 --> 00000883748573
                    #pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = valueToReplace
                elif parameter == "predefinedValue":
                    if valueToReplace == "mean":
                        mean = pandasDF[columnName].mean()
                        print("mean: ", mean)
                        #pandasDF[columnName] = pandasDF[columnName].str.replace(value[0], mean)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = mean
                    elif valueToReplace == "median":
                        median = pandasDF[columnName].median()
                        print("median: ", median)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = median
                    elif valueToReplace == "mode":
                        mode = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                        print("mode: ", mode)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = mode
                    elif valueToReplace == "stdDeviation":
                        std = pandasDF[columnName].std()
                        print("std: ", std)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = std
                    elif valueToReplace == "copyFromColumn":
                        pandasDF[columnName] = np.where(pandasDF[columnName].astype(str).str.contains(value[0]), pandasDF[replaceColumn],
                                                        pandasDF[columnName])

                print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                print("No. of records in Dataframe:", len(pandasDF))
                print(pandasDF)
            except OSError as err:
                print("OS error: {0}".format(err))
            # except:
            #     FailureMessage = {"status": "Failure",
            #                   "message": "Some thing went wrong please check the inputs"}
            #     FailureMessageJson = json.dumps(FailureMessage)
            #     print(FailureMessageJson)
            #     return json.dumps(FailureMessage)

        elif condition == "startswith":  # value should always be in form ["3"] even for the int columns
            print("condition", condition)
            try:
                if parameter == "customValue":
                    pandasDF[columnName] = pandasDF[columnName].str.replace(value[0], valueToReplace)
                    #pandasDF.loc[pandasDF[columnName].astype(str).str.startswith(value[0]), columnName] = valueToReplace
                elif parameter == "predefinedValue":
                    if valueToReplace == "mean":
                        mean = pandasDF[columnName].mean()
                        print("mean: ", mean)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.startswith(value[0]), columnName] = mean
                    elif valueToReplace == "median":
                        median = pandasDF[columnName].median()
                        print("median: ", median)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.startswith(value[0]), columnName] = median
                    elif valueToReplace == "mode":
                        mode = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                        print("mode: ", mode)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.startswith(value[0]), columnName] = mode

                    elif valueToReplace == "stdDeviation":
                        std = pandasDF[columnName].std()
                        print("std: ", std)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.startswith(value[0]), columnName] = std
                    elif valueToReplace == "copyFromColumn":
                        pandasDF[columnName] = np.where(pandasDF[columnName].astype(str).str.startswith(value[0]), pandasDF[replaceColumn],
                                                        pandasDF[columnName])


                print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                print("No. of records in Dataframe:", len(pandasDF))
                print(pandasDF)
            except:
                FailureMessage = {"status": "Failure",
                              "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif condition == "distinct":  # value should always be in form ["3"] even for the int columns
            print("condition", condition)
            occurances = value[0]
            #pandasDF[columnName] = pandasDF[columnName].astype(str)
            print("occurances",occurances)

            v = pandasDF[columnName].value_counts()
            #valueV = (v.values() == 3)
            more_than_1 = list(v[v == occurances].index)
            more_than_1 = [str(value) for value in more_than_1]
            regex_replace = '|'.join(more_than_1)
            #more_than_1_rows = DF[DF['col1'].isin(more_than_1)
            print(regex_replace)

            try:
                if len(more_than_1) != 0:
                    if parameter == "customValue":
                        #pandasDF[columnName] = pandasDF[columnName].astype(str)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(regex_replace, regex = True), columnName] = valueToReplace
                        #pandasDF[columnName].loc[pandasDF[columnName].isin(v == occurances)] = valueToReplace
                        #pandasDF[columnName] = np.where(((pandasDF.groupby(columnName).size().Value >= occurances).index),valueToReplace,pandasDF[columnName])

                        # print(df_mask)
                        # pandasDF.loc[df_mask[columnName], columnName] = valueToReplace
                        #pandasDF.loc[pandasDF[columnName].astype(str).str.startswith(value[0]), columnName] = valueToReplace
                    elif parameter == "predefinedValue":
                        if valueToReplace == "mean":
                            mean = pandasDF[columnName].mean()
                            print(mean)
                            pandasDF.loc[pandasDF[columnName].astype(str).str.contains(regex_replace, regex = True), columnName] = mean
                            #pandasDF.loc[pandasDF[columnName] == more_than_1[0], columnName] = valueToReplace1
                            #pandasDF.loc[pandasDF[columnName].astype(str).str.contains(more_than_1[0]), columnName] = valueToReplace1
                            #pandasDF[columnName].loc[pandasDF[columnName].isin(v.index[v == occurances])] = valueToReplace1
                        elif valueToReplace == "median":
                            median = pandasDF[columnName].median()
                            print(median)
                            pandasDF.loc[pandasDF[columnName].astype(str).str.contains(regex_replace, regex = True), columnName] = median
                            #pandasDF.loc[pandasDF[columnName].astype(str).str.contains(more_than_1[0]), columnName] = valueToReplace1
                            #pandasDF[columnName].loc[pandasDF[columnName].isin(v.index[v == occurances])] = valueToReplace1
                        elif valueToReplace == "mode":
                            mode = pandasDF[columnName].mode().iat[0]  # pandasDF[columnName].mode()
                            #valueToReplace1 = pandasDF[columnName].mode()
                            print(mode)
                            pandasDF.loc[pandasDF[columnName].astype(str).str.contains(regex_replace, regex = True), columnName] = mode
                            #pandasDF.loc[pandasDF[columnName].astype(str).str.contains(more_than_1[0]), columnName] = mode
                            #pandasDF[columnName].loc[pandasDF[columnName].isin(v.index[v == occurances])] = mode
                        elif valueToReplace == "stdDeviation":
                            stdDeviation = pandasDF[columnName].std()
                            print(stdDeviation)
                            pandasDF.loc[pandasDF[columnName].astype(str).str.contains(regex_replace, regex = True), columnName] = stdDeviation
                            #pandasDF.loc[pandasDF[columnName].astype(str).str.contains(more_than_1[0]), columnName] = valueToReplace1
                        elif valueToReplace == "copyFromColumn":
                            mask_values = pandasDF[columnName].astype(str).str.contains(regex_replace, regex=True)
                            pandasDF.loc[mask_values, columnName] = pandasDF.loc[mask_values, replaceColumn]
                            # mask_index = pandasDF[columnName].isin(v.index[v == occurances])
                            # pandasDF.loc[mask_index, columnName] = pandasDF.loc[mask_index, replaceColumn]


                            # pandasDF[columnName] = pandasDF[columnName].astype(str)
                            # pandasDF[columnName] = np.where(pandasDF[columnName].astype(str).str.startswith(value[0]), pandasDF[replaceColumn],
                            #                                 pandasDF[columnName])
                    print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                    print("No. of records in Dataframe:", len(pandasDF))
                    print(pandasDF)
                else:
                    raise SystemExit({"status": "Failure",
                                      "message": "Number of occurances does not match with any value in column selected."})


            except:
                    FailureMessage = {"status": "Failure",
                                  "message": "Number of occurances does not match with any value in column selected / Some thing went wrong please check the inputs"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)
            # else:
            #     print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
            #     print("No. of records in Dataframe:", len(pandasDF))
            #     print(pandasDF)
            # except OSError as err:
            #     print("OS error: {0}".format(err))
            # except:
            #     FailureMessage = {"status": "Failure",
            #                   "message": "Some thing went wrong please check the inputs"}
            #     FailureMessageJson = json.dumps(FailureMessage)
            #     print(FailureMessageJson)
            #     return json.dumps(FailureMessage)

        elif condition == "endswith":  # value should always be in form ["3"] even for the int columns
            print("condition", condition)
            try:
                if parameter == "customValue":
                    pandasDF[columnName] = pandasDF[columnName].str.replace(value[0], valueToReplace)
                    #pandasDF.loc[pandasDF[columnName].astype(str).str.endswith(value[0]), columnName] = valueToReplace
                elif parameter == "predefinedValue":
                    if valueToReplace == "mean":
                        mean = pandasDF[columnName].mean()
                        print("mean: ", mean)

                        pandasDF.loc[pandasDF[columnName].astype(str).str.endswith(value[0]), columnName] = mean
                    elif valueToReplace == "median":
                        median = pandasDF[columnName].median()
                        print("median: ", median)

                        pandasDF.loc[pandasDF[columnName].astype(str).str.endswith(value[0]), columnName] = median
                    elif valueToReplace == "mode":
                        mode = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                        print("mode: ", mode)

                        #print("freq_mode = ",pandasDF[columnName].mode().iat[0])
                        pandasDF.loc[pandasDF[columnName].astype(str).str.endswith(value[0]), columnName] = mode
                        # regex_str = value[0]+'$'
                        # print("new value: ",regex_str)
                        # mask = pandasDF[columnName].astype(str).str.contains(regex_str, regex=True, flags=re.IGNORECASE)
                        # pandasDF.loc[mask, columnName] = mode[1]
                    elif valueToReplace == "stdDeviation":
                        std = pandasDF[columnName].std()
                        print("std : ", std)

                        pandasDF.loc[pandasDF[columnName].astype(str).str.endswith(value[0]), columnName] = std
                    elif valueToReplace == "copyFromColumn":
                        pandasDF[columnName] = np.where(pandasDF[columnName].astype(str).str.endswith(value[0]), pandasDF[replaceColumn],
                                                        pandasDF[columnName])


                print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                print("No. of records in Dataframe:", len(pandasDF))
                print(pandasDF)
            except:
                FailureMessage = {"status": "Failure",
                              "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif condition == "missing":
            print("condition", condition)

            try:
                if parameter == "customValue":
                    pandasDF[columnName] = pandasDF[columnName].fillna(valueToReplace)
                elif parameter == "predefinedValue":
                    if valueToReplace == "mean":
                        pandasDF[columnName] = pandasDF[columnName].fillna(pandasDF[columnName].mean(), inplace=True)
                    elif valueToReplace == "median":
                        pandasDF[columnName] = pandasDF[columnName].fillna(pandasDF[columnName].median(), inplace=True)
                    elif valueToReplace == "mode":
                        mode = pandasDF[columnName].mode().iat[0]  # pandasDF[columnName].mode()
                        pandasDF[columnName] = pandasDF[columnName].fillna(mode, inplace=True)
                    elif valueToReplace == "stdDeviation":
                        pandasDF[columnName] = pandasDF[columnName].fillna(pandasDF[columnName].std(), inplace=True)
                    elif valueToReplace == "copyFromColumn":
                        pandasDF[columnName] = pandasDF[columnName].fillna(pandasDF[replaceColumn])

                print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                print("No. of records in Dataframe:", len(pandasDF))
                pandasDF[columnName] = pandasDF[columnName].astype(str)
                print(pandasDF)
            except:
                FailureMessage = {"status": "Failure",
                              "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif condition == "findandreplace":
            print("condition", condition)
            # df[columnName] = df[columnName].astype(str)
            containsvalue = value[0]

            print("findvalue", containsvalue)
            try:
                if parameter == "customValue":
                    pandasDF[columnName] = pandasDF[columnName].str.replace(value[0], valueToReplace)
                    # pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = valueToReplace
                elif parameter == "predefinedValue":
                    if valueToReplace == "mean":
                        mean = pandasDF[columnName].mean()
                        print("mean: ", mean)
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = mean
                    elif valueToReplace == "median":
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = pandasDF[
                            columnName].median()
                    elif valueToReplace == "mode":
                        mode = pandasDF[columnName].mode().iat[0]  # pandasDF[columnName].mode()
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = mode
                    elif valueToReplace == "stdDeviation":
                        pandasDF.loc[pandasDF[columnName].astype(str).str.contains(value[0]), columnName] = pandasDF[
                            columnName].std()
                    elif valueToReplace == "copyFromColumn":
                        pandasDF[columnName] = np.where(pandasDF[columnName].astype(str).str.contains(value[0]),
                                                        pandasDF[replaceColumn],
                                                        pandasDF[columnName])

                print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                print("No. of records in Dataframe:", len(pandasDF))
                print(pandasDF)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)
        elif condition == "REstartswith" or condition == "REendswith" or condition == "REcontains":
            print("condition", condition)
            print(value[0])
            regex_str = value[0]
            try:

                if parameter == "customValue":
                    if ('{' in regex_str) | ('}' in regex_str):
                        print("pattern: ", regex_str)
                        # if ('{' in regex_str) | ('}' in regex_str):

                        pandasDF = replace_regex(columnName, regex_str, valueToReplace, pandasDF, method='replace_all',pattern=True)
                    else:
                        print("regex: ", regex_str)
                        pandasDF = replace_regex(columnName, regex_str, valueToReplace, pandasDF, method='replace_all',pattern=False)

                elif parameter == "predefinedValue":
                        if valueToReplace == "mean":
                            mean = pandasDF[columnName].mean()
                            if ('{' in regex_str) | ('}' in regex_str):
                                print("pattern: ", regex_str)
                                # if ('{' in regex_str) | ('}' in regex_str):
                                pandasDF = replace_regex(columnName, regex_str, mean, pandasDF,method='replace_all', pattern=True)
                            else:
                                print("regex: ", regex_str)
                                pandasDF = replace_regex(columnName, regex_str, mean, pandasDF,method='replace_all', pattern=False)
                        elif valueToReplace == "median":
                            median = pandasDF[columnName].median()
                            if ('{' in regex_str) | ('}' in regex_str):
                                print("pattern: ", regex_str)
                                # if ('{' in regex_str) | ('}' in regex_str):
                                pandasDF = replace_regex(columnName, regex_str, median, pandasDF,method='replace_all', pattern=True)
                            else:
                                print("regex: ", regex_str)
                                pandasDF = replace_regex(columnName, regex_str, median, pandasDF,method='replace_all', pattern=False)
                        elif valueToReplace == "mode":
                            mode = pandasDF[columnName].mode().iat[0]#pandasDF[columnName].mode()
                            if ('{' in regex_str) | ('}' in regex_str):
                                print("pattern: ", regex_str)
                                # if ('{' in regex_str) | ('}' in regex_str):
                                pandasDF = replace_regex(columnName, regex_str, mode, pandasDF,method='replace_all', pattern=True)
                            else:
                                print("regex: ", regex_str)
                                pandasDF = replace_regex(columnName, regex_str, mode, pandasDF,method='replace_all', pattern=False)

                        elif valueToReplace == "stdDeviation":
                            std = pandasDF[columnName].std()
                            if ('{' in regex_str) | ('}' in regex_str):
                                print("pattern: ", regex_str)
                                # if ('{' in regex_str) | ('}' in regex_str):
                                pandasDF = replace_regex(columnName, regex_str, std, pandasDF,method='replace_all', pattern=True)
                            else:
                                print("regex: ", regex_str)
                                pandasDF = replace_regex(columnName, regex_str, std, pandasDF,method='replace_all', pattern=False)

                        elif valueToReplace == "copyFromColumn":
                            if ('{' in regex_str) | ('}' in regex_str):
                                print("pattern: ", regex_str)
                                # if ('{' in regex_str) | ('}' in regex_str):
                                pandasDF = replace_regex(columnName, regex_str, pandasDF[replaceColumn], pandasDF, method='replace_all',pattern=True)
                            else:
                                print("regex: ", regex_str)
                                pandasDF = replace_regex(columnName, regex_str, pandasDF[replaceColumn], pandasDF, method='replace_all',pattern=False)

                print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
                print("No. of records in Dataframe:", len(pandasDF))
                print(pandasDF)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)
            # print("condition", condition)
            # print(value[0])
            # #value = value[0]
            # try:
            #     if parameter == "customValue":
            #         pandasDF[columnName] = pandasDF[columnName].replace({value[0]: valueToReplace})
            #     elif parameter == "predefinedValue":
            #         if valueToReplace == "mean":
            #             mean1 = pandasDF[columnName].mean()
            #             pandasDF[columnName] = pandasDF[columnName].replace({value[0]: mean1})
            #         elif valueToReplace == "median":
            #             median1 = pandasDF[columnName].median()
            #             pandasDF[columnName] = pandasDF[columnName].replace({value[0]: median1})
            #         elif valueToReplace == "mode":
            #             mode1 = pandasDF[columnName].mode()
            #             pandasDF[columnName] = pandasDF[columnName].replace({value[0]: mode1})
            #         elif valueToReplace == "stdDeviation":
            #             std1 = pandasDF[columnName].std()
            #             pandasDF[columnName] = pandasDF[columnName].replace({value[0]: std1})
            #         elif valueToReplace == "copyFromColumn":
            #             pandasDF[columnName] = np.where(pandasDF[columnName] == value[0], pandasDF[replaceColumn],pandasDF[columnName])
            #
            #
            #     pandasDF[columnName] = pandasDF[columnName].replace([value],valueToReplace)
            #     print("values after replacing:", pandasDF[columnName].tolist())
            #     print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
            #     print("No. of records in Dataframe:", len(pandasDF))
            #     print(pandasDF)
            # except:
            #     FailureMessage = {"status": "Failure","message": "Some thing went wrong please check the inputs"}
            #     FailureMessageJson = json.dumps(FailureMessage)
            #     print(FailureMessageJson)
            #     return json.dumps(FailureMessage)
        print("values after replacing:", pandasDF[columnName].tolist())
        pandasDF[columnName] = pandasDF[columnName].astype(str)
        df1 = spark.createDataFrame(pandasDF)
        df1.show()
        outputDeltaTablepath = path + "/" + outputDeltaTable
        df1.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        message = {"status": "Success", "message": "Successfully replaced the values from condition in selected column",
                "data": {"inputsteps": len(pandasDF), "outputsteps": df1.count()}}
        return json.dumps(message)
    except OSError as err:
        print("OS error: {0}".format(err))
    # except:
    #     FailureMessage = {"status": "Failure",
    #                       "message": "Delta Table entered doesnt exists, Please enter a valid Delta Table."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
    #     FailureMessageJson = json.dumps(FailureMessage)
    #     print(FailureMessageJson)
    #     return json.dumps(FailureMessage)

#customReplacedef("1","newPSNAN","s01","3", "222","1","lessThan","OBJECTID",[10],"None","FT_TEACHER")#greaterthan
#customReplacedef("1","newPSNAN","s01","3", "222","1","findAndReplace","DISTRICTID",[401870],"999999")