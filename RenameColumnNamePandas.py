import pyspark
import json
from deltalake import DeltaTable
from pyspark.sql.functions import lit, col, isnan, count
#from properties import path
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
def renameColumnNamedef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnName,NewColumnName):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    inputDeltaTable = path + "/" + inputDeltaTable
    #PYSPARK_SUBMIT_ARGS = "--driver-memory 4g pyspark-shell" python script.py

    try:
        print("Reading Universal data")
        pandaDF = DeltaTable(inputDeltaTable).to_pandas()
        print(pandaDF.head())
        print("Reading Universal data")
        columnNames = pandaDF.columns.tolist()
        print("Column names in Delta table: ", columnNames)
        df = pandaDF.rename(columns={columnName: NewColumnName})
        #df = Data.withColumnRenamed(columnName, NewColumnName)

        print("Column names of new Delta table: ", df.columns)
        outputDeltaTablepath = path + "/" + outputDeltaTable

        df.write.format("delta").save(outputDeltaTablepath)
        #df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully renamed the selected column",
                "data": {"inputsteps": len(pandaDF), "outputsteps": len(df)}}
        return json.dumps(list)

    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

renameColumnNamedef("1","newPSNAN","s01","3", "614","1","ZIP","NODE")
