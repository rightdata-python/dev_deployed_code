import pandas as pd
import math
import json
import pandas as pd
import pyspark
from deltalake import DeltaTable
#from properties import path
from data_quality_index import technical_quality_score
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")


def replaceMismatchDatatype(projectId, reciepeId, sessionId, inputStepId, currentStepId, inputResultId, columnName, dataType, parameter, valueToReplace, replaceColumn):
    if sessionId == None:
        inputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    inputDeltaTablepath = path + "/" + inputDeltaTable
    outputDeltaTablepath = path + "/" + outputDeltaTable
    try:
        print("Reading data")
        pandasDF = DeltaTable(inputDeltaTablepath).to_pandas()
        print(pandasDF[columnName].dtypes)
        #data = pd.read_csv(data)
        print(pandasDF)
        #datatypeDict = {columnName: eval(datatype)}
        if dataType == "IntegerType" or dataType == "ShortType" or dataType == "LongType" :
            dataType = "IntegerType"
        elif dataType == "FloatType" or dataType == "DoubleType":
            dataType = "FloatType"
        elif dataType == "BooleanType" or dataType == "BinaryType":
            dataType = "BooleanType"
        else:
            dataType = dataType

        datatypeDict = {columnName: dataType}
        #df_mask = mismatchDatatype(pandasDF[[columnName]], datatypeDict)
        # REPLACE
        # pandasDF.loc[~df_mask[columnName], columnName] = toReplace
        # pandasDF.loc[:, columnName]
        df_mask = technical_quality_score(pandasDF[[columnName]], datatypeDict, method='mask')

        if parameter == "customValue":
            pandasDF.loc[~df_mask[columnName], columnName] = valueToReplace
            #pandasDF.loc[:, columnName]

        elif parameter == "predefinedValue":
            if valueToReplace == "mean":
                print(type(pandasDF[columnName].mean()))
                print(df_mask[columnName])
                pandasDF.loc[~df_mask[columnName], columnName] = pandasDF[columnName].mean()
                #pandasDF.loc[:, columnName]

            elif valueToReplace == "median":
                pandasDF.loc[~df_mask[columnName], columnName] = pandasDF[columnName].median()
                #pandasDF.loc[:, columnName]

            elif valueToReplace == "mode":
                mode = pandasDF[columnName].mode().iat[0]
                pandasDF.loc[~df_mask[columnName], columnName] = mode
                #pandasDF.loc[:, columnName]

            elif valueToReplace == "stdDeviation":
                pandasDF.loc[~df_mask[columnName], columnName] = pandasDF[columnName].std()
                #pandasDF.loc[:, columnName]

            elif valueToReplace == "copyFromColumn":
                pandasDF.loc[~df_mask[columnName], columnName] = pandasDF[replaceColumn]
                #pandasDF.loc[:, columnName]




        print("No of Columns and Column names in Dataframe: ", len(pandasDF.columns), ";", pandasDF.columns)
        print("No. of records in Dataframe:", len(pandasDF))
        pandasDF[columnName] = pandasDF[columnName].astype(str)
        print(pandasDF)

        print("values after replacing:", pandasDF[columnName].tolist())

        print("Output Table with path:", outputDeltaTablepath)
        print(pandasDF[columnName].dtypes)

        spark_df = spark.createDataFrame(pandasDF)
        print("spark df created")
        print(pandasDF[columnName].dtypes)
        spark_df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        print("saved in given location")
        list = {"status": "Success", "message": "Successfully completed Replace Mismatch DataType",
            "data": {"inputsteps": len(pandasDF), "outputsteps": spark_df.count()}}
        print(list)
        return json.dumps(list)

    except OSError as err:
             print("OS error: {0}".format(err))
    # except:
    #     FailureMessage = {"status": "Failure",
    #               "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
    #     FailureMessageJson = json.dumps(FailureMessage)
    #     print(FailureMessageJson)
    #     return json.dumps(FailureMessage)

#def replaceMismatchDatatype(projectId, reciepeId, sessionId, inputStepId, currentStepId, inputResultId, columnName, datatype, toReplace):
#(projectId, reciepeId, sessionId, inputStepId, currentStepId, inputResultId, columnName, dataType, parameter, valueToReplace, replaceColumn)
replaceMismatchDatatype("1","newPSNAN","s01","3", "444","1", "FT_TEACHER","IntegerType","predefinedValue","mode","abd")

