# POSSIBLES NUMBER TYPES DETECTOR
import pandas as pd
import math
def is_float(value, not_integer=True):
    """
    Funtion to check if value is float or not.

    Parameters
    ----------
    value: str.
        Value to analize.

    Returns
    -------
    True if value is float or False is not.
    """
    if pd.api.types.is_bool(value):
        return False
    try:
        float_value = float(value)
        int_mask = float_value.is_integer()  # & not_integer
        if math.isnan(float_value) | int_mask:
            return False
        return True
    except ValueError:
        return False


# Check if value is integer
def is_integer(value):
    """
    Funtion to check if value is integer or not.

    Parameters
    ----------
    value: str.
        Value to analize.

    Returns
    -------
    True if value is integer or False is not.
    """
    if pd.api.types.is_bool(value):
        return False
    try:
        float_value = float(value)
        if float_value.is_integer():
            return True
        return False
    except ValueError:
        return False


# Define a simple function to check if value is boolean
def is_bool(x):
    """
    Funtion to check if value is boolean or not.

    Parameters
    ----------
    x: str.
        Value to analize.

    Returns
    -------
    True if value is boolean or False is not.
    """
    possible_bool = [True, False, 'True', 'False']
    if (x in possible_bool) & (pd.api.types.is_integer(x) != True) & (pd.api.types.is_float(x) != True):
        return True
    return False


def is_string(x):
    """
    Funtion to check if value is string or not.

    Parameters
    ----------
    x: str.
        Value to analize.

    Returns
    -------
    True if value is string or False is not.
    """

    if x in [True, False, 'True', 'False']:
        return False

    try:
        float(x)
        return False
    except:
        return True


# POSSIBLES DATE DETECTOR
def is_date(value):
    """
    Funtion to check if value is date or not.

    Parameters
    ----------
    value: str.
        Value to analize.

    Returns
    -------
    True if value is date or False is not.
    """
    try:
        str_value = str(value)
        date_value = pd.to_datetime(str_value, errors='coerce')
        return True
    except:
        return False


# POSSIBLES TIME TYPES DETECTOR
def is_time(value):
    """
    Funtion to check if value is time or not.

    Parameters
    ----------
    value: str.
        Value to analize.

    Returns
    -------
    True if value is time or False is not.
    """
    str_value = str(value)
    if str_value.isnumeric():
        return False
    date_value = pd.to_timedelta(str_value, errors='coerce')
    if type(date_value) == pd._libs.tslibs.nattype.NaTType:
        return False
    return True


def mismatchDatatype(df, dtype_selected, method='mask'):
    """
    Function to get the invalid data types selected

    Parameters
    ----------
    df: pandas data frame.
        Data frame with the columnas to analyze.
    dtype_selected: dictionary
        The key are equal to the column and the value are equal to the desired data type.. The available are:
        ['StringType', "IntegerType", "FloatType", "BooleanType", "DateType", "TimestampType"]
    method: string
        The method that you want to run. The available are:
        ['mask', 'count']

    Returns
    -------
    valid_types|invalid_serie: pandas data frame or pandas serie
        valid_types is a pandas data frame with the mask of all the valid data types value
        invalid_serie is a pandas serie with the count invalid value.
    """
    # First select the columns for the dtypes selected for the client
    col_string = [key for key, value in dtype_selected.items() if value == "StringType"]
    col_int = [key for key, value in dtype_selected.items() if value in ["IntegerType", "LongType", "ShortType"]]
    col_float = [key for key, value in dtype_selected.items() if value in ["FloatType", "DoubleType"]]
    col_bool = [key for key, value in dtype_selected.items() if value == "BooleanType"]
    col_date = [key for key, value in dtype_selected.items() if value == "DateType"]
    col_time = [key for key, value in dtype_selected.items() if value == "TimestampType"]

    def get_valid_mask(serie, method):
        clean_serie = serie.dropna()
        valid = clean_serie.apply(method)
        return valid

    def count_invalid(serie):
        clean_serie = serie.dropna()
        valid = clean_serie.sum()
        invalid = clean_serie.shape[0] - valid
        return invalid

        # Apply the different dtypes function depend of the dtypes that the client selected

    int_series = df.filter(col_int).apply(lambda x: get_invalid_data(x, is_integer))

    float_series = df.filter(col_float).apply(lambda x: get_invalid_data(x, is_float))

    string_series = df.filter(col_string).apply(lambda x: get_invalid_data(x, is_string))

    bool_series = df.filter(col_bool).apply(lambda x: get_invalid_data(x, is_bool))

    date_series = df.filter(col_date).apply(lambda x: get_invalid_data(x, is_date))

    time_series = df.filter(col_time).apply(lambda x: get_invalid_data(x, is_time))

    # Concat the different data types data frames
    valid_types = pd.concat([int_series, float_series, string_series, bool_series, date_series, time_series], axis=1)

    if method == 'mask':
        return valid_types
    elif method == 'count':
        invalid_serie = valid_types.apply(count_invalid)
        return invalid_serie
    else:
        return 'Invalid method'


