import pyspark
from properties import path
import json
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def UNIONdef(projectId,reciepeId,sessionId,stepId, currentStepId,columnNames, columnMapping):
    if sessionId == None:
        test_json = {"projectId": projectId, "reciepeId": reciepeId,"stepId": stepId}
        df_list_path = [path + "/" + "R_" + str(test_json['projectId']) + "_" + str(test_json['reciepeId']) + "_" + str(df_name['inputStepId']) + "_" + str(df_name['inputResultId']) for
                        df_name in test_json["stepId"]]
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        test_json = {"projectId": projectId, "reciepeId": reciepeId, "sessionId": sessionId, "stepId": stepId}
        df_list_path = [path + "/" + "R_" + str(test_json['projectId']) + "_" + str(test_json['reciepeId']) + "_" + str(
            test_json['sessionId']) + "_" + str(df_name['inputStepId']) + "_" + str(df_name['inputResultId']) for df_name in test_json["stepId"]]
        outputDeltaTable = path + "/" +"R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Tables:",df_list_path)
    print("Output name with location:",outputDeltaTable)
    print("columnNames",columnNames)
    dict_test ={'data_info':columnNames, 'columnMapping':columnMapping}
    print(dict_test)
    unionDF = union_multiple_datasets(df_list_path, dict_test)

    print("No of Columns and Column names in output: ", len(unionDF.columns), ";", unionDF.columns)
    print("No. of records in output:", len(unionDF))
    print(unionDF)
    df1 = spark.createDataFrame(unionDF)
    df1.show()
    outputDeltaTablepath = path + "/" + outputDeltaTable
    df1.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
    list = {"status": "Success", "message": "UNION request completed",
            "data": {"outputsteps": df1.count()}}
    return json.dumps(list)

# UNIONdef("1","newPSNAN","s01", [ { "inputStepId": "222", "inputResultId": "1" }, { "inputStepId": "333", "inputResultId": "1" } , { "inputStepId": "444", "inputResultId": "1" }], "555",[
#               [{"name": "OBJECTID", "alias": "id_original"},
#               {"name": "NCESID", "alias": "Breakdown_category"},
#               {"name": "NAME", "alias": "Year"},
#               {"name": "ADDRESS", "alias": "RD_Value"},
#               {"name": "CITY", "alias": "Units"},
#               {"name": "STATE", "alias": "Footnotes"},
#               {"name": "ZIP", "alias": "Column1"},
#               {"name": "COUNTRY1", "alias": "Column2"},
#               {"name": "TELEPHONE", "alias": "Column3"},
#               {"name": "TYPE", "alias": "Column4"},
#               {"name": "STATUS", "alias": "Column5"},
#               {"name": "POPULATION", "alias": "Column6"}],
#
#              [{"name": "ID", "alias": "id_second"},
#             {"name": "NAICS_CODE", "alias": "Description1"},
#             {"name": "COUNTRY2", "alias": "Period1"},
#             {"name": "SOURCE", "alias": "Previouspublish1"},
#             {"name": "SOURCEDATE", "alias": "Revised1"}],
#
#         [{"name": "ID_third", "alias": "id_third"},
#             {"name": "COUNTRY3", "alias": "WEBSITE"},
#             {"name": "LEVEL_", "alias": "LEVEL_3"},
#             {"name": "ENROLLMENT", "alias": "ENROLLMENT3"},
#             {"name": "ST_GRADE", "alias": "ST_GRADE3"}]
#     ], [["OBJECTID1", "OBJECTID2", "OBJECTID3"],["COUNTRY","COUNTRY2","COUNTRY3"]])



import pandas as pd
from deltalake import DeltaTable


def union_api(df_left, df_right, info_df_left, info_df_right):
    """
    Function to merge 2 dataset into one.

    Parameters
    ----------
    df1: Pandas dataframe.
        Left Data frame to merge.

    df2: Pandas dataframe.
        Right Data frame to merge.

    info_df1: Dict
        Dictionary with the information of the left data set

    info_df2: Dict
        Dictionary with the information of the right data set

    Return
    ----------
    new_df: Pandas dataframe.
        Data frame with the 2 data frames marged
    """

    # Save with the name and the alias for the left data frame.
    col_names_left = [dic['name'] for dic in info_df_left['data_info']]
    new_names_left = {dic['name']: dic['alias'] for dic in info_df_left['data_info']}

    # Save with the name and the alias for the right data frame.
    col_names_right = [dic['name'] for dic in info_df_right['data_info']]
    new_names_right = {dic['name']: dic['alias'] for dic in info_df_right['data_info']}

    # Filter the column selected for the left dataset and rename it.
    df_left_new = df_left.filter(col_names_left)
    df_left_new = df_left_new.rename(columns=new_names_left)

    # Filter the column selected for the right dataset and rename it.
    df_right_new = df_right.filter(col_names_right)
    df_right_new = df_right_new.rename(columns=new_names_right)

    # Save the mapping column for the left data frame.
    left_on = info_df_left['columnMapping']
    left_on_alias = [new_names_left[col] for col in left_on]

    # Save the mapping column for the right data frame.
    right_on = info_df_right['columnMapping']
    right_on_alias = [new_names_right[col] for col in right_on]

    # Do the merge of the 2 data frame.
    new_df = pd.merge(df_left_new, df_right_new, how='outer', left_on=left_on_alias, right_on=right_on_alias)

    # Fill the null values in the left index
    for index, col in enumerate(left_on_alias):
        new_df[col] = new_df[col].fillna(new_df[right_on_alias[index]])

    # Eliminate the right index
    new_df = new_df.drop(columns=right_on_alias)

    # Return the new data frame
    return new_df


def union_multiple_datasets(df_list_path, dict_info_dfs):
    """
    Function to merge multiples data sets into one. The type of merge is outer.
    If you pass only one dataset in the list, the functions return the same dataframe

    Parameters
    ------------
    df_list_path: list
        List with the path of the dataframe to merge.

    dict_info_dfs: dict
        Dictionary with two keys with the information of the dataset. The first key is data_info that has a list with the columns that
        wants to keep of every dataset and the corresponding alias. The position in the list refers to the data frame same position in the df_list.
        The second key is columnMapping, the value is a list with the columns to do the merge of the different data set. Like in the data_info key, the
        position in the list refers to the data frame same position in the df_list.

    Return
    ------------
    df_merge: pandas dataframe
        Dataframe with the multiples data sets merged.
    """
    # Read the data and put in a list
    df_list = [DeltaTable(path).to_pandas() for path in df_list_path]
    print("df1",df_list[0].columns)
    print("df2",df_list[1].columns)
    print("df3",df_list[2].columns)
    # Save the first data frame only for the case that you pass one data set
    df_merge = df_list[0]

    while len(df_list) > 1:
        # Keep the info for the merge in the first two data sets
        info_df1 = {key: values[0] for key, values in dict_info_dfs.items()}
        info_df2 = {key: values[1] for key, values in dict_info_dfs.items()}

        # Merge the first two data sets
        df_merge = union_api(df_list[0], df_list[1], info_df1, info_df2)

        # Combine the information of the firs two data set into new one
        data_info_new = {key: values + info_df2[key] for key, values in info_df1.items() if key != 'columnMapping'}
        new_info_data = [{key: dic['alias'] for key in dic.keys()} for dic in data_info_new['data_info']]
        data_info_new['data_info'] = new_info_data
        # Add the new combined columns to mapping
        data_info_new['columnMapping'] = [dic['alias'] for dic in info_df1['data_info'] if
                                          dic['name'] in info_df1['columnMapping']]

        # Replace in the dictionary dict_info_dfs the information for the first two data sets with the new one
        dict_info_dfs['data_info'].pop(0)
        dict_info_dfs['data_info'].pop(0)
        dict_info_dfs['data_info'].insert(0, data_info_new['data_info'])
        dict_info_dfs['columnMapping'].pop(0)
        dict_info_dfs['columnMapping'].pop(0)
        dict_info_dfs['columnMapping'].insert(0, data_info_new['columnMapping'])

        # Add the new data frame in the df_list and replace the 2 data frames merged.
        df_list.pop(0)
        df_list.pop(0)
        df_list.insert(0, df_merge)

    return df_merge