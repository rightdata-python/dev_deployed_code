import json
import pyspark

from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
from pyspark.sql.functions import lit, rand, when
from properties import path
#from pyspark.sql import SparkSession
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .config("spark.databricks.delta.checkLatestSchemaOnRead", "false")\
     .enableHiveSupport() \
     .getOrCreate() \
     .newSession() \
#.enableHiveSupport() \
#     .config("spark.databricks.delta.checkLatestSchemaOnRead", "false")\
print("spark session Started")

def columnCategoryCountPaginationdef( projectId, reciepeId, sessionId, inputStepId, currentStepId, inputResultId, columnName):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId
    spark.sql("REFRESH TABLE "+ inputDeltaTable)
    #sqlVar = spark.sql("REFRESH TABLE " + inputDeltaTable)
    print("Input Table:", inputDeltaTable)
    DeltaTable = path + "/" + inputDeltaTable
    print("Input Table with path:", DeltaTable)
    #spark._jsparkSession.catalog().refreshByPath(DeltaTable)
    #spark.catalog().refreshByPath(DeltaTable)
    print("Reading Universal data")
    #sparkSession.read().format("delta").load("uat." + r_1_t621_stp1_1)
    # Data_DeltaDF = None
    # print(Data_DeltaDF)
    #Data1 = spark.read.format("delta").load(DeltaTable)#"uat." + "r_1_t621_stp1_1")#
    Data_DeltaDF = spark.read.format("delta").load(DeltaTable)
    Data_DeltaDF.show()

    # print("Column names for first load: ", Data.columns)
    columnNames = Data_DeltaDF.columns
    print("Column names", columnNames)
    #print(Data_DeltaDF.select('*').distinct().count())
    Data_DeltaDF.select(F.countDistinct("col1")).show()

    from pyspark.sql.functions import col, countDistinct

    print(Data_DeltaDF.agg(*(countDistinct(col(c)).alias(c) for c in Data_DeltaDF.columns)))
    nullList = Data_DeltaDF.select([count(
            when(col(c).contains('NULL') | col(c).isNull() | isnan(c),
                  c)).alias(c) for c in Data_DeltaDF.columns]).collect()
    empty = Data_DeltaDF.select([count(when((col(c) == ''), c)).alias(c) for c in Data_DeltaDF.columns]).collect()
    print(nullList)
    # FinalDict = Data_DeltaDF(lambda x: previewspark(x))#, x.name))
    # # previewdef(lambda x:x.index))
    # print(FinalDict)
    # print(type(FinalDict))
    # # FinalDict.loc[columnNames]
    # print([dic for dic in FinalDict])



        # if columnName == None:
        #     FinalDict = {"RowValues": []}
        #     for i in columnNames:
        #         dictData = {}
        #         dictData["column_name"] = i
        #         datatype = Data_DeltaDF.schema[i].dataType
        #         dictData["data_type"] = str(datatype)
        #         categoryCount = Data_DeltaDF.select(col(i)).distinct().count()
        #         dictData["category_count"] = categoryCount
        #         countdata = Data_DeltaDF.select(col(i)).count()
        #         #invalidpercentage = (100 - validvaluespercentage)
        #         #dictData["invalid_data_percentage"] = invalidpercentage
        #         # dictData["valid_values_count"] = (countdata - invalidlist[0][i])
        #         # dictData["invalid_values_count"] = invalidlist[0][i]
        #         #null
        #         if nullList[0][i] == 0:
        #             nullpercentage = 0
        #         else:
        #             nullpercentage = (nullList[0][i] / countdata) * 100
        #         nullcount = nullList[0][i]
        #         dictData["null_count"] = nullcount
        #         dictData["null_percentage"] = nullpercentage
        #         #empty
        #         if empty[0][i] == 0:
        #             emptypercentage = 0
        #         else:
        #             emptypercentage = (empty[0][i] / countdata) * 100
        #         emptycount = empty[0][i]
        #         dictData["empty_count"] = emptycount
        #         dictData["empty_percentage"] = emptypercentage
        #         #datatypeMissmatch
        #         datatypemismatchcount = 0
        #         datatypemismatchpercentage = 0
        #         dictData["mismatched_datatype_count"] = datatypemismatchcount
        #         dictData["mismatched_datatype_percentage"] = datatypemismatchpercentage
        #         #domain missmatch
        #         domainmismatchcount = 0
        #         domainmismatchpercentage = 0
        #         dictData["mismatched_domian_count"] = domainmismatchcount
        #         dictData["mismatched_domian_percentage"] = domainmismatchpercentage
        #
        #         #validpercentage
        #         validvaluescount = countdata - nullcount - emptycount - datatypemismatchcount - domainmismatchcount
        #         dictData["valid_data_count"] = validvaluescount
        #         validvaluespercentage = 100 - nullpercentage - emptypercentage - datatypemismatchpercentage - domainmismatchpercentage
        #         dictData["valid_data_percentage"] = validvaluespercentage
        #
        #         print("Details of column ", i, " is :", dictData)
        #         FinalDict["RowValues"].append(dictData)
        #     print(FinalDict)
        #     json_object = json.dumps(FinalDict)#, indent=2, sort_keys=True)#strict=False)#
        #     #content = json.loads(r.decode('utf-8').replace('\n', ''))
        #     list = {"status": "Success", "message": "Successfully Completed Column Category Count Pagination Request",
        #         "data": json_object}
        #     return json.dumps(list)
        # else:
        #     try:
        #         FinalDict = {"RowValues": []}
        #         dictData = {}
        #         dictData["column_name"] = columnName
        #         datatype = Data_DeltaDF.schema[columnName].dataType
        #         dictData["data_type"] = str(datatype)
        #         categoryCount = Data_DeltaDF.select(col(columnName)).distinct().count()
        #         dictData["category_count"] = categoryCount
        #         countdata = Data_DeltaDF.select(col(columnName)).count()
        #         #null
        #         if nullList[0][columnName] == 0:
        #             nullpercentage = 0
        #         else:
        #             nullpercentage = (nullList[0][columnName] / countdata) * 100
        #         nullcount = nullList[0][columnName]
        #         dictData["null_count"] = nullcount
        #         dictData["null_percentage"] = nullpercentage
        #         # empty
        #         if empty[0][columnName] == 0:
        #             emptypercentage = 0
        #         else:
        #             emptypercentage = (empty[0][columnName] / countdata) * 100
        #         emptycount = 0
        #         dictData["empty_count"] = emptycount
        #         dictData["empty_percentage"] = emptypercentage
        #         # datatypeMissmatch
        #         datatypemismatchcount = 0
        #         datatypemismatchpercentage = 0
        #         dictData["mismatched_datatype_count"] = datatypemismatchcount
        #         dictData["mismatched_datatype_percentage"] = datatypemismatchpercentage
        #         # domain missmatch
        #         domainmismatchcount = 0
        #         domainmismatchpercentage = 0
        #         dictData["mismatched_domian_count"] = domainmismatchcount
        #         dictData["mismatched_domian_percentage"] = domainmismatchpercentage
        #         # validpercentage
        #         validvaluescount = countdata - nullcount - emptycount - datatypemismatchcount - domainmismatchcount
        #         dictData["valid_data_count"] = validvaluescount
        #         validvaluespercentage = 100 - nullpercentage - emptypercentage - datatypemismatchpercentage - domainmismatchpercentage
        #         dictData["valid_data_percentage"] = validvaluespercentage
        #         print("Details of column ", columnName, " is :", dictData)
        #         FinalDict["RowValues"].append(dictData)
        #         print(FinalDict)
        #         json_object = json.dumps(FinalDict)#, strict=False)
        #         #return json_object
        #         list = {"status": "Success", "message": "Successfully completed column category count pagination Request",
        #                 "data": json_object}
        #         return json.dumps(list)
        #     except:
        #         FailureMessage = {"status": "Failure",
        #                           "message": "Selected Column in not in the Delta Table given"}
        #         FailureMessageJson = json.dumps(FailureMessage)
        #         print(FailureMessageJson)
        #         return json.dumps(FailureMessage)
    # except:
    #     FailureMessage = {"status": "Failure",
    #                       "message": "Delta Table entered doesnt exists, Please enter a valid Delta Table"}
    #     FailureMessageJson = json.dumps(FailureMessage)
    #     print(FailureMessageJson)
    #     return json.dumps(FailureMessage)

columnCategoryCountPaginationdef("1","newPSNAN","s01","3", "4","1", None)

def previewspark(Data_DeltaDF):
    # nullList = Data_DeltaDF.select([count(
    #     when(col(c).contains('NULL') | col(c).isNull() | isnan(c),
    #          c)).alias(c) for c in Data_DeltaDF.columns]).collect()
    # empty = Data_DeltaDF.select([count(when((col(c) == ''), c)).alias(c) for c in Data_DeltaDF.columns]).collect()
    FinalDict = {"RowValues": []}
    dictData = {}
    #dictData["column_name"] = columnName
    datatype = Data_DeltaDF.schema.dataType
    dictData["data_type"] = str(datatype)
    categoryCount = Data_DeltaDF.countDistinct()
    dictData["category_count"] = categoryCount
    countdata = Data_DeltaDF.count()
    # # null
    # if nullList[0][columnName] == 0:
    #     nullpercentage = 0
    # else:
    #     nullpercentage = (nullList[0][columnName] / countdata) * 100
    # nullcount = nullList[0][columnName]
    # dictData["null_count"] = nullcount
    # dictData["null_percentage"] = nullpercentage
    # # empty
    # if empty[0][columnName] == 0:
    #     emptypercentage = 0
    # else:
    #     emptypercentage = (empty[0][columnName] / countdata) * 100
    # emptycount = 0
    # dictData["empty_count"] = emptycount
    # dictData["empty_percentage"] = emptypercentage
    # # datatypeMissmatch
    # datatypemismatchcount = 0
    # datatypemismatchpercentage = 0
    # dictData["mismatched_datatype_count"] = datatypemismatchcount
    # dictData["mismatched_datatype_percentage"] = datatypemismatchpercentage
    # # domain missmatch
    # domainmismatchcount = 0
    # domainmismatchpercentage = 0
    # dictData["mismatched_domian_count"] = domainmismatchcount
    # dictData["mismatched_domian_percentage"] = domainmismatchpercentage
    # # validpercentage
    # validvaluescount = countdata - nullcount - emptycount - datatypemismatchcount - domainmismatchcount
    # dictData["valid_data_count"] = validvaluescount
    # validvaluespercentage = 100 - nullpercentage - emptypercentage - datatypemismatchpercentage - domainmismatchpercentage
    # dictData["valid_data_percentage"] = validvaluespercentage
    print("Details of column ", dictData)
    return dictData
    # FinalDict["RowValues"].append(dictData)
    # print(FinalDict)
    # json_object = json.dumps(FinalDict)  # , strict=False)
    # # return json_object
    # list = {"status": "Success", "message": "Successfully completed column category count pagination Request",
    #         "data": json_object}
    # return json.dumps(list)

