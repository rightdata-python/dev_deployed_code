import datetime
import pandas as pd
import math
import numpy as np
import time
import sys
from deltalake import DeltaTable
from data_quality_index import data_quality_index
from data_domain_errors import DataDomainErrors
import json
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT" #enter location as intended

def DataQualityScoredef(projectId,reciepeId,sessionId, initalInputStepId, finalInputStepId, currentStepId, initalInputResultId,  finalInputResultId, domainSelected, dataTypeSelected):
    if sessionId == None:
        UniverseDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + initalInputStepId + "_" + initalInputResultId
        SampleDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + finalInputStepId + "_" + finalInputResultId
    else:
        UniverseDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+ initalInputStepId + "_" + initalInputResultId
        SampleDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + finalInputStepId + "_" + finalInputResultId
    InputTableUniverse = path +"/" + UniverseDeltaTable
    InputTableSample = path + "/" + SampleDeltaTable

    try:
        #indata
        print("Reading Universal data")
        uData = DeltaTable(InputTableUniverse).to_pandas()
        #outdata
        print("Reading Sample data")
        sData = DeltaTable(InputTableSample).to_pandas()

        UcolumnNamesCount = len(uData.columns)  # uData.columns.count()
        ScolumnNamesCount = len(sData.columns)  # sData.columns.count()

        # print(columnNames)

        #after dropping null
        sData_noNull = sData.dropna()

        countofsData = len(sData)
        countofsData_noNull = len(sData_noNull)

        SValidValues = (countofsData_noNull / countofsData) * 100
        SMissingValues = 100 - SValidValues

        DictValues = {}
        DictValues["Data in"] = {"Rows": len(uData), "Columns": UcolumnNamesCount}
        DictValues["Data out"] = {"Rows": len(sData), "Columns": ScolumnNamesCount}
        # Data Quality Score of Out data
        # Data quality score
        # domain_quality_score = functional_quality_domain_score(sData, dataTypeSelected)
        # quality_types_score = technical_quality_score(sData, dataTypeSelected)
        # DataQualityScore = data_quality_index(sData, domainSelected, dataTypeSelected)
        DataQualityScore, availability_score, domain_quality_score, quality_types_score = data_quality_index(sData, domainSelected, dataTypeSelected)

        quality_index, notnull_score, domain_quality_score, quality_types_score, proportion_valid_data, proportion_invalid_data = data_quality_index(sData, domainSelected, dataTypeSelected, calculate_valid=True)
        #"Invalid Domain Percetage": 0, "Invalid Datatype Percentage":0 needs replacement with calculated values

        DictValues["Data Quality"] = {"Valid Proportion": proportion_valid_data, "InValid Proportion":proportion_invalid_data, "Not null Proportion": notnull_score, "Null Proportion": 1- notnull_score , "Valid Domain Proportion": domain_quality_score, "Valid DataType Proportion":quality_types_score, "Data Quality Score": quality_index}
        print("Dict", DictValues)
        json_object = json.dumps(DictValues)
        #return json_object
        list = {"status": "Success", "message": "Successfully Completed Data Quality scoring",
            "data": json_object}
        return json.dumps(list)
    except OSError as err:
         print("OS error: {0}".format(err))
    # except:
    #     FailureMessage = {"status": "Failure",
    #                       "message": "Some thing went wrong, please check the inputs provided"}
    #     FailureMessageJson = json.dumps(FailureMessage)
    #     print(FailureMessageJson)
    #     return json.dumps(FailureMessage)






