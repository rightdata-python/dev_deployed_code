import pandas as pd
import re


def replace_regex(column, regex_str, new_value, df, method='replace_all', pattern=True, flags=re.IGNORECASE):
    """
    Function that use a given regex or pattern to match and replace with a new value. You can replace
    all the string or only the match with the regex o pattern. The available pattern are:

    Character patterns:
        {any_character} = Match any character, exactly once
        {alpha} = Alpha character [A-Za-z_]
        {upper} = Uppercase alpha character [A-Z_]
        {lower} = Lowercase alpha character [a-z_]
        {digit} = Digit character [0-9]
        {delim} = Single delimiter character e.g :, ,, |, /, -, ., \s
        {delim_ws} = Single delimiter and all the whitespace around it
        {alpha_numeric} = Match a single alphanumeric character
        {alphanum-underscore} = Match a single alphanumeric character or underscore character
        {hashtag} = Match #hashtag values
        {contains} = The value that you want to match

    Position patterns:
        {start} = Match the start of the line
        {end} = Match the end of the line

    Type patterns:
        {phone} = Match a valid U.S. phone number. See Phone Number Data Type.
        {email} = Match a valid email address. See .Email Address Data Type
        {url} = Match a valid URL. See URL Data Type.
        {ip_address} = Match a valid IP address. See IP Address Data Type.
        {hex_ip_address} = Match a valid hexadecimal IP address (e.g. 0x0CA40012)
        {bool_type} = Match a valid Boolean value. See Boolean Data Type.
        {street} = Match a U.S.-formatted street address (e.g. 123 Main Street)
        {occupancy} = Match a valid U.S.-formatted occupancy address value (e.g. Apt 2D)
        {city} = Match a city name within U.S.-formatted address value
        {state} = Match a valid U.S. state value (e.g. California).
        {state_abbrev} = Match a valid two-letter U.S. state abbreviation value (e.g. CA)
        {zip_code} = Match a valid five-digit zip code

    Datetime patterns:
        {month} = Match full name of month (e.g. January)
        {month-abbrev} = Match short name of month (e.g. Jan)
        {time} = Match time value in HOUR:MINUTE:SECOND format (e.g. 11:59:23)
        {period} = Match time period of the day: AM/PM
        {dayofweek} = Match long name for day of the week (e.g. Sunday).
        {dayofweek-abbrev} = Match short name for day of the week (e.g. Sun).
        {utcoffset} = Match a valid UTC offset value (e.g. -0500, +0400, Z)

    Parameters
    ----------
    column: str
        The column of the data frame that need to be fixed.

    regex_str: str
        The regex expression o pattern expression.

    new_value: str
        The new value to add.

    df: pandas dataframe
        The dataframe to apply the transformation.

    method: string, default=replace_all
        the available methods are:
            * replace_all: Replace all the value with the new value.
            * replace_part: Replace only the part of the string that match with the pattern or regex.
            * count: Return the numbers of times that the regex or patter appear .
            * remove: Remove the rows where the regex or patter match with the string.

    pattern: bool, default=True
        If you pass a pattern this parameter need to be True, else if you pass a regex this
        parameter need to be False.

    flag: int or re flags, default=0
        The regex flags. This value is optiona

    Return
    ----------
    df_new: pandas dataframe
        The new dataframe with the select column fixed.
    """
    dict_regex = {

        # Character patterns:
        'any_character': '{1}',
        'alpha': '[A-Za-z_]',
        'upper': '[A-Z_]',
        'lower': '[a-z_]',
        'digit': '\d',
        'delim': '\s',
        'delim_ws': '\s',
        'alpha_numeric': '\w',
        'alphanum_underscore': '\w',
        'hashtag': '#',
        'contains': '',

        # Position patterns:
        'start': '^',
        'end': '$',

        # Type patterns:
        'phone': "^(\+?\([0-9]{3}\) ?|[0-9]{3}-)[0-9]{3}-[0-9]{4}$",
        'email': '[\w\.]+@[\w\-]+\.[\w]+',
        'url': '(https?:\/\/(?:[-\w.]|(?:%[\da-fA-F]{2}))+)|(www\.(?:[-\w.]|(?:%[\da-fA-F]{2}))+)',
        'ip_address': "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",
        #         'hex_ip_address': ,
        'bool_type': '^True$|^False$|^true$|^false$',
        'street': '(\d{1,5}\s[A-Z]+)|([A-Z]+\s\d{1,5})',
        #         'occupancy': ,
        #         'city': ,
        'state': 'Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|WashingtonDC|Delaware|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New Hampshire|New Jersey|New Mexico|New York|North Carolina|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|Rhode Island|South Carolina|South Dakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West Virginia|Wisconsin|Wyoming',
        'state_abbrev': 'AL|AK|AZ|AR|CA|CO|CT|DC|DE|FL|GA|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VT|VA|WA|WV|WI|WY',
        'zip_code': '^\d{5}$',

        # Datetime patterns:
        'month': ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october',
                  'november', 'december'],
        'month-abbrev': ['jun', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'],
        'time': '^([0-1]?\d|2[0-3]):[0-5]?\d:[0-5]?\d$',
        'period': '(1[0-2]|0?[1-9])(AM|PM)',
        'dayofweek': ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'],
        'dayofweek-abbrev': ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'],
        #         'utcoffset':
    }

    # Copy the dataset
    df_new = df.copy()
    # Fix the regex
    if pattern:
        try:
            regex_str = regex_str.format(**dict_regex)
        except:
            return 'Invalid pattern'

    #  Make a mask with the values that match with the regex
    mask = df_new[column].astype(str).str.contains(regex_str, regex=True, flags=flags)

    # If method is equal to replace_all, replace all the string if the regex match
    if method == 'replace_all':
        # Replace this values with the new one
        df_new.loc[mask, column] = new_value

    # If method is equal to replace_part, replace the regex part with the new value
    elif method == 'replace_part':
        # Replace only the regex part with the new value
        df_new.loc[:, column] = df_new.loc[:, column].astype(str).str.replace(regex_str, new_value, regex=True,
                                                                              flags=flags)

    # If method is equal to count, sum all the regex match
    elif method == 'count':
        # Count the number of times that the regex match
        count = mask.sum()
        return count
    # If method is equal to remove, eliminate all the row that match with the regex
    elif method == 'remove':
        df_new = df_new.loc[~mask, :]

    else:
        return 'Invalid method, please select one of the available methods: [replace_all, replace_part, count, remove]'
    # Return the new dataframe
    return df_new

