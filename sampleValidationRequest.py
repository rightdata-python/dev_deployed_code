

#validating the universe data with sample data and returning metrics like count, distinct count, mean, median, mode, std, min, max and calculated Consistency
#Expected input: JSON
#Expected output: JSON

#import libraries and packages
import json
import pyspark
from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
from sklearn.model_selection import StratifiedShuffleSplit
from pyspark.sql.functions import lit, rand, when
from properties import path

#spark session
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

##defing samplingvalidation function to perform sample validation
def samplingValidationRequestdef(projectId,reciepeId,sessionId, universeInputStepId,  sampleInputStepId,  currentStepId, universeInputResultId,  sampleInputResultId):
    """
    Inputs:
    projectID: project id of the data, expects int
    reciepeId: reciepe id of the data, expects str
    sessionId: sessionId of the data, expects str(optional)
    universeInputStepId: InputStepId of universe data, expects str
    sampleInputStepId: inputStepId of sanple data, expects str
    currentStepId: currentStepId for the new data, expects str(optional)
    universeInputResultId: inputResultId of universe data, expects str
    sampleInputResultId: inputResultId of sample data, expects str
    """
    # constructing the file name and defining path to extract delta table from path
    if sessionId == None:
        UniverseDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + universeInputStepId + "_" + universeInputResultId
        SampleDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + sampleInputStepId + "_" + sampleInputResultId
    else:
        UniverseDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+ universeInputStepId + "_" + universeInputResultId
        SampleDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + sampleInputStepId + "_" + sampleInputResultId
    InputTableUniverse = path +"/" + UniverseDeltaTable
    InputTableSample = path + "/" + SampleDeltaTable
    print("Reading Universal data")
    try:
        # reading delta table as spark df
        uData = spark.read.format("delta").load(InputTableUniverse)
        print("Reading Sample data")
        sData = spark.read.format("delta").load(InputTableSample)
        #column names into columnNames
        columnNames = uData.columns
        # print(columnNames)
        from pyspark.sql.functions import count
        #invalid list containing None, NULL,empty, or null
        invalidlist = uData.select([count(
            when(col(c).contains('None') | col(c).contains('NULL') | (col(c) == '') | col(c).isNull() | isnan(c),
                 c)).alias(c) for c in uData.columns]).collect()
        invalidlist1 = sData.select([count(
            when(col(d).contains('None') | col(d).contains('NULL') | (col(d) == '') | col(d).isNull() | isnan(d),
                 d)).alias(d) for d in sData.columns]).collect()
        rowData = []
        # Get All column names and it's types
        #int column list and other in other list
        columnsdtypeint = []
        columnsdtypeother = []
        for y in uData.dtypes:
            if y[1] == 'int' or y[1] == 'double':
                columnsdtypeint.append(y[0])
            else:
                columnsdtypeother.append(y[0])
        print("int columns", columnsdtypeint)
        print("other columns", columnsdtypeother)
        # dictU = {}
        #dictonary to store the metrics
        dictF = {"RowValues": []}
        for i in columnNames:
            #for int columns
            if i in columnsdtypeint:
                dictU = {}
                #column name
                dictU["column_name"] = i
                # count of records
                count = uData.select(col(i)).count()
                count1 = sData.select(col(i)).count()
                #length of both data
                dictU["No. of values"] = {"Sample": count1, "Universal": count}

                # distinct count
                distinct = uData.select(col(i)).distinct().count()
                distinct1 = sData.select(col(i)).distinct().count()
                #count of distinct values of both data
                dictU["No. of Distinct Values"] = {"Sample": distinct1, "Universal": distinct}

                # invalid data count of both data
                nan = count - invalidlist[0][i]
                nan1 = count1 - invalidlist1[0][i]
                dictU["No. of Finite Values"] = {"Sample": nan1, "Universal": nan}
                # mean & standard diviation
                uData_stats = uData.select(_mean(col(i)).alias('mean'), _stddev(col(i)).alias('std')).collect()
                mean = uData_stats[0]['mean']
                std = uData_stats[0]['std']
                sData_stats = sData.select(_mean(col(i)).alias('mean1'), _stddev(col(i)).alias('std1')).collect()
                mean1 = sData_stats[0]['mean1']
                std1 = sData_stats[0]['std1']
                dictU["Mean"] = {"Sample": mean1, "Universal": mean}
                dictU["Standard Deviation"] = {"Sample": std1, "Universal": std}
                # median
                from pyspark.sql.types import IntegerType
                data_df = uData.withColumn(i, uData[i].cast(IntegerType()))
                median = data_df.approxQuantile(i, [0.5], 0.25)
                data_df1 = sData.withColumn(i, sData[i].cast(IntegerType()))
                median1 = data_df1.approxQuantile(i, [0.5], 0.25)

                dictU["Median"] = {"Sample": median1[0], "Universal": median[0]}
                dictU["Mode"] = {"Sample": "N/A", "Universal": "N/A"}
                # Mean
                if mean == 0:
                    mean_avg = 0
                elif mean1 > mean:
                    mean_avg = (mean * 100) / mean1
                else:
                    mean_avg = (mean1 * 100) / mean
                print("mean average: ", mean_avg)
                # Median
                if median[0] == 0:
                    median_avg = 0
                elif median1[0] > median[0]:
                    median_avg = (median[0] * 100) / median1[0]
                else:
                    median_avg = (median1[0] * 100) / median[0]
                print("median average: ", median_avg)
                # standard deviation
                if std == 0:
                    sd_avg = 0
                elif std1 > std:
                    sd_avg = (std * 100) / std1
                else:
                    sd_avg = (std1 * 100) / std
                print("Standard deviation average: ", sd_avg)
                #calculate consistency by taking avg of avg mean, avg median, avg std
                Consistency = (mean_avg + median_avg + sd_avg) / 3
                dictU["Consistency"] = Consistency
                # max
                df = uData.dropna(subset=[i])
                rowvalue = df.agg({i: "max"}).collect()[0]
                maxvalue = rowvalue[f"max({i})"]
                df1 = sData.dropna(subset=[i])
                rowvalue1 = df1.agg({i: "max"}).collect()[0]
                maxvalue1 = rowvalue1[f"max({i})"]

                dictU["Max"] = {"Sample": maxvalue1, "Universal": maxvalue}
                # min
                rowminvalue = df.agg({i: "min"}).collect()[0]
                minvalue = rowminvalue[f"min({i})"]
                rowminvalue1 = df1.agg({i: "min"}).collect()[0]
                minvalue1 = rowminvalue1[f"min({i})"]

                dictU["Min"] = {"Sample": minvalue1, "Universal": minvalue}

                print("Rowdetails for column ", i, " is :", dictU)

            else:
                #for other column other than int
                dictU = {}
                dictU["column_name"] = i
                # count of records
                count = uData.select(col(i)).count()
                count1 = sData.select(col(i)).count()
                dictU["No. of values"] = {"Sample": count1, "Universal": count}

                # distinct count
                distinct = uData.select(col(i)).distinct().count()
                distinct1 = sData.select(col(i)).distinct().count()
                dictU["No. of Distinct Values"] = {"Sample": distinct1, "Universal": distinct}

                # invalid data count
                nan = count - invalidlist[0][i]
                nan1 = count1 - invalidlist1[0][i]
                dictU["No. of Finite Values"] = {"Sample": nan1, "Universal": nan}
                # mean & standard diviation
                dictU["Mean"] = {"Sample": "N/A", "Universal": "N/A"}
                dictU["Standard Deviation"] = {"Sample": "N/A", "Universal": "N/A"}
                # median
                dictU["Median"] = {"Sample": "N/A", "Universal": "N/A"}
                mode = uData.groupby(i).count().orderBy("count", ascending=False).first()[0]
                mode1 = sData.groupby(i).count().orderBy("count", ascending=False).first()[0]
                dictU["Mode"] = {"Sample": mode1, "Universal": mode}
                if distinct == 1:
                    dictU["Consistency"] = 100
                else:
                    if mode == mode1:
                        Consistency = 90
                    else:
                        Consistency = 50
                    dictU["Consistency"] = Consistency
                # max
                df = uData.dropna(subset=[i])
                rowvalue = df.agg({i: "max"}).collect()[0]
                maxvalue = rowvalue[f"max({i})"]
                df1 = sData.dropna(subset=[i])
                rowvalue1 = df1.agg({i: "max"}).collect()[0]
                maxvalue1 = rowvalue1[f"max({i})"]

                dictU["Max"] = {"Sample": maxvalue1, "Universal": maxvalue}
                # min
                rowminvalue = df.agg({i: "min"}).collect()[0]
                minvalue = rowminvalue[f"min({i})"]
                rowminvalue1 = df1.agg({i: "min"}).collect()[0]
                minvalue1 = rowminvalue1[f"min({i})"]

                dictU["Min"] = {"Sample": minvalue1, "Universal": minvalue}

                print("Rowdetails for column ", i, " is :", dictU)

            dictF["RowValues"].append(dictU)

        print(dictF)
        # Serializing json
        json_object = json.dumps(dictF, indent=4)
        # Request outputs json of list below with success message
        list = {"status": "Success", "message": "Successfully Completed Sample Validation Request",
                "data": json_object}
        return json.dumps(list)
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the input parameters!"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)
        return json_object

