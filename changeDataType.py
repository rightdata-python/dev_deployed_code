import dbutils as dbutils
import pyspark
import json
#import dbutils.fs
from pyspark.sql.functions import lit, col, isnan, count
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
from pyspark.sql.types import StringType,BooleanType,DateType,IntegerType, DoubleType, ShortType, LongType, FloatType, TimestampType,BinaryType
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def changeDataTypedef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnName,dataType):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    DeltaTable = path + "/" + inputDeltaTable
    outputDeltaTablepath = path + "/" + outputDeltaTable
    print("Reading Universal data")
    try:
        Data = spark.read.format("delta").load(DeltaTable)
        print("Column names of input Delta table: ", Data.columns)
        try:
            print("Data type of selected column is : ", Data.schema[columnName].dataType)
            countData = Data.count()
            print("No. of records in Data:", countData)
            from pyspark.sql.functions import col
            # convert to StringType
            if dataType == "StringType":
                df = Data.withColumn(columnName, col(columnName).cast(StringType()))
                print("Column names of output Delta table: ", df.columns)
                print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                df.show()
                df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
                list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                json_object = json.dumps(list)
                return json.dumps(list)

            # convert to DoubleType
            elif dataType == "DoubleType":
                df = Data.withColumn(columnName, col(columnName).cast(DoubleType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure",
                                      "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    #json_object = json.dumps(list, indent=2, sort_keys=True)
                    return json.dumps(list)

            # convert to IntegerType
            elif dataType == "IntegerType":
                df = Data.withColumn(columnName, col(columnName).cast(IntegerType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure",
                                      "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    json_object = json.dumps(list, indent=2, sort_keys=True)
                    return json.dumps(list)

            # convert to DateType
            elif dataType == "DateType":
                df = Data.withColumn(columnName, col(columnName).cast(DateType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure",
                                      "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    json_object = json.dumps(list, indent=2, sort_keys=True)
                    return json.dumps(list)

            # convert to BooleanType
            elif dataType == "BooleanType":
                df = Data.withColumn(columnName, col(columnName).cast(BooleanType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure",
                                      "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    json_object = json.dumps(list, indent=2, sort_keys=True)
                    return json.dumps(list)

            # convert to ShortType
            elif dataType == "ShortType":
                df = Data.withColumn(columnName, col(columnName).cast(ShortType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure",
                                      "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    json_object = json.dumps(list, indent=2, sort_keys=True)
                    return json.dumps(list)

            # convert to LongType
            elif dataType == "LongType":
                df = Data.withColumn(columnName, col(columnName).cast(LongType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure",
                                      "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    json_object = json.dumps(list, indent=2, sort_keys=True)
                    return json.dumps(list)

            # convert to FloatType
            elif dataType == "FloatType":
                df = Data.withColumn(columnName, col(columnName).cast(FloatType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure",
                                      "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    json_object = json.dumps(list, indent=2, sort_keys=True)
                    return json.dumps(list)

            # convert to BinaryType(when string is converted to Binary and column has some null values the datatype is shown as string)
            elif dataType == "BinaryType":
                df = Data.withColumn(columnName, col(columnName).cast(BinaryType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure",
                                      "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    json_object = json.dumps(list, indent=2, sort_keys=True)
                    return json.dumps(list)

            # convert to TimestampType
            elif dataType == "TimestampType":
                df = Data.withColumn(columnName, col(columnName).cast(TimestampType()))
                filterdf = df.filter(col(columnName).isNull())
                filtercount = filterdf.count()
                if filtercount == countData:
                    print("column in filtered df:", filtercount)
                    FailureMessage = {"status": "Failure", "message": "The Datatype selected is not applicable for the column selected"}
                    FailureMessageJson = json.dumps(FailureMessage)
                    print(FailureMessageJson)
                    return json.dumps(FailureMessage)

                else:
                    print("Column names of output Delta table: ", df.columns)
                    print("Data type of selected column is changed to : ", Data.schema[columnName].dataType)
                    df.show()
                    df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(
                        outputDeltaTablepath)
                    list = {"status": "Success","message":"Successfully changed the datatype of the selected column","data":{"inputsteps": Data.count(), "outputsteps": df.count()}}
                    #json_object = json.dumps(list)
                    return json.dumps(list)

            else:
                FailureMessage = {"status": "Failure",
                                  "message": "Please enter a valid DataType from the list: StringType, BooleanType, DateType, IntegerType, DoubleType, ShortType, LongType, FloatType, TimestampType, BinaryType"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        except:
            FailureMessage = {"status": "Failure",
                              "message": "Column selected is not in the Input Delta Table"}
            FailureMessageJson = json.dumps(FailureMessage)
            print(FailureMessageJson)
            return json.dumps(FailureMessage)

    except:
        FailureMessage = {"status": "Failure","message": "Delta Table entered doesnt exists, Please enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

    # for col in Data.dtypes:
    #     print("Column name and datatype before changing : ", col[0] + " , " + col[1])

#changeDataTypedef("1","newPSNANs","s01","3", "4","1", "NAME","BooleanType")
