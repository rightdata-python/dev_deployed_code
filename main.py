#importing required files and packages

import uvicorn
import asyncio
from fastapi import FastAPI, Depends
from fastapi.concurrency import run_in_threadpool
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from fastapi import FastAPI, Response
from starlette.websockets import WebSocket
from typing import List, Optional
from pydantic import BaseModel
from samplingDTRequest import samplingDTRequestdef
from sampleValidationRequest import samplingValidationRequestdef
from sampleValidation_Pandas import samplingValidationpandas
from columnCategoryCountPaginationRequest import columnCategoryCountPaginationdef
from columnPaginationPandas import columnPaginationPandasDef
from listOfCategories import listOfCategoriesdef
from deleteColumn import deleteColumndef
from customRemove import customRemovedef
from changeDataType import changeDataTypedef
from RenameColumnName import renameColumnNamedef
from splitColumn import splitColumndef
from replaceMissing import replaceMissingdef
from customReplace import customReplacedef
from CountOfMissingRequest import CountOfMissingdef
from columnDetails import columnDetailsdef
from Join import JoinDatasets
from DataQualityScore import DataQualityScoredef
from firstCellTransformation import firstCellTransformationdef
from masking import maskingreq
from capping_IQR import iqr_for_all_columns,remove_outliers,display_outliers,Cap_Percentile_Outlier,Remove_Percentile_Outlier,Rulebased_Outlier
from ordinal_encoding import ordinal_function,ordinal_encoding_pandas
from oneHotEncoding import OneHotEncodingTech
from ReplaceMismatchDatatype import replaceMismatchDatatype
from RemoveMismatchDatatype import removeMismatchDatatype
from pivot import pivot_def
from fuzzyJoin import fuzzyMatch_Def
from recordLinkage import recordLinkage_Def
from duplicateColumn import duplicateColumndef
from IQR_Percentile_Graph import IQR_graph_data, Percentile_graph_data
from union import UNIONdef
#assigning FastAPI as app
from fastapi.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware

from IQR_check_pandas import iqr_for_all_columns_pandas
from replace_new_custom import custom_replace
from count_remove import countRemovedef

from example_onehot import OneHotEncodingTech,iqr_for_all_columns_example

ws_clients = []
app = FastAPI()

def get_ws_clients():
    return ws_clients

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"]
)

#path = "/home/ubuntu/warehouse/DELTA_WAREHOUSE/uat.db"
################Sampling################
##################################3####
class SamplingRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    samplingMethod: str
    sampleSize: int
@app.post("/SamplingDTRequestAPI")
#calling the functions with given inputs
async def SamplingRequestAPI(sr:SamplingRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("Sampling process started") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(lambda: samplingDTRequestdef(sr.projectId,sr.reciepeId,sr.sessionId,sr.inputStepId, sr.currentStepId,sr.inputResultId, sr.samplingMethod, sr.sampleSize))
    await asyncio.wait([ws.send_text("Sampling process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
######SampleValidataion#############
####################################
class SamplingValidationRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    universeInputStepId: str
    sampleInputStepId: str
    currentStepId: Optional[str]
    universeInputResultId: str
    sampleInputResultId: str
@app.post("/SamplingValidationRequestAPI")
#calling the functions with given inputs
async def SamplingValidationRequestAPI(svr:SamplingValidationRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("Universe vs Sample Validation process started") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(lambda: samplingValidationRequestdef(svr.projectId,svr.reciepeId,svr.sessionId,svr.universeInputStepId, svr.sampleInputStepId, svr.currentStepId,svr.universeInputResultId, svr.sampleInputResultId))
    await asyncio.wait([ws.send_text("Universe vs Sample Validation process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###################columnCategoryCountPagination#############
#############################################################
class columnCategoryCountPaginationRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    columnName: Optional[str]

@app.post("/ColumnCategoriesCountPaginationRequestAPI")
# calling the functions with given inputs
def ColumnCategoriesCountPaginationRequestAPI(ccp:columnCategoryCountPaginationRequest):#, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("Preview data: Column Categories count and Pagination, process started") for ws in clients])
    Result = columnCategoryCountPaginationdef(ccp.projectId,ccp.reciepeId,ccp.sessionId,ccp.inputStepId, ccp.currentStepId,ccp.inputResultId, ccp.columnName)
    #await asyncio.wait([ws.send_text("Preview data: Column Categories count and Pagination, process ended!") for ws in clients])
    #return {f"{Result}"}
    return Response(content=Result, media_type="application/json;charset=UTF-8")
################List of Categories############################
##############################################################
class listOfCategoriesRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str

@app.post("/listOfCategoriesRequestAPI")
# calling the functions with given inputs
def listOfCategoriesRequestAPI(ccr:listOfCategoriesRequest):#, clients=Depends(get_ws_clients)):
    # await asyncio.wait([ws.send_text("List of Categories process started") for ws in clients])
    Result = listOfCategoriesdef(ccr.projectId,ccr.reciepeId,ccr.sessionId,ccr.inputStepId, ccr.currentStepId,ccr.inputResultId)
    #await asyncio.wait([ws.send_text("List of Categories process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###########DeleteColumn############################
#################################################
class deleteColumnRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnNames: List[dict]
@app.post("/deleteColumnRequestAPI")
# calling the functions with given inputs
async def deleteColumnRequestAPI(dcr:deleteColumnRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("DELETE_COLUMN_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: deleteColumndef(dcr.projectId,dcr.reciepeId,dcr.sessionId,dcr.inputStepId, dcr.currentStepId,dcr.inputResultId,dcr.columnNames))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("DELETE_COLUMN_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
################Count of missing values##########
################################################
class CountOfMissingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
@app.post("/CountOfMissingRequestAPI")
# calling the functions with given inputs
def CountOfMissingRequestAPI(cmr:CountOfMissingRequest, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("Count of missing values process started") for ws in clients])
    Result = CountOfMissingdef(cmr.projectId,cmr.reciepeId,cmr.sessionId,cmr.inputStepId, cmr.currentStepId,cmr.inputResultId)
    #await asyncio.wait([ws.send_text("Count of missing values process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#################RemoveMisMatch##############
#############################################projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,customValue,columnname,value
class customRemoveRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    customValue: str
    columnName: str
    value: Optional[list]
    columnToMatch: Optional[list]
@app.post("/customRemoveRequestAPI")
# calling the functions with given inputs
async def customRemoveRequestAPI(rmm:customRemoveRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("CUSTOM_REMOVE_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: customRemovedef(rmm.projectId,rmm.reciepeId,rmm.sessionId,rmm.inputStepId, rmm.currentStepId,rmm.inputResultId,rmm.customValue,rmm.columnName,rmm.value, rmm. columnToMatch))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("CUSTOM_REMOVE_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
##################replaceMissing################
################################################
class replaceMissingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    parameter:str
    customValue: str
    columnName: str
    replaceColumn: Optional[str]
@app.post("/replaceMissingRequestAPI")
# calling the functions with given inputs
# calling the functions with given inputs
async def replaceMissingRequestAPI(rmm:replaceMissingRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("REPLACE_MISSING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: replaceMissingdef(rmm.projectId,rmm.reciepeId,rmm.sessionId,rmm.inputStepId, rmm.currentStepId,rmm.inputResultId,rmm. parameter, rmm.customValue,rmm.columnName, rmm.replaceColumn))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("REPLACE_MISSING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
####################custom Replace###############################
###################################################################
class replacecustomRequest(BaseModel):
    # dataset location-customValue,columnName,value,valueToReplace
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    condition: str
    columnName: str
    parameter: str
    value: list
    valueToReplace: Optional[str]
    replaceColumn: Optional[str]
@app.post("/customReplaceRequestAPI")
# calling the functions with given inputs
async def replacecustomRequestAPI(rmm:replacecustomRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("REPLACE_CUSTOM_STARTED") for ws in clients])
    Result = await run_in_threadpool(
        lambda: customReplacedef(rmm.projectId,rmm.reciepeId,rmm.sessionId,rmm.inputStepId, rmm.currentStepId,rmm.inputResultId,rmm.condition,rmm.columnName,rmm.parameter,rmm.value,rmm.valueToReplace,rmm.replaceColumn))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("REPLACE_CUSTOM_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")

##################CountMissing################
################################################
class CountCustomRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    customValue: str
    columnName: str
    value : Optional[list]
@app.post("/CountCustomRequestAPI")
# calling the functions with given inputs
def CountCustomRequestAPI(cm:CountCustomRequest):#, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("COUNT_CUSTOM_STARTED") for ws in clients])
    Result = countRemovedef(cm.projectId,cm.reciepeId,cm.sessionId,cm.inputStepId, cm.currentStepId,cm.inputResultId, cm.customValue,cm.columnName,cm.value)
    #await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    #await asyncio.wait([ws.send_text("COUNT_CUSTOM_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###############ChangeDatatype################
#############################################
class changeDataTypeRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    dataType: str
@app.post("/changeDataTypeRequestAPI")
# calling the functions with given inputs
async def changeDataTypeRequestAPI(cdr:changeDataTypeRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("CHANGE_DATATYPE_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: changeDataTypedef(cdr.projectId,cdr.reciepeId,cdr.sessionId,cdr.inputStepId, cdr.currentStepId,cdr.inputResultId,cdr.columnName,cdr.dataType))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("CHANGE_DATATYPE_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
############ChangeColumnname###############
######################################
class renameColumnNameRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    NewColumnName: str
@app.post("/renameColumnNameRequestAPI")
# calling the functions with given inputs
async def renameColumnNameRequestAPI(ccn:renameColumnNameRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("RENAME_COLUMN_NAME_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: renameColumnNamedef(ccn.projectId,ccn.reciepeId,ccn.sessionId,ccn.inputStepId, ccn.currentStepId,ccn.inputResultId,ccn.columnName,ccn.NewColumnName))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("RENAME_COLUMN_NAME_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###############splitColumn#################
##################################################
class splitColumnRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    split: str
    columnName: str
    byCondition: dict
    newColumnNames: Optional[List[dict]]
@app.post("/splitColumnRequestAPI")
# calling the functions with given inputs
async def splitColumnRequestAPI(sdr: splitColumnRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("SPLIT_COLUMN_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: splitColumndef(sdr.projectId,sdr.reciepeId,sdr.sessionId,sdr.inputStepId, sdr.currentStepId,sdr.inputResultId,sdr.split, sdr.columnName,sdr.byCondition,sdr.newColumnNames))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("SPLIT_COLUMN_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#############ColumnDetails#####################
class columnDetailsRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    columnName: str
@app.post("/columnDetailsRequestAPI")
# calling the functions with given inputs
def columnDetailsRequestAPI(cdd: columnDetailsRequest, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("Column Details process started") for ws in clients])
    Result = columnDetailsdef(cdd.projectId,cdd.reciepeId,cdd.sessionId,cdd.inputStepId, cdd.currentStepId,cdd.inputResultId,cdd.columnName)
    #await asyncio.wait([ws.send_text("Column Details process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###############################################
###############Masking#########################projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnname,condition,s,valuetoreplace,start,finish
class MaskingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnname: str
    condition : str
    value: Optional[str]
    #value_1: str
    valuetoreplace: Optional[str]
    start : Optional[int]
    finish : Optional[int]

@app.post("/MaskingRequestApi")
# calling the functions with given inputs
async def MaskingRequestApi(rm: MaskingRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("MASK_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: maskingreq(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnname,rm.condition,rm.value,rm.valuetoreplace,rm.start,rm.finish))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("MASK_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###################################################
################OrdinalEncoding####################
class OrdinalEncodingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    col_name: str
    col_name_ordinal: str
    scale_mapper : dict
@app.post("/OrdinalRequestApi")
# calling the functions with given inputs
async def OrdinalRequestApi(oer:OrdinalEncodingRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: ordinal_function(oer.projectId,oer.reciepeId,oer.sessionId,oer.inputStepId, oer.currentStepId,oer.inputResultId,oer.col_name,oer.col_name_ordinal,oer.scale_mapper))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###################################################
################OrdinalEncoding####################
class OneHotEncodingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnames: str
    
@app.post("/OneHotEncodingRequestApi")
# calling the functions with given inputs
async def OneHotEncodingRequestApi(oer:OneHotEncodingRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: OneHotEncodingTech(oer.projectId,oer.reciepeId,oer.sessionId,oer.inputStepId, oer.currentStepId,oer.inputResultId,oer.columnames))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###################################################
####################IQR Capping####################
class IQRRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnvalues: list

@app.post("/IQRRequestApi")
# calling the functions with given inputs
async def IQRRequestApi(rm: IQRRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("IQR_OUTLIER_CAPPING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: iqr_for_all_columns(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnvalues))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("IQR_OUTLIER_CAPPING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###################################################
#################Remove Outliers###################
class RemoveOutliers(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    column: str

@app.post("/RemoveOutliersRequestApi")
# calling the functions with given inputs
async def RemoveOutliersRequestApi(rm: RemoveOutliers, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("REMOVE_OUTLIERS_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: remove_outliers(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.column))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("REMOVE_OUTLIERS_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###################################################
##################CountOutliers####################
class CountOutliers(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    column: str

@app.post("/CountOutliersRequestApi")
# calling the functions with given inputs
def CountOutliersRequestApi(rm: RemoveOutliers, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("IQR outlier capping process started") for ws in clients])
    Result = display_outliers(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.column)
    #await asyncio.wait([ws.send_text("IQR outlier capping process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")


###################################################
##################Cap_Percentile_Outlier####################
class CapPercentileRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnname: str 
    maxthreshold: str
    minthreshold: str

@app.post("/CapPercentileRequestApi")
# calling the functions with given inputs
async def CapPercentileRequestApi(pr: CapPercentileRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("CAP_PERCENTILE_OUTLIER_CAPPING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: Cap_Percentile_Outlier(pr.projectId,pr.reciepeId,pr.sessionId,pr.inputStepId,pr.currentStepId,pr.inputResultId, pr.columnname,pr.maxthreshold,pr.minthreshold))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("CAP_PERCENTILE_OUTLIER_CAPPING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###################################################
##################RemovePercentileRequest####################
class RemovePercentileRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnname: str 
    maxthreshold: str
    minthreshold:str

@app.post("/RemovePercentileRequestApi")
# calling the functions with given inputs
async def RemovePercentileRequestApi(pr: RemovePercentileRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("REMOVE_PERCENTILE_OUTLIER_CAPPING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: Remove_Percentile_Outlier(pr.projectId,pr.reciepeId,pr.sessionId,pr.inputStepId,pr.currentStepId,pr.inputResultId, pr.columnname,pr.maxthreshold,pr.minthreshold))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("REMOVE_PERCENTILE_OUTLIER_CAPPING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###################################################
##################RuleBasedRequest####################
class RuleBasedRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    lowerbound: str
    upperbound: str
    columnnames: str

@app.post("/RuleBasedRequestApi")
# calling the functions with given inputs
async def RuleBasedRequestApi(pr: RuleBasedRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("RULEBASED_OUTLIER_CAPPING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: Rulebased_Outlier(pr.projectId,pr.reciepeId,pr.sessionId,pr.inputStepId,pr.currentStepId,pr.inputResultId, pr.lowerbound,pr.upperbound,pr.columnnames))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("RULEBASED_OUTLIER_CAPPING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
################join###################
########################################
class JoinDatasetsRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    stepId: List[dict]
    leftInputDetails :dict
    rightInputDetails: dict
    columnMapping: List[dict]
    currentStepId: str
    mergeType:str

@app.post("/JoinDatasetsRequestAPI")
# calling the functions with given inputs
async def JoinDatasetsRequestAPI(jdr: JoinDatasetsRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("JOIN_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: JoinDatasets(jdr.projectId,jdr.reciepeId,jdr.sessionId,jdr.stepId, jdr.leftInputDetails, jdr.rightInputDetails, jdr.columnMapping,jdr.currentStepId,jdr.mergeType))
    await asyncio.wait([ws.send_text("JOIN_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
################DataQualityScoreAPI##########
############################################
class DataQualityScoreRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    initalInputStepId: str
    finalInputStepId: str
    currentStepId: Optional[str]
    initalInputResultId: str
    finalInputResultId: str
    domainSelected: dict
    dataTypeSelected: dict
@app.post("/DataQualityScoreRequestAPI")
#calling the functions with given inputs
def DataQualityScoreRequestAPI(svr:DataQualityScoreRequest, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("Data Quality score process started") for ws in clients])
    Result = DataQualityScoredef(svr.projectId,svr.reciepeId,svr.sessionId,svr.initalInputStepId, svr.finalInputStepId, svr.currentStepId,svr.initalInputResultId, svr.finalInputResultId, svr.domainSelected, svr.dataTypeSelected)
    #await asyncio.wait([ws.send_text("Data Quality score process ended!") for ws in clients])
    #return {f"{Result}"}
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#########################################################
####################firstCellTransformation######################
class firstCellTransformationRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    transformation: str
    columnName: str
@app.post("/firstCellTransformationRequestAPI")
#calling the functions with given inputs
def firstCellTransformationRequestAPI(fct:firstCellTransformationRequest, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("FIRST CELL INFORMATION STARTED") for ws in clients])
    Result = firstCellTransformationdef(fct.projectId,fct.reciepeId,fct.sessionId,fct.inputStepId, fct.currentStepId,fct.inputResultId, fct.transformation, fct.columnName)
    #await asyncio.wait([ws.send_text("FIRST CELL INFORMATION ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#########################################
@app.websocket("/notify")
async def websocket_endpoint(websocket: WebSocket, clients=Depends(get_ws_clients)):
    await websocket.accept()
    clients.append(websocket)
    try:
        while True:
            _ = await websocket.receive_text()
    finally:
        ws_clients.remove(websocket)
###########################################################
class samplingValidationpandasRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    universeInputStepId: str
    sampleInputStepId: str
    currentStepId: Optional[str]
    universeInputResultId: str
    sampleInputResultId: str
@app.post("/samplingValidationpandasRequestAPI")
#calling the functions with given inputs
async def samplingValidationpandasRequestAPI(svr:samplingValidationpandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("Universe vs Sample Validation process started") for ws in clients])
    Result = samplingValidationpandas(svr.projectId,svr.reciepeId,svr.sessionId,svr.universeInputStepId, svr.sampleInputStepId, svr.currentStepId,svr.universeInputResultId, svr.sampleInputResultId)
    await asyncio.wait([ws.send_text("Universe vs Sample Validation process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
###########################################################
class columnPaginationPandasRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
@app.post("/columnPaginationPandasRequestAPI")
#calling the functions with given inputs
async def columnPaginationPandasRequestAPI(cpp:columnPaginationPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("columnPaginationPandasRequest process started") for ws in clients])
    Result = columnPaginationPandasDef(cpp.projectId,cpp.reciepeId,cpp.sessionId, cpp.inputStepId, cpp.currentStepId, cpp.inputResultId)
    await asyncio.wait([ws.send_text("columnPaginationPandasRequest process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#########################################################################################
####################iqrpandastest#################################
class IQRRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnvalues: list

@app.post("/IQRRequestApiPandas")
# calling the functions with given inputs
async def IQRRequestApi(rm: IQRRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("IQR_OUTLIER_CAPPING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: iqr_for_all_columns_pandas(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnvalues))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("IQR_OUTLIER_CAPPING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
############################################################
################OrdinalEncodingPandasNew####################
class OrdinalEncodingPandas(BaseModel):
    # dataset location
    #filename,colname,newcolname,mappingobjects,outputfilelocation
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    #filename: str
    colname: str
    newcolname: str
    mappingobjects: dict
    
@app.post("/OrdinalEncodingRequestApi")
# calling the functions with given inputs
async def OrdinalEncodingRequestApi(oerp:OrdinalEncodingPandas, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: ordinal_encoding_pandas(oerp.projectId,oerp.reciepeId,oerp.sessionId,oerp.inputStepId,oerp.currentStepId,oerp.inputResultId,oerp.colname,oerp.newcolname,oerp.mappingobjects))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###########################################################
###########################################################s
class pivotRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    column: str
    rows: List
    values: dict
@app.post("/pivotRequestAPI")
#calling the functions with given inputs
async def pivotRequestAPI(cpp:pivotRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("PIVOT_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: pivot_def(cpp.projectId,cpp.reciepeId,cpp.sessionId, cpp.inputStepId, cpp.currentStepId, cpp.inputResultId, cpp.column, cpp.rows, cpp.values))
    await asyncio.wait([ws.send_text("PIVOT_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#########Replace MisMatchDatatype######################
class replaceMismatchDatatypeRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    dataType: str
    parameter: str
    valueToReplace: str
    replaceColumn: Optional[str]

@app.post("/replaceMismatchDatatypeRequestAPI")
#calling the functions with given inputs
async def replaceMismatchDatatypeRequestAPI(sr:replaceMismatchDatatypeRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("REPLACE_MISMATCH_DATATYPE_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: replaceMismatchDatatype(sr.projectId,sr.reciepeId,sr.sessionId,sr.inputStepId, sr.currentStepId,sr.inputResultId, sr.columnName, sr.dataType,sr.parameter, sr.valueToReplace, sr. replaceColumn))
    await asyncio.wait([ws.send_text("REPLACE_MISMATCH_DATATYPE_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
##############Remove Mismatchdatatype##############
class removeMismatchDatatypeRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    dataType: str

@app.post("/removeMismatchDatatypeRequestAPI")
#calling the functions with given inputs
async def removeMismatchDatatypeRequestAPI(sr:removeMismatchDatatypeRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("REMOVE_MISMATCH_DATATYPE_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: removeMismatchDatatype(sr.projectId,sr.reciepeId,sr.sessionId,sr.inputStepId, sr.currentStepId,sr.inputResultId, sr.columnName, sr.dataType))
    await asyncio.wait([ws.send_text("REMOVE_MISMATCH_DATATYPE_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
    
    
###################################################
################Onehot####################
class OneHotEncodingRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnames: str
    
@app.post("/OneHotEncodingRequestApiexample")
# calling the functions with given inputs
async def OneHotEncodingRequestApi(oer:OneHotEncodingRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_STARTED") for ws in clients])
    Result = OneHotEncodingTech(oer.projectId,oer.reciepeId,oer.sessionId,oer.inputStepId, oer.currentStepId,oer.inputResultId,oer.columnames)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
    
###################################################
####################IQR Capping spark####################
class IQRRequestExample(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnvalues: list

@app.post("/IQRRequestApiexample")
# calling the functions with given inputs
async def IQRRequestApiexample(rm: IQRRequestExample, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("IQR_OUTLIER_CAPPING_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: iqr_for_all_columns_example(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnvalues))
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("IQR_OUTLIER_CAPPING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
################FUZZY LOOKUP####################
#Fuzzy Matcher
class fuzzyMatchRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    leftStepID: str
    leftResultID: str
    rightStepID :str
    rightResultID: str
    currentStepId: str
    columnMapping: List[dict]
    threshold: str

@app.post("/fuzzyMatchRequestAPI")
# calling the functions with given inputs
async def fuzzyMatchRequestAPI(fmr: fuzzyMatchRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("FUZZYMATCH_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: fuzzyMatch_Def(fmr.projectId, fmr.reciepeId, fmr.sessionId, fmr.leftStepID, fmr.leftResultID, fmr.rightStepID, fmr.rightResultID,
                       fmr.currentStepId, fmr.columnMapping, fmr.threshold))
    await asyncio.wait([ws.send_text("FUZZYMATCH_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#RecordLinkage
class recordLinkageRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    leftStepID: str
    leftResultID: str
    rightStepID :str
    rightResultID: str
    currentStepId: str
    consistentColumn: dict
    columnMapping: List[dict]

@app.post("/recordLinkageRequestAPI")
# calling the functions with given inputs
async def recordLinkageRequestAPI(rlr: recordLinkageRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("RECORDLINKAGE_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: recordLinkage_Def(rlr.projectId, rlr.reciepeId, rlr.sessionId, rlr.leftStepID, rlr.leftResultID, rlr.rightStepID, rlr.rightResultID,
                       rlr.currentStepId, rlr.consistentColumn, rlr.columnMapping))
    await asyncio.wait([ws.send_text("RECORDLINKAGE_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
########################################################################################
class duplicateColumnRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    newColumnName: str
@app.post("/duplicateColumnRequestAPI")
#calling the functions with given inputs
async def duplicateColumnRequestAPI(sr:duplicateColumnRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("DUPLICATE_COLUMN_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: duplicateColumndef(sr.projectId,sr.reciepeId,sr.sessionId,sr.inputStepId, sr.currentStepId,sr.inputResultId, sr.columnName, sr.newColumnName))
    await asyncio.wait([ws.send_text("DUPLICATE_COLUMN_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
####################IQR Graph###############
class IQRGraphRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    inputResultId: str
    columnName: str
@app.post("/IQRGraphRequestAPI")
#calling the functions with given inputs
def IQRGraphRequestAPI(cpp:IQRGraphRequest, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("IQR graph request started") for ws in clients])
    Result = IQR_graph_data(cpp.projectId,cpp.reciepeId,cpp.sessionId, cpp.inputStepId, cpp.inputResultId, cpp.columnName)
    #await asyncio.wait([ws.send_text("IQR graph request ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")

#########percentile Graph##############
class PercentileGraphRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    inputResultId: str
    columnName: str
@app.post("/PercentileGraphRequestAPI")
#calling the functions with given inputs
def PercentileGraphRequestAPI(cpp:PercentileGraphRequest, clients=Depends(get_ws_clients)):
    #await asyncio.wait([ws.send_text("IQR graph request started") for ws in clients])
    Result = Percentile_graph_data(cpp.projectId,cpp.reciepeId,cpp.sessionId, cpp.inputStepId, cpp.inputResultId, cpp.columnName)
    #await asyncio.wait([ws.send_text("IQR graph request ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#################UNION################################
class UNIONRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    stepId: List[dict]
    currentStepId: str
    columnNames: list
    columnMapping: list
@app.post("/UNIONRequestAPI")
#calling the functions with given inputs
async def UNIONRequestAPI(UNION:UNIONRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("UNION_STARTED") for ws in clients])
    # Run the process that is not asyncronous in asycronous way with the fuction run_in_threadpool.
    Result = await run_in_threadpool(
        lambda: UNIONdef(UNION.projectId,UNION.reciepeId,UNION.sessionId,UNION.stepId, UNION.currentStepId,UNION.columnNames, UNION.columnMapping))
    await asyncio.wait([ws.send_text("UNION_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
#########################################################


###################################################################################----PANDAS----#####################################################################################



##################replaceMissing################
################################################
from replacemissing_pandas import replaceMissingPandasdef
from changeDatatype_pandas import changeDataTypedefpandas
from Renamecolumnname_pandas import renameColumnNamepandasdef
from deleteColumn_pandas import deleteColumnpandasdef
from firstCellTransformation_pandas import firstCellTransformationPandasdef
from listOfCategories_pandas import listOfCategoriesPandasdef
from countOfMissingRequest_pandas import CountOfMissingPandasdef
from columnDetails_pandas import columnDetailsPandasdef
from Join_pandas import JoinDatasetsPandas
from count_outlier_iqr_pandas import display_outliers_pandas
 
##################replaceMissing################
################################################
class replaceMissingPandasRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    parameter:str
    customValue: str
    columnName: str
    replaceColumn: Optional[str]
@app.post("/replaceMissingPandasRequestAPI")
# calling the functions with given inputs
# calling the functions with given inputs
async def replaceMissingPandasRequestAPI(rmm:replaceMissingPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("REPLACE_MISSING_STARTED") for ws in clients])
    Result = replaceMissingPandasdef(rmm.projectId,rmm.reciepeId,rmm.sessionId,rmm.inputStepId, rmm.currentStepId,rmm.inputResultId,rmm. parameter, rmm.customValue,rmm.columnName, rmm.replaceColumn)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("REPLACE_MISSING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###############ChangeDatatype################
#############################################
class changeDataTypePandasRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    dataType: str
@app.post("/changeDataTypePandasRequestAPI")
# calling the functions with given inputs
async def changeDataTypePandasRequestAPI(cdr:changeDataTypePandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("CHANGE_DATATYPE_STARTED") for ws in clients])
    Result = changeDataTypedefpandas(cdr.projectId,cdr.reciepeId,cdr.sessionId,cdr.inputStepId, cdr.currentStepId,cdr.inputResultId,cdr.columnName,cdr.dataType)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("CHANGE_DATATYPE_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
############renameColumnName###############
######################################
class renameColumnNamePandasRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnName: str
    NewColumnName: str
@app.post("/renameColumnNamePandasRequestAPI")
# calling the functions with given inputs
async def renameColumnNamePandasRequestAPI(ccn:renameColumnNamePandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("RENAME_COLUMN_NAME_STARTED") for ws in clients])
    Result = renameColumnNamepandasdef(ccn.projectId,ccn.reciepeId,ccn.sessionId,ccn.inputStepId, ccn.currentStepId,ccn.inputResultId,ccn.columnName,ccn.NewColumnName)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("RENAME_COLUMN_NAME_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###########DeleteColumn############################
#################################################
class deleteColumnPandasRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnNames: str
@app.post("/deleteColumnPandasRequestAPI")
# calling the functions with given inputs
async def deleteColumnPandasRequestAPI(dcr:deleteColumnPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("DELETE_COLUMN_STARTED") for ws in clients])
    Result = deleteColumnpandasdef(dcr.projectId,dcr.reciepeId,dcr.sessionId,dcr.inputStepId, dcr.currentStepId,dcr.inputResultId,dcr.columnNames)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("DELETE_COLUMN_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
#########################################################
####################firstCellTransformation######################
class firstCellTransformationPandasRequest(BaseModel):
    #dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    transformation: str
    columnName: str
@app.post("/firstCellTransformationPandasRequestAPI")
#calling the functions with given inputs
async def firstCellTransformationPandasRequestAPI(fct:firstCellTransformationPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("FIRST CELL INFORMATION STARTED") for ws in clients])
    Result = firstCellTransformationPandasdef(fct.projectId,fct.reciepeId,fct.sessionId,fct.inputStepId, fct.currentStepId,fct.inputResultId, fct.transformation, fct.columnName)
    await asyncio.wait([ws.send_text("FIRST CELL INFORMATION ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
################List of Categories############################
##############################################################
class listOfCategoriesPandasRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str

@app.post("/listOfCategoriesPandasRequestAPI")
# calling the functions with given inputs
async def listOfCategoriesPandasRequestAPI(ccr:listOfCategoriesPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("List of Categories process started") for ws in clients])
    Result = listOfCategoriesPandasdef(ccr.projectId,ccr.reciepeId,ccr.sessionId,ccr.inputStepId, ccr.currentStepId,ccr.inputResultId)
    await asyncio.wait([ws.send_text("List of Categories process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
################Count of missing values##########
################################################
class CountOfMissingPandasRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
@app.post("/CountOfMissingPandasRequestAPI")
# calling the functions with given inputs
async def CountOfMissingPandasRequestAPI(cmr:CountOfMissingPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("Count of missing values process started") for ws in clients])
    Result = CountOfMissingPandasdef(cmr.projectId,cmr.reciepeId,cmr.sessionId,cmr.inputStepId, cmr.currentStepId,cmr.inputResultId)
    await asyncio.wait([ws.send_text("Count of missing values process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
#############ColumnDetails#####################
class columnDetailsPandasRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    columnName: str
@app.post("/columnDetailsPandasRequestAPI")
# calling the functions with given inputs
async def columnDetailsPandasRequestAPI(cdd: columnDetailsPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("Column Details process started") for ws in clients])
    Result = columnDetailsPandasdef(cdd.projectId,cdd.reciepeId,cdd.sessionId,cdd.inputStepId, cdd.currentStepId,cdd.inputResultId,cdd.columnName)
    await asyncio.wait([ws.send_text("Column Details process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
################join###################
########################################
class JoinDatasetsPandasRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    stepId: List[dict]
    leftInputDetails :dict
    rightInputDetails: dict
    columnMapping: List[dict]
    currentStepId: str
    mergeType:str

@app.post("/JoinDatasetsPandasRequestAPI")
# calling the functions with given inputs
async def JoinDatasetsPandasRequestAPI(jdr: JoinDatasetsPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("JOIN_STARTED") for ws in clients])
    Result = JoinDatasetsPandas(jdr.projectId,jdr.reciepeId,jdr.sessionId,jdr.stepId, jdr.leftInputDetails, jdr.rightInputDetails, jdr.columnMapping,jdr.currentStepId,jdr.mergeType)
    await asyncio.wait([ws.send_text("JOIN_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
    
###################################################
##################CountOutliersPandas####################
class CountOutliersPandas(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: Optional[str]
    inputResultId: str
    column: str

@app.post("/CountOutliersPandasRequestApi")
# calling the functions with given inputs
async def CountOutliersPandasRequestApi(rm: CountOutliersPandas, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("IQR outlier capping process started") for ws in clients])
    Result = display_outliers_pandas(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.column)
    await asyncio.wait([ws.send_text("IQR outlier capping process ended!") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
from replace_predefind import customReplacedefmissing
####################custom Replacemissing###############################
###################################################################
class customReplaceRequestAPImissing(BaseModel):
    # dataset location-customValue,columnName,value,valueToReplace
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    condition: str
    columnName: str
    parameter: str
    #value: list
    valueToReplace: Optional[str]
    replaceColumn: Optional[str]
@app.post("/customReplaceRequestAPImissing")
# calling the functions with given inputs
async def replacecustomRequestmissingAPI(rmm:customReplaceRequestAPImissing, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("REPLACE_CUSTOM_STARTED") for ws in clients])
    Result = customReplacedefmissing(rmm.projectId,rmm.reciepeId,rmm.sessionId,rmm.inputStepId, rmm.currentStepId,rmm.inputResultId,rmm.condition,rmm.columnName,rmm.parameter,rmm.valueToReplace,rmm.replaceColumn)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("REPLACE_CUSTOM_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
################join###################

class JoinDatasetsPandasRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    stepId: List[dict]
    leftInputDetails :dict
    rightInputDetails: dict
    columnMapping: List[dict]
    currentStepId: str
    mergeType:str

@app.post("/JoinDatasetsPandasRequestAPI")
# calling the functions with given inputs
async def JoinDatasetsPandasRequestAPI(jdr: JoinDatasetsPandasRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("JOIN_STARTED") for ws in clients])
    Result = JoinDatasetsPandas(jdr.projectId,jdr.reciepeId,jdr.sessionId,jdr.stepId, jdr.leftInputDetails, jdr.rightInputDetails, jdr.columnMapping,jdr.currentStepId,jdr.mergeType)
    await asyncio.wait([ws.send_text("JOIN_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")

    
    
#-------------------------------------------------------------------------SPARK------------------------------------------------------------------------------------------


from custom_remove_spark import customRemovesparkdef
from splitColumn_spark import splitColumndefspark
from masking_spark import maskingreqspark
from outliercapping_spark import PercentileoutlierSpark,IQRcappingoutlierSpark
from one_hot_spark import OneHotEncodingSparkTech

#################RemoveMisMatch##############
#############################################projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,customValue,columnname,value
class customRemoveRequestSpark(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    customValue: str
    columnName: str
    value: Optional[list]
    columnToMatch: Optional[list]
@app.post("/customRemoveRequestSparkAPI")
# calling the functions with given inputs
async def customRemoveRequestSparkAPI(rmm:customRemoveRequestSpark, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("CUSTOM_REMOVE_STARTED") for ws in clients])
    Result = customRemovesparkdef(rmm.projectId,rmm.reciepeId,rmm.sessionId,rmm.inputStepId, rmm.currentStepId,rmm.inputResultId,rmm.customValue,rmm.columnName,rmm.value, rmm. columnToMatch)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("CUSTOM_REMOVE_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###############splitColumn#################
##################################################
class splitColumnSparkRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    split: str
    columnName: str
    byCondition: dict
    newColumnNames: Optional[List[dict]]
@app.post("/splitColumnSparkRequestAPI")
# calling the functions with given inputs
async def splitColumnSparkRequestAPI(sdr: splitColumnSparkRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("SPLIT_COLUMN_STARTED") for ws in clients])
    Result = splitColumndefspark(sdr.projectId,sdr.reciepeId,sdr.sessionId,sdr.inputStepId, sdr.currentStepId,sdr.inputResultId,sdr.split, sdr.columnName,sdr.byCondition,sdr.newColumnNames)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("SPLIT_COLUMN_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###############################################
###############Masking#########################projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnname,condition,s,valuetoreplace,start,finish
class MaskingsparkRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnname: str
    condition : str
    value: Optional[str]
    #value_1: str
    valuetoreplace: Optional[str]
    start : Optional[int]
    finish : Optional[int]

@app.post("/MaskingSparkRequestApi")
# calling the functions with given inputs
async def MaskingSparkRequestApi(rm: MaskingsparkRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("MASK_STARTED") for ws in clients])
    Result = maskingreqspark(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnname,rm.condition,rm.value,rm.valuetoreplace,rm.start,rm.finish)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("MASK_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###############################################
###############PercentileOutlier###############projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,data, columnname, lower_percentile, upper_percentile):
class PercentileOutliersparkRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnname: str
    lower_percentile : float
    upper_percentile: float

@app.post("/PercentileOutliersparkRequestApi")
# calling the functions with given inputs
async def PercentileOutliersparkRequestApi(rm: PercentileOutliersparkRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("MASK_STARTED") for ws in clients])
    Result = PercentileoutlierSpark(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnname,rm.lower_percentile,rm.lower_percentile)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("MASK_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    
###############################################
###############IQROutlier###############projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,data, columnname, lower_percentile, upper_percentile):
class IQROutliersparkRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnname: str

@app.post("/IQROutliersparkRequestApi")
# calling the functions with given inputs
async def IQROutliersparkRequestApi(rm: IQROutliersparkRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("MASK_STARTED") for ws in clients])
    Result = IQRcappingoutlierSpark(rm.projectId,rm.reciepeId,rm.sessionId,rm.inputStepId,rm.currentStepId,rm.inputResultId, rm.columnname)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("MASK_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")
    

###################################################
################OneHotEncodingRequest####################
class OneHotEncodingSparkRequest(BaseModel):
    # dataset location
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    columnames: str
    
@app.post("/OneHotEncodingSparkRequestApi")
# calling the functions with given inputs
async def OneHotEncodingSparkRequestApi(oer:OneHotEncodingSparkRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_STARTED") for ws in clients])
    Result = OneHotEncodingSparkTech(oer.projectId,oer.reciepeId,oer.sessionId,oer.inputStepId, oer.currentStepId,oer.inputResultId,oer.columnames)
    await asyncio.wait([ws.send_text({f"{Result}"}) for ws in clients])
    await asyncio.wait([ws.send_text("ORDINAL_ENCODING_ENDED") for ws in clients])
    return Response(content=Result, media_type="application/json;charset=UTF-8")