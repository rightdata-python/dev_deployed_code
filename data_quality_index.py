import datetime
import pandas as pd
import math
import numpy as np
import time
import sys
#sys.path.append('C:/Users/gasto/OneDrive/RightData/API')
from data_domain_errors import DataDomainErrors

def data_availability_score(df):
    proportion_nulls = df.isna().sum() / df.shape[0]
    proportion_not_nulls = 1 - proportion_nulls
    mean_not_null = proportion_not_nulls.mean()
    return mean_not_null

def functional_quality_domain_score(df, domain_selected, mask=None):
    if type(mask) == pd.core.frame.DataFrame:
        df_new = df[mask].copy()
    else:
        df_new = df.copy()
    model_error = DataDomainErrors()
    data_domain_errors = model_error.transform(df_new, domain_selected)
    valid_domain = [100 - data_domain_errors[key] if data_domain_errors[key] != 'Invalid data domain' else 0 for key in data_domain_errors.keys()]
    proportion_valid_domain = np.mean(valid_domain) / 100
    proportion_valid_domain = proportion_valid_domain.round(3)
    return proportion_valid_domain

# POSSIBLES NUMBER TYPES DETECTOR
def is_float(value, not_integer=True):
    """
    Funtion to check if value is float or not.

    Parameters
    ----------
    value: str.
        Value to analize.

    Returns
    -------
    True if value is float or False is not.
    """
    if pd.api.types.is_bool(value):
        return False
    try:
        float_value = float(value)
        int_mask = float_value.is_integer()# & not_integer
        if math.isnan(float_value) | int_mask:
            return False
        return True
    except ValueError:
        return False

# Check if value is integer
def is_integer(value):
    """
    Funtion to check if value is integer or not.

    Parameters
    ----------
    value: str.
        Value to analize.

    Returns
    -------
    True if value is integer or False is not.
    """
    if pd.api.types.is_bool(value):
        return False
    try:
        float_value = float(value)
        if float_value.is_integer():
            return True
        return False
    except ValueError:
        return False
    
# Define a simple function to check if value is boolean
def is_bool(x):
    """
    Funtion to check if value is boolean or not.

    Parameters
    ----------
    x: str.
        Value to analize.

    Returns
    -------
    True if value is boolean or False is not.
    """
    possible_bool = [True, False, 'True', 'False']
    if (x in possible_bool) & (pd.api.types.is_integer(x) != True) & (pd.api.types.is_float(x) != True):
        return True
    return False
    
def is_string(x):
    """
    Funtion to check if value is string or not.

    Parameters
    ----------
    x: str.
        Value to analize.

    Returns
    -------
    True if value is string or False is not.
    """    
    
    if x in [True, False, 'True', 'False']:
        return False
    
    try:
        float(x)
        return False
    except:
        return True
    
# POSSIBLES DATE DETECTOR
def get_date_serie(serie):
    clean_serie = serie.dropna().astype(str)
    valid_date = pd.to_datetime(clean_serie, errors='coerce')
    valid_date_mask = ~valid_date.isna()
    return valid_date_mask

# POSSIBLES TIME TYPES DETECTOR
def is_time(value):
    """
    Funtion to check if value is time or not.

    Parameters
    ----------
    value: str.
        Value to analize.

    Returns
    -------
    True if value is time or False is not.
    """
    str_value = str(value)
    if str_value.isnumeric():
        return False
    date_value = pd.to_timedelta(str_value, errors='coerce')
    if type(date_value) == pd._libs.tslibs.nattype.NaTType:
        return False
    return True

# Simple function to get the proportion of valid data types
def get_valid_percentage(serie):
    clean_serie = serie.dropna()
    shape = clean_serie.shape[0]
    return (1 - (shape - clean_serie.sum()) / shape)

def technical_quality_score(df, dtype_selected, method='mask'):
    """
    Function to get the invalid data types selected

    Parameters
    ----------
    df: pandas data frame.
        Data frame with the columnas to analyze.
    dtype_selected: dictionary
        The key are equal to the column and the value are equal to the desired data type.. The available are:
        ['StringType', "IntegerType", "FloatType", "BooleanType", "DateType", "TimestampType"]
    method: string
        The method that you want to run. The available are:
        ['mask', 'count']

    Returns 
    -------
    valid_types|invalid_serie: pandas data frame or pandas serie
        valid_types is a pandas data frame with the mask of all the valid data types value
        invalid_serie is a pandas serie with the count invalid value.
    """
    # First select the columns for the dtypes selected for the client
    col_string = [key for key, value in dtype_selected.items() if value == "StringType"]
    col_int = [key for key, value in dtype_selected.items() if value in ["IntegerType", "LongType", "ShortType"]]
    col_float = [key for key, value in dtype_selected.items() if value in ["FloatType", "DoubleType"]]
    col_bool = [key for key, value in dtype_selected.items() if value == "BooleanType"]
    col_date = [key for key, value in dtype_selected.items() if value == "DateType"]
    col_time = [key for key, value in dtype_selected.items() if value == "TimestampType"]

    def get_valid_mask(serie, method):
        clean_serie = serie.dropna()
        valid = clean_serie.apply(method)
        return valid 

    def count_invalid(serie):
        clean_serie = serie.dropna()
        valid = clean_serie.sum()
        invalid = clean_serie.shape[0] - valid
        return invalid 

    # import asyncio
    # await asyncio.sleep(30)
    # Apply the different dtypes function depend of the dtypes that the client selected
    # With this we get the mask for the different dtypes, where the 
    int_series = df.filter(col_int).apply(lambda x: get_valid_mask(x, is_integer))

    float_series = df.filter(col_float).apply(lambda x: get_valid_mask(x, is_float))

    string_series = df.filter(col_string).apply(lambda x: get_valid_mask(x, is_string))

    bool_series = df.filter(col_bool).apply(lambda x: get_valid_mask(x, is_bool))

    date_series = df.filter(col_date).apply(get_date_serie)

    time_series = df.filter(col_time).apply(lambda x: get_valid_mask(x, is_time))

    # Concat the different data types data frames
    valid_types = pd.concat([int_series, float_series, string_series, bool_series, date_series, time_series], axis=1)
    
    if method == 'mask':
        return valid_types
    elif method == 'count':
        invalid_serie = valid_types.apply(count_invalid)
        return invalid_serie
    elif method == 'dtype_score':
        dtype_score = valid_types.apply(get_valid_percentage).mean()
        return dtype_score
    else:
        return 'Invalid method'

### generic mismatch function
def alldtype_mismatch(col, data,  desired_datatype_dict):
    cnt=0
    clean_serie=data[col].dropna()

    for i in clean_serie:
        if isinstance(i, desired_datatype_dict[col]):
            cnt+=1
    z = clean_serie.shape[0] - cnt
    per=(z/len(data[col]))*100
    return per

### function to call
#alldtype_mismatch(data, col, desired_datatype_dict[col])

def dtype_mismatch(data,desired_datatype_dict):
    from itertools import repeat
    col_list = data.columns.tolist()
    
    # replace the desiere data type to the real one
    real_datatype = pd.Series(desired_datatype_dict)
    real_datatype = real_datatype.replace({'Integer': int, 'Float': float, 'String': str, 
                                           'Date': datetime.datetime, 'Time': datetime.timedelta,
                                           'Boolean': bool}).to_dict()
    
    ##wrapper function applies to a single column
    results= map(alldtype_mismatch, col_list, repeat(data), repeat(real_datatype))
    result_list=[]

    for result in results:
        result_list.append(result)

#     result_dict= dict(pd.DataFrame({'column': desired_datatype_dict.keys(), 'invalid_percentage': result_list}).values)
    result_dict = pd.DataFrame({'column': desired_datatype_dict.keys(), 'invalid_percentage': result_list})
    mean_invalid_types = result_dict.invalid_percentage.mean()
    mean_valid_types = 100 - mean_invalid_types
    
    return mean_valid_types



def data_quality_index(df, domain_selected, dtype_selected, type_model='first', calculate_valid=False):
    """
    Funtion to get data quality index

    Parameters
    ----------
    df: pandas data frame.
        Data frame with the columnas to analize.
        
    domain_selected: dict
        Dictionary with the domain selected for the columns
        
    dtype_selected: dict
        Dictionary with the types selected for the columns
    
    type_model: String
        The model that you want to use for evaluate the data types selected. If you select
        the value first, the model selected is the technical_quality_score that evalute if a value is from this data types.
        If you select second, the model selected is dtype_mismatch, that only take in account the type of the value to evaluate.

    Returns
    -------
    quality_index: float
        Data quality index for the data frame. This value go from 0 to 1. The value 1 is the best data quality score
    """
    df = df.replace({'nan': np.NaN, 'NaN': np.NaN})
    # Calculate the data_availability_score that take in account the null values
    notnull_score = data_availability_score(df)
    # Select the model for the data types
    if type_model == 'first':
        # Caculate the score for the data types selected
        quality_types_mask = technical_quality_score(df, dtype_selected, method='mask')
        quality_types_score = quality_types_mask.apply(get_valid_percentage).mean()
    else:
        # Caculate the score for the data types selected
        quality_types_score = dtype_mismatch(df, dtype_selected)
        
    # Calculate the functional_quality_domain_score that take in account the data domain
    domain_quality_score = functional_quality_domain_score(df, domain_selected)
        
    # Calculate the data quality score
    quality_index = notnull_score * domain_quality_score * quality_types_score
    
    if calculate_valid:
        # Calculate the valid and invalid data
        domain_score_without_dtypes = functional_quality_domain_score(df, domain_selected, quality_types_mask)
        null = 1 - notnull_score
        invalid_types = notnull_score * (1 - quality_types_score)
        invalid_domain = (notnull_score * quality_types_score) * (1 - domain_score_without_dtypes)
        proportion_invalid_data = null + invalid_types + invalid_domain
        proportion_valid_data = 1 - proportion_invalid_data

        return quality_index, notnull_score, domain_quality_score, quality_types_score,  proportion_valid_data, proportion_invalid_data
    
    return quality_index, notnull_score, domain_quality_score, quality_types_score