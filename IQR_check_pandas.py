import pandas as pd
from sklearn.preprocessing import OrdinalEncoder
import numpy as np
from pyspark.sql.functions import lit, rand, when
import pyspark
import json
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
from pyspark.sql.functions import lit, col,isnan, when, count, mean as _mean, stddev as _stddev
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def iqr_for_all_columns_pandas(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnvalues):
    # col_vals = ["HP"]

    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    InputTable = path + "/" + inputDeltaTable
    print("Reading Delta Table")
    try:
        Data = spark.read.format("delta").load(InputTable)
        columnname = columnvalues[0]
        print("selected column:", columnname)
        df = Data.toPandas()
        df1 = df.copy()
        df = df._get_numeric_data()
        columnvalues = []
        # q1 = df[columnname].quantile(0.25)
        # q3 = df[columnname].quantile(0.75)
        q1 = df.quantile(0.25)
        q3 = df.quantile(0.75)

        iqr = q3 - q1

        lower_bound = q1 - (1.5 * iqr)
        upper_bound = q3 + (1.5 * iqr)

        print(lower_bound)
        print(upper_bound)
        # df_no_outlier = iqr[(iqr.col_vals>lower_bound)&(iqr.col_vals<upper_bound)]
        # df.loc[df[columnname] < lower_bound, columnname] = lower_bound
        # df.loc[df[columnname] > upper_bound, columnname] = upper_bound
        for col in columnvalues:
            for i in range(0, len(df[col])):
                if df[col][i] < lower_bound[col]:
                    df[col][i] = lower_bound[col]

                if df[col][i] > upper_bound[col]:
                    df[col][i] = upper_bound[col]

        for col in columnvalues:
            df1[col] = df[col]
        print("-------------------", df1)
        df3 = spark.createDataFrame(df1)
        df3.show()
        outputDeltaTablepath = path + "/" + outputDeltaTable
        df3.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully completed IQR capping for selected column",
                "data": {"inputsteps": Data.count(), "outputsteps": df3.count()}}
        return json.dumps(list)
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

