from Pattern import tfidf_dedupe
import pyspark
import pandas as pd
import json

#import sparse_dot_topn.sparse_dot_topn as ct
from pyspark.sql.functions import col,isnan, when

from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def columnDetailsdef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId, columnName):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId
    InputTable = path + "/" + inputDeltaTable
    print("Reading Universal data")
    try:
        Data = spark.read.format("delta").load(InputTable)
        columnNames = Data.columns
        print("Column names in delta table: ", columnNames)
        ind = columnNames.index(columnName)
        print("Index of column ", columnName, " is :", ind)
        from pyspark.sql.functions import count
        try:
            invalidlist = Data.select(
                [count(when(col(c).isNull() | isnan(c), c)).alias(c) for c in Data.columns]).collect()
            emptylist = Data.select([count(when((col(c) == ''), c)).alias(c) for c in Data.columns]).collect()
            countRecords = Data.select(col(columnName)).count()
            Invalid = invalidlist[0][ind]
            Valid = countRecords - Invalid
            valid_percentage = (Valid / countRecords) * 100
            invalid_perentage = (Invalid / countRecords) * 100
            empty = emptylist[0][ind]
            empty_percentage = (empty / countRecords) * 100

            # DQList = [{"Total Rows": countRecords, "Valid Values": Valid, "InValid Values": Invalid, "Empty": empty}]
            DQList = {"Data Quality": [{"Total Rows": {"value": countRecords, "percentage": 100},
                                        "Valid Values": {"value": Valid, "percentage": valid_percentage},
                                        "InValid Values": {"value": Invalid, "percentage": invalid_perentage},
                                        "Empty": {"value": empty, "percentage": empty_percentage}}]}

            DQListJSON = json.dumps(DQList)
            print("Data Quality list:", DQListJSON)

            #######Unique values and Stats#############
            distinct_ids = [x[columnName] for x in Data.select(columnName).distinct().collect()]
            # print(distinct_ids)
            UVList = {"unique Values": []}
            for i in distinct_ids:
                countdistint = Data.select().where((Data[columnName] == i)).count()
                Unique_Value = {i: countdistint}
                UVList["unique Values"].append(Unique_Value)
            # print("Unique Value List", UVList)
            UVListJSON = json.dumps(UVList)
            print("Unique Value List:", UVListJSON)
            # distinct_column_val = Data.select(columnName).distinct().collect()
            # distinct_column_vals = [v[columnName] for v in distinct_column_val]
            # print("unique values",distinct_column_vals)
            # UVList = []
            # for i in distinct_column_vals:
            #     #unique = dict(zip(i, [Data.select().where((Data[columnName] == i)).count()]))
            #     #print(unique)
            #     UVList.append(unique)
            # print("unique values list: ", UVList)
            # Statistics ={"Statistics info":[]}
            dictu = {}
            # max
            df = Data.dropna(subset=[columnName])
            rowvalue = df.agg({columnName: "max"}).collect()[0]
            maxvalue = rowvalue[f"max({columnName})"]
            # dictu["Max"] = maxvalue
            # min
            rowminvalue = df.agg({columnName: "min"}).collect()[0]
            minvalue = rowminvalue[f"min({columnName})"]
            # dictu["Min"] = minvalue
            distinct1 = Data.select(col(columnName)).distinct().count()
            # dictu["Distinct"] = distinct1
            # Get All column names and it's types
            columnsdtypeint = []
            columnsdtypeother = []
            for y in Data.dtypes:
                if y[1] == 'int' or y[1] == 'double':
                    columnsdtypeint.append(y[0])
                else:
                    columnsdtypeother.append(y[0])
            if columnName in columnsdtypeint:
                # mean & standard diviation
                uData_stats = Data.select(_mean(col(columnName)).alias('mean'),
                                          _stddev(col(columnName)).alias('std')).collect()
                mean = uData_stats[0]['mean']
                std = uData_stats[0]['std']
                # dictu["Mean"] = mean
                # dictu["Standard Deviation"] = std
                # median
                from pyspark.sql.types import IntegerType
                data_df = Data.withColumn(columnName, Data[columnName].cast(IntegerType()))
                median = data_df.approxQuantile(columnName, [0.5], 0.25)
                # dictu["Median"] = median[0]
                # dictu["Mode"] = "N/A"
                Statistics = [
                    {"Min": minvalue, "Max": maxvalue, "Mean": mean, "Median": median[0], "Standard Deviation": std,
                     "Mode": "N/A", "Distinct": distinct1}]
            else:
                # dictu["Mean"] = "N/A"
                # dictu["Standard Deviation"] = "N/A"
                # median
                # dictu["Median"] = "N/A"
                mode = Data.groupby(columnName).count().orderBy("count", ascending=False).first()[0]
                dictu["Mode"] = mode
                Statistics = {"Statistics": [
                    {"Min": minvalue, "Max": maxvalue, "Mean": "N/A", "Median": "N/A", "Standard Deviation": "N/A",
                     "Mode": mode, "Distinct": distinct1}]}
            # Statistics["Statistics info"].append(dictu)

            StatisticsJSON = json.dumps(Statistics)
            print("Statistics: ", StatisticsJSON)
            ############Distribution###########################
            distinctvalues = Data.select(col(columnName)).distinct().count()
            Cardianlity = (distinctvalues / countRecords) * 100
            if Cardianlity <= 80:
                df = Data.groupBy(columnName).count()
                dfpandas = df.toPandas()
                sorted_df = dfpandas.sort_values(["count"], ascending=False)
                result = sorted_df.to_json(orient="index")
                parsed = json.loads(result)
                Disjsonobject = json.dumps(parsed, indent=4)
                Disjson = {"Distribution": [Disjsonobject]}
                Disjson = json.dumps(Disjson)
                print("Distribution:", Disjson)
            else:
                if columnName in columnsdtypeint:
                    df = Data.toPandas()
                    df['quantile1'] = pd.qcut(df[columnName], q=10, precision=0)
                    quartile = df['quantile1'].value_counts()
                    Disjson = quartile.to_json()
                    # quartiledf = quartile.DataFrame({'List': quartile.index, 'Count': quartile.values})
                    # result = quartiledf.to_json(orient="values")
                    # parsed = json.loads(result)
                    # Disjsonobject = json.dumps(parsed, indent=4)
                    # Disjson = {"Distribution": Disjsonobject}
                    # Disjson = json.dumps(Disjson)
                    # top10List = sorted_df.to_json(orient = "split")
                    print("Distribution:", Disjson)
                else:
                    Disjson = {"Distribution": "N/A"}
                    Disjson = json.dumps(Disjson)
                    print("Distribution:", Disjson)

            #pattern
            patternJson = tfidf_dedupe(InputTable, columnName)
            print(patternJson)

            final_list = [DQListJSON, UVListJSON, Disjson, StatisticsJSON, patternJson]
            print(final_list)
            json_object = json.dumps(final_list, indent=4, separators=(',', ':'))
            #lreturn fina_listJson  # [DQListJSON, UVListJSON, Disjsonobject, StatisticsJSON]
            list = {"status": "Success", "message": "Successfully completed column details request",
                    "data": json_object}
            return json.dumps(list)
        # except OSError as err:
        #     print("OS error: {0}".format(err))
        except:
            FailureMessage = {"status": "Failure",
                              "message": "Please check the column selected, Cannot retrieve column details if column doesnt exists or contains all null values."}
            FailureMessageJson = json.dumps(FailureMessage)
            print(FailureMessageJson)
            return FailureMessageJson
    # except OSError as err:
    #     print("OS error: {0}".format(err))
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Delta Table entered doesnt exists, Please enter a valid Delta Table!"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return FailureMessageJson


#columnDetailsdef("1","newPSNAN","s01","3", "4","1", "NAME")