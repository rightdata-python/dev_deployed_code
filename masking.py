import pandas as pd
import numpy as np
#from random import sample
#from sklearn.model_selection import StratifiedShuffleSplit
from pyspark.sql.functions import lit, rand, when
import pyspark
import json
from properties import path

from pyspark.sql.functions import lit, col,isnan, when, count, mean as _mean, stddev as _stddev
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")


def maskingreq(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnname,condition,value,valuetoreplace,start,finish):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    InputTable = path + "/" + inputDeltaTable
    print("Reading Delta Table")
    try:
        Data = spark.read.format("delta").load(InputTable)
        print("column names",Data.columns)
        column_dataset1 = Data.toPandas()
        # column_dataset1 = pd.read_csv(filename)
        if condition == "None":
            column_dataset1[columnname] = ['X' * len(i) for i in column_dataset1[columnname].astype(str)]
            print(column_dataset1)
        elif condition == "CustomMask":
            # mask entire row
            column_dataset1[columnname] = column_dataset1[columnname].astype(str).str[:0] + valuetoreplace
            print(column_dataset1)
            
        elif valuetoreplace == "None":
            # mask entire row
            column_dataset1[columnname] = ['X' * len(i) for i in column_dataset1[columnname].astype(str)]
            print(column_dataset1)

            # else:
            #    column_dataset1[columnname] = column_dataset1[columnname].astype(str).str[:-value] + maskvalue
            #    print(column_dataset1)
        elif condition == "FullMask":
            column_dataset1.loc[column_dataset1[columnname].astype(str).str.contains(valuetoreplace), columnname] = "***********"
            print(column_dataset1)
            
        elif condition == "LastFourDigits":
            valueto = value + "$"
            column_dataset1[columnname] = column_dataset1[columnname].astype(str).str.replace(valueto, valuetoreplace,regex=True) #end
            print(column_dataset1)
            
        elif condition == "FirstTwoDigits":
            valueto = "^" + value
            column_dataset1[columnname] = column_dataset1[columnname].astype(str).str.replace(valueto, valuetoreplace,regex=True)  # start
            print(column_dataset1)
            
        elif condition == "Inbetween":
            valueto = value
            column_dataset1[columnname] = column_dataset1[columnname].astype(str).str.replace(valueto, valuetoreplace,regex=True)  # middle
            print(column_dataset1)
            
        elif condition == "last":
            # masking last
            column_dataset1[columnname] = column_dataset1[columnname].str[:-4] + 'XXXX'
            print(column_dataset1)
            #return json.dumps(list)

        elif condition == "first":
            # masking first
            print("*******************came to first******************")
            column_dataset1[columnname] = 'XXX' + column_dataset1[columnname].str[3:]
            print(column_dataset1)
            #return json.dumps(list)

        elif condition == "position":
            print("came to position")
            #add = 'XXXXXX'
            column_dataset1[columnname] = column_dataset1[columnname].apply(lambda x: x[:start] + valuetoreplace + x[finish:])
            print(column_dataset1)
            #return json.dumps(list)


        df = spark.createDataFrame(column_dataset1)
        outputDeltaTablepath = path + "/" + outputDeltaTable
        df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully masked the values in selected column",
                "data": {"inputsteps": Data.count(), "outputsteps": df.count()}}
        return json.dumps(list)
    except OSError as err:
         print("OS error: {0}".format(err))
    #except:
     #   FailureMessage = {"status": "Failure",
     #                     "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
      #  FailureMessageJson = json.dumps(FailureMessage)
       # print(FailureMessageJson)
        #return json.dumps(FailureMessage)
