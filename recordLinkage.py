#importing the required libraries and packages
import recordlinkage as rl
import re as re
import pandas
import pyspark
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DevDeployedCode"
import json
from deltalake import DeltaTable
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

#defining coltomove function to show the input columns as first columns in output
def coltomove(df, cols_move_list=[], ref_col=''):
    cols = df.columns.tolist()
    a1 = cols[:list(cols).index(ref_col) + 1]
    a2 = cols_move_list

    a1 = [i for i in a1 if i not in a2]
    a3 = [i for i in cols if i not in a1 + a2]

    return (df[a1 + a2 + a3])

#defining fuzzymatcher fuction to take inputs, perform the actions and gives the output
def recordLinkage_Def(projectId, reciepeId, sessionId, leftStepID, leftResultID, rightStepID,rightResultID, currentStepId, consistentColumn, columnMapping):
    if sessionId == None:
        leftDeltaTable = "R_" + str( projectId) + "_" + reciepeId + "_" + leftStepID + "_" + leftResultID
        rightDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + rightStepID + "_" + rightResultID
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        leftDeltaTable = "R_" + str( projectId) + "_" + reciepeId + "_" + sessionId + "_" + leftStepID + "_" + leftResultID
        rightDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + rightStepID + "_" + rightResultID
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    leftDeltaTablepath = path + "/" + leftDeltaTable
    rightDeltaTablepath = path + "/" + rightDeltaTable
    try:
        print("Reading Data")
        #Read Driver dataset or left dataset as dataframe
        df_left = DeltaTable(leftDeltaTablepath).to_pandas()
        df_left.index.name = 'Index_df_left'
        df_left = df_left.astype(str)
        print("No of Rows, Columns and Column names in DriverDataset: ", len(df_left), ";",len(df_left.columns), ";", df_left.columns)
        #Read Reference dataset or right dataset as dataframe
        df_right = DeltaTable(rightDeltaTablepath).to_pandas()
        df_right.index.name = 'Index_df_right'
        df_right = df_right.astype(str)
        print("No of Rows, Columns and Column names in ReferenceData: ", len(df_right),";",len(df_right.columns), ";", df_right.columns)
        # consistent column names to df_left_column1 and df_right_column1 from driver and reference datasets respectively
        df_left_column1 = consistentColumn["driverColumn"]
        df_right_column1 = consistentColumn["referenceColumn"]
        # assigning record linkage index to indexer
        indexer = rl.Index()
        # generating all the possible pairs with full()
        # indexer.full()
        # Block on State type because it has consistent values across datasets
        # if our datasets are large, generating all the possible pairs will be very computationally expensive. To avoid generating all the possible pairs, we should choose one column which has consistent values from both datasets
        indexer.block(left_on=df_left_column1, right_on=df_right_column1)
        # Create candidate pairs
        pairs = indexer.index(df_left, df_right)
        # Create a comparing object
        compare = rl.Compare()

        drop_list = []
        for i, x in enumerate(columnMapping):
            # print(str(x['driverColumn']), str(x['referenceColumn']))
            df_left[x['driverColumn']] = df_left[x['driverColumn']].astype(str)
            df_right[x['referenceColumn']] = df_right[x['referenceColumn']].astype(str)
            thres = float(x['threshold'])
            compare.string(x['driverColumn'], x['referenceColumn'], threshold=thres, method='levenshtein',
                           label=f'columnNames{i}')
            drop_list.append(f'columnNames{i}')
            value1 = i
        # Extracting the columNnames of driver dataset which came as inputs to drivercolumnnames list
        drivercolumnnames = []
        for x in columnMapping:
            drivercolumnnames.append(x['driverColumn'])
        # Extracting the columnNames of refernce dataset which came as inputs to drivercolumnnames list
        referencecolumnnames = []
        for y in columnMapping:
            referencecolumnnames.append(y['referenceColumn'])

        # generating pairs by comparing the columns
        features = compare.compute(pairs, df_left, df_right)
        # creating score column to show the count of matches for given columns by row
        features['Score'] = features.loc[:, ].sum(axis=1)
        # sorting the column sort by value and selecting only the columns which has all the columns matches(value1)
        df_matches = features[features.Score >= value1 + 1].reset_index()
        # inital merge with potential matches
        initial_merge = df_matches.merge(df_left, left_on='Index_df_left', right_on='Index_df_left', how='left')
        # Final merge of driver and reference dataset
        final_merge = initial_merge.merge(df_right, left_on='Index_df_right', right_on='Index_df_right', how='left')
        # droping the columns which were created in the flow
        final_merge = final_merge.drop(columns=drop_list)
        # droping the columns which were created in the flow
        final_merge = final_merge.drop(columns=['Index_df_left', 'Index_df_right'])
        # combining all the input column names into Mathcedcolumns list to move these columns to front of the dataset. For visual appeal
        MatchedColumns = []
        if list(drivercolumnnames) == list(referencecolumnnames):
            for j in columnMapping:
                append_dri_columnnames = '_x'
                append_ref_columnnames = '_y'
                MatchedColumns.extend(
                    [j['driverColumn'] + append_dri_columnnames, j['referenceColumn'] + append_ref_columnnames])
        else:
            for k in columnMapping:
                MatchedColumns.extend([k['driverColumn'], k['referenceColumn']])
        # move columns which came as input to first of the dataset
        final_merge = coltomove(final_merge, cols_move_list=MatchedColumns, ref_col='Score')
        # sorting dataset by consistent column values of driver dataset
        final_merge = final_merge.sort_values(by=df_left_column1, ascending=False)
        # sorting the dataset by score
        final_merge = final_merge.sort_values(by=['Score'], ascending=False)
        print(final_merge.head())
        #final_merge = final_merge.set_index('Score')
        print("No of Rows, Columns and Column names in Record Linkage Join Data: ", len(final_merge), ";",
              len(final_merge.columns), ";", final_merge.columns)
        # Threshold from input if none all the dataset is saved in the output location else only the data above the threshold of best_match_score is saved in the output location.
        df1 = spark.createDataFrame(final_merge)
        df1.show()
        outputDeltaTablepath = path + "/" + outputDeltaTable
        df1.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        lists = {"status": "Success", "message": "Record Linkage is completed",
                "data": {"inputsteps": len(final_merge), "outputsteps": df1.count()}}
        return json.dumps(lists)

    # except OSError as err:
    #     print("OS error: {0}".format(err))
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Some thing went wrong please check the inputs"}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

#recordLinkage_Def("1", "FL", None, "3", "1", "234","1", "456", {"driverColumn": "FacilityName","referenceColumn": "ProviderName"}, [{"driverColumn": "City", "referenceColumn": "ProviderCity","threshold": "0.70"},{"driverColumn": "Address","referenceColumn": "ProviderAddress","threshold": "0.70"}])

