import asyncio
from fastapi import FastAPI, Depends
from starlette.websockets import WebSocket
#importing required files and packages
import uvicorn
from typing import List, Optional
from pydantic import BaseModel
from columnCategoryCountPaginationRequest_1 import columnCategoryCountPaginationdef

ws_clients = []
app = FastAPI()

def get_ws_clients():
    return ws_clients

@app.get("/update")
async def notify(clients=Depends(get_ws_clients)):
    """
    Notify all websocket clients

    """
    await asyncio.wait([ws.send_text("process started") for ws in clients])
    await asyncio.wait([ws.send_text("process ended!") for ws in clients])
    return {}


@app.websocket("/notify")
async def websocket_endpoint(websocket: WebSocket, clients=Depends(get_ws_clients)):
    await websocket.accept()
    clients.append(websocket)
    try:
        while True:
            _ = await websocket.receive_text()
    finally:
        ws_clients.remove(websocket)


# class columnCategoryCountPaginationRequest(BaseModel):
#     # dataset location
#     projectId: str
#     reciepeId: str
#     sessionId: Optional[str]
#     inputStepId: str
#     currentStepId: Optional[str]
#     inputResultId: str
#
# # def get_ws_clients():
# #     return ws_clients
#
# @app.post("/ColumnCategoriesCountPaginationRequestAPI2")
# async def ColumnCategoriesCountPaginationRequestAPI2(ccp:columnCategoryCountPaginationRequest,clients=Depends(get_ws_clients)):
#     """
#     Notify all websocket clients
#     """
#     #ws = create_connection(" http://18.144.30.185:8085/server-endpoint")
#     await asyncio.wait([ws.send_text("process started") for ws in clients])
#     Result = columnCategoryCountPaginationdef(ccp.projectId, ccp.reciepeId, ccp.sessionId, ccp.inputStepId, ccp.currentStepId, ccp.inputResultId)
#     await asyncio.wait([ws.send_text("process ended!") for ws in clients])
#     return {f"{Result}"}
#
#
# @app.websocket("/notify")
# async def websocket_endpoint(websocket: WebSocket, clients=Depends(get_ws_clients)):
#     await websocket.accept()
#     clients.append(websocket)
#     try:
#         while True:
#             _ = await websocket.receive_text()
#     finally:
#         ws_clients.remove(websocket)


