import asyncio
from fastapi import FastAPI, Depends
from starlette.websockets import WebSocket
#importing required files and packages
import uvicorn
from typing import List, Optional
from pydantic import BaseModel
from samplingDTRequest import samplingDTRequestdef
#from columnCategoryCountPaginationRequest import columnCategoryCountPaginationdef

ws_clients = []
app = FastAPI()

def get_ws_clients():
    return ws_clients

class SamplingRequest(BaseModel):
    projectId: int
    reciepeId: str
    sessionId: Optional[str]
    inputStepId: str
    currentStepId: str
    inputResultId: str
    samplingMethod: str
    sampleSize: int
@app.post("/SamplingDTRequestAPI")
#calling the functions with given inputs
async def SamplingRequestAPI(sr: SamplingRequest, clients=Depends(get_ws_clients)):
    await asyncio.wait([ws.send_text("process started") for ws in clients])
    Result = samplingDTRequestdef(sr.projectId,sr.reciepeId,sr.sessionId,sr.inputStepId, sr.currentStepId,sr.inputResultId, sr.samplingMethod, sr.sampleSize)
    await asyncio.wait([ws.send_text("process ended!") for ws in clients])
    return {f"{Result}"}


@app.websocket("/notify")
async def websocket_endpoint(websocket: WebSocket, clients=Depends(get_ws_clients)):
    await websocket.accept()
    clients.append(websocket)
    try:
        while True:
            _ = await websocket.receive_text()
    finally:
        ws_clients.remove(websocket)
