import json
import pandas as pd
import pyspark
from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
from sklearn.model_selection import StratifiedShuffleSplit
from pyspark.sql.functions import lit, rand, when
from deltalake import DeltaTable
#from properties import path
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
def samplingValidationpandas(projectId,reciepeId,sessionId, universeInputStepId,  sampleInputStepId,  currentStepId, universeInputResultId,  sampleInputResultId):
        print("sampling validatiaon started")
        if sessionId == None:
            UniverseDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + universeInputStepId + "_" + universeInputResultId
            SampleDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + sampleInputStepId + "_" + sampleInputResultId
        else:
            UniverseDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+ universeInputStepId + "_" + universeInputResultId
            SampleDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + sampleInputStepId + "_" + sampleInputResultId
        InputTableUniverse = path +"/" + UniverseDeltaTable
        InputTableSample = path + "/" + SampleDeltaTable
        print("Reading Universal data")
        uData = DeltaTable(InputTableUniverse).to_pandas()
        print("Reading Sample data")
        sData = DeltaTable(InputTableSample).to_pandas()
        print(uData.head())
        print(sData.head())
        columnNames = uData.columns.tolist()
        #print(columnNames)
        # Get All column names and it's types
        columnsdtypeint = []
        columnsdtypeother = []
        #print(uData.dtypes)
        for y in columnNames:
            #print(y)
            if uData[y].dtype == 'int32' or  uData[y].dtype == 'int' or uData[y].dtype == 'int64' or uData[y].dtype == 'float' or uData[y].dtype == 'float32' or uData[y].dtype == 'float64' or uData[y].dtype == 'double':
                 columnsdtypeint.append(y)
            else:
                 columnsdtypeother.append(y)

        Dict_universe = uData.apply(lambda x: sv_universe(x, x.name))
        Dict_sample = sData.apply(lambda x: sv_sample(x, x.name))
        FinalDict_universe = [dic for dic in Dict_universe]
        print(type(FinalDict_universe))
        #print(FinalDict_universe)
        FinalDict_sample = [dic for dic in Dict_sample]
        print("FinalDict_universe: ",FinalDict_universe)
        print("FinalDict_sample:",FinalDict_sample)
        combined_list = [{key: FinalDict_sample[index][key] | FinalDict_universe[index][key] for key in
                          FinalDict_sample[index].keys()} for index in range(len(FinalDict_sample))]
        print(combined_list)
        #Dict_consistency = lambda x: Consitency(x))
        #print(Dict_consistency)
        # json_object = json.dumps(combined_list, default=np_encoder)
        # print(json_object)
        # for i in combined_list:
        #     print(combined_list[i][3])
        # print(type(combined_list))
        #print(combined_list[0]['Mean'])

def Consitency(combined_list):
            consistency = {}
            mean1 = combined_list['Mean']['Sample']
            uniqueNumber = combined_list['No. of Distinct Values']['Universal']
            mean = combined_list['Mean']['Universal']
            std1 = combined_list['Standard Deviation']['Sample']
            std = combined_list['Standard Deviation']['Universal']
            median1 = combined_list['Median']['Sample']
            median = combined_list['Median']['Universal']
            mode1 = combined_list['Mode']['Sample']
            mode = combined_list['Mode']['Universal']
            if mean1 == "N/A":
                if uniqueNumber == 1:
                     consistency["Consistency"]  = 100
                else:
                    if mode == mode1:
                        Consistency = 90

                    else:
                        Consistency = 50
                    consistency["Consistency"] = Consistency
            else:
                # mean
                if mean == 0:
                    mean_avg = 0
                elif mean1 > mean:
                    mean_avg = (mean * 100) / mean1
                else:
                    mean_avg = (mean1 * 100) / mean
                # Median
                if median == 0:
                    median_avg = 0
                elif median1 > median:
                    median_avg = (median * 100) / median1
                else:
                    median_avg = (median1 * 100) / median
                # print("median average: ", median_avg)
                # standard deviation
                if std == 0:
                    sd_avg = 0
                elif std1 > std:
                    sd_avg = (std * 100) / std1
                else:
                    sd_avg = (std1 * 100) / std
                # print("Standard deviation average: ", sd_avg)
                Consistency = (mean_avg + median_avg + sd_avg) / 3
                consistency["Consistency"] = Consistency

            return consistency





        #for i in range(0, len(Dict_universe)):
        #combined_list = [{key: dic[key] | Dict_universe[][key] for key in dic.keys()} for dic in Dict_sample]
        #print(combined_list)
        # concatseries = pd.concat([Dict_universe, Dict_sample],axis=1)
        # print(concatseries)
        # print(type(concatseries))
        #FinalDict_universe = [dic for dic in Dict_universe]
        # print(FinalDict_universe)
        #FinalDict_sample = [dic for dic in Dict_sample]
        # combined_list = [{key: dic[key] | FinalDict_universe[0][key] for key in dic.keys()} for dic in FinalDict_sample]
        # print(combined_list)
        # # print(FinalDict_universe)
        # # print(Convert(FinalDict_universe))
        # # print(FinalDict_universe)
        # print(type(FinalDict_universe))
        # #combined_list = [{key: dic[key] | FinalDict_universe[0][key] for key in dic.keys()} for dic in FinalDict_sample]
        # combined_dict = {key: FinalDict_sample[key] | FinalDict_universe[key] for key in FinalDict_sample.keys()}
        # print(combined_dict)
        # # # FinalDict.loc[columnNames]
        # FinalDict_universe = [dic for dic in Dict_universe]
        # FinalDict_sample = [dic for dic in Dict_sample]
        # print(FinalDict_universe)
        # #df1 = pd.DataFrame(FinalDict_universe).set_index('id')
        # #print(type(FinalDict_universe))
        # print(FinalDict_sample)

        #df2 = pd.DataFrame(FinalDict_sample).set_index('id')
        #df = df1.merge(df2, left_index=True, right_index=True)
        #Finallist = df.T.to_dict()
        #print(Finallist)
        #from collections import defaultdict
        # d = defaultdict(dict)
        # for l in (FinalDict_universe, FinalDict_sample):
        #     for elem in l:
        #         d[elem['No. of values']].update(elem)
        #         d[elem['No. of Distinct Values']].update(elem)
        #         d[elem['No. of Finite Values']].update(elem)
        #         d[elem['Mean']].update(elem)
        # l3 = d.values()
        # print(l3)
        # dd = defaultdict(list)
        # for d in (FinalDict_universe, FinalDict_sample):  # you can list as many input dicts as you want here
        #     for key, value in d.items():
        #         dd[key].append(value)
        #
        # print(dd)
#
def sv_universe(uData, column):
    if uData.dtype == 'int32' or uData.dtype == 'int' or uData.dtype == 'int64' or uData.dtype == 'float' or uData.dtype == 'float32' or uData.dtype == 'float64' or uData.dtype == 'double':
        dictU = {}
        dictU["column_name"] = {"Universal": column}
        dictU["No. of values"] = {"Universal": len(uData)}
        # distinct count
        dictU["No. of Distinct Values"] = {"Universal": uData.nunique()}
                    # {"Sample": sData[column].value_counts(),
                    #                                "Universal": uData[column].value_counts()}
        # invalid data count
        nan = len(uData) - uData.isnull().sum()
        dictU["No. of Finite Values"] = {"Universal": nan}
        # mean & standard diviation
        mean = uData.mean()
        dictU["Mean"] = {"Universal": mean}
        std = uData.std()
        dictU["Standard Deviation"] = {"Universal": std}
        # median
        median = uData.median()
        dictU["Median"] = {"Universal": median}
        dictU["Mode"] = {"Universal": uData.mode()[0]}  # dataframe.mode()['Column'][0]
        # Mean
        # if mean == 0:
        #     mean_avg = 0
        # elif mean1 > mean:
        #     mean_avg = (mean * 100) / mean1
        # else:
        #     mean_avg = (mean1 * 100) / mean
        # # print("mean average: ", mean_avg)
        # # Median
        # if median == 0:
        #     median_avg = 0
        # elif median1 > median:
        #     median_avg = (median * 100) / median1
        # else:
        #     median_avg = (median1 * 100) / median
        # # print("median average: ", median_avg)
        # # standard deviation
        # if std == 0:
        #     sd_avg = 0
        # elif std1 > std:
        #     sd_avg = (std * 100) / std1
        # else:
        #     sd_avg = (std1 * 100) / std
        # # print("Standard deviation average: ", sd_avg)
        # Consistency = (mean_avg + median_avg + sd_avg) / 3
        # dictU["Consistency"] = Consistency
        # max
        dictU["Max"] = {"Universal": uData.max()}
        # min
        dictU["Min"] = {"Universal": uData.min()}

        print("Rowdetails for column ", column, " is :", dictU)
        return dictU
    else:
        dictU = {}
        dictU["column_name"] = {"Universal": column}
        # count of records
        dictU["No. of values"] = {"Universal": len(uData)}
        # distinct count
        dictU["No. of Distinct Values"] = {"Universal": uData.nunique()}
        # invalid data count
        nan = len(uData) - uData.isnull().sum()
        dictU["No. of Finite Values"] = {"Universal": nan}
        # mean & standard diviation
        dictU["Mean"] = {"Universal": "N/A"}
        dictU["Standard Deviation"] = {"Universal": "N/A"}
        # median
        dictU["Median"] = {"Universal": "N/A"}
        mode = uData.mode()[0]
        dictU["Mode"] = {"Universal": mode}
        # if uData.nunique() == 1:
        #     dictU["Consistency"] = 100
        # else:
        #     if mode == mode1:
        #         Consistency = 90
        #     else:
        #         Consistency = 50
        #     dictU["Consistency"] = Consistency
            # max
            # t_array = uData[column].to_numpy()
            #
            # max = t_array.max()
            # max1 = sData[column].max()
        dictU["Max"] = {"Universal": "N/A"}  # , "Universal": max}
            # min
        dictU["Min"] = {"Universal": "N/A"}
            # "Sample": sData[column].min(), "Universal": uData[column].min()}

        print("Rowdetails for column ", column, " is :", dictU)
        return dictU

            #dictF["RowValues"].append(dictU)

        # print(dictF)
        # # Serializing json
        # json_object = json.dumps(dictF, default=np_encoder)#indent=4)
        # list = {"status": "Success", "message": "Successfully Completed Sample Validation Request",
        #         "data": json_object}
        # return json.dumps(list)

def sv_sample(uData, column):
    if uData.dtype == 'int32' or uData.dtype == 'int' or uData.dtype == 'int64' or uData.dtype == 'float' or uData.dtype == 'float32' or uData.dtype == 'float64' or uData.dtype == 'double':
        dictU = {}
        dictU["column_name"] = {"Sample": column}
        dictU["No. of values"] = {"Sample": len(uData)}
        # distinct count
        dictU["No. of Distinct Values"] = {"Sample": uData.nunique()}
        nan = len(uData) - uData.isnull().sum()
        dictU["No. of Finite Values"] = {"Sample": nan}
        # mean & standard diviation
        mean = uData.mean()
        dictU["Mean"] = {"Sample": mean}
        std = uData.std()
        dictU["Standard Deviation"] = {"Sample": std}
        # median
        median = uData.median()
        dictU["Median"] = {"Sample": median}
        dictU["Mode"] = {"Sample": uData.mode()[0]}  # dataframe.mode()['Column'][0]
        dictU["Max"] = {"Sample": uData.max()}
        # min
        dictU["Min"] = {"Sample": uData.min()}

        print("Rowdetails for column ", column, " is :", dictU)
        return dictU
    else:
        dictU = {}
        dictU["column_name"] = {"Sample": column}
        # count of records
        dictU["No. of values"] = {"Sample": len(uData)}
        # distinct count
        dictU["No. of Distinct Values"] = {"Sample": uData.nunique()}
        # invalid data count
        nan = len(uData) - uData.isnull().sum()
        dictU["No. of Finite Values"] = {"Sample": nan}
        # mean & standard diviation
        dictU["Mean"] = {"Sample": "N/A"}
        dictU["Standard Deviation"] = {"Sample": "N/A"}
        # median
        dictU["Median"] = {"Sample": "N/A"}
        mode = uData.mode()[0]
        dictU["Mode"] = {"Sample": mode}
        dictU["Max"] = {"Sample": "N/A"}  # , "Universal": max}
            # min
        dictU["Min"] = {"Sample": "N/A"}
            # "Sample": sData[column].min(), "Universal": uData[column].min()}

        #print("Rowdetails for column ", column, " is :", dictU)
        return dictU

import numpy as np
def np_encoder(object):
    if isinstance(object, np.generic):
        return object.item()
class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)

samplingValidationpandas("1","newPSNAN","s01","3", "4","999","1","1")









