

#To perform sampling on selected delta table and return a delta table with selected sampling method and sample size
#Expected input: JSON
#Expected output: Delta Table

#import libraries and packages
from pyspark.sql.functions import lit, rand, when
import pyspark
import json
from pyspark.sql import SparkSession
from pyspark.context import SparkContext
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
#starting spark session
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

#defing sampling function to perform sampling
def samplingDTRequestdef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId, samplingMethod, sampleSize):
    """
    Inputs:
    projectID: project id of the data, expects int
    reciepeId: reciepe id of the data, expects str
    sessionId: sessionId of the data, expects str(optional)
    inputStepId: inputStepId of the data, expects str
    currentStepId: currentStepId for the new data, expects str
    inputResultId: inputResultId of the data, expects str
    samplingMethod: samplingMethod to be performed on data, expects str (takes only str as "firstRecords", "stratified", "random")
    sampleSize: sampleSize for the new data, expects int (should be less than universe data, taken care by UI)
    """
    # constructing the file name and defining path to extract delta table from path
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:",inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    InputTable = path +"/" + inputDeltaTable
    print("Input Table with path:", InputTable)
    print("Reading Delta Table")

    try:
        #reading delta table as spark df
        df = spark.read.format("delta").load(InputTable)
        df.show()
        #check if sample size is less then universe data
        if sampleSize > df.count():
            print("Samplesize entered is greater than the size of delta table")
        print("count of the records in Delta Table: ", df.count())
        print("Column names of the Delta Table: ", df.columns)
        #random sampling
        if samplingMethod == "random":
            print("started Random sampling")
            # converting samplesize to fraction
            frac = (sampleSize / df.count())
            # print(frac)
            countfrac = (round(frac, 6) + 0.02)
            if countfrac >= 1:
                countfrac1 = 1.00
            else:
                countfrac1 = countfrac
            print("Sample size in fraction: ", countfrac1)
            sampledata = df.sample(withReplacement=False, fraction=countfrac1, seed=2021)
        #first n records sampling
        elif samplingMethod == "firstRecords":
            print("selecting first records for sampling")
            sampledata = df.limit(sampleSize)
        #stratified sampling
        elif samplingMethod == "stratified":
            print("started stratified sampling")
            # creating a column with random values to apply stratified sampling on given delta table
            df = df.withColumn('samplecol', when(rand() > 0.5, 1).otherwise(0))
            frac = (sampleSize / df.count())
            print(frac)
            countfrac = (round(frac, 6) + 0.2)
            print(countfrac)
            fractions = df.select("samplecol").distinct().withColumn("fraction", lit(countfrac)).rdd.collectAsMap()
            print(fractions)
            sampledata1 = df.sampleBy('samplecol', fractions, seed=2021)  # sample(0.1,123)
            sampledata = sampledata1.drop(sampledata1.samplecol)
        print("Sampling is done and saving the DeltaTable")
        #check if the sample data is same length as sample size
        if sampledata.count() > sampleSize:
            sampledataFinal = sampledata.limit(sampleSize)
            print("count of the records in Sample Delta Table: ", sampledataFinal.count())
            print("Column names of the Sample Delta Table: ", sampledataFinal.columns)
        else:
            sampledataFinal = sampledata
            print("count of the records in Sample Delta Table: ", sampledataFinal.count())
            print("Column names of the Sample Delta Table: ", sampledataFinal.columns)
        outputDeltaTablepath = path + "/" + outputDeltaTable
        #sampledataFinal.write.format("delta").save(outputDeltaTablepath)
        #save as delta table and send success message for the request
        sampledataFinal.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully completed " + samplingMethod + " sampling.",
                "data": {"inputsteps": df.count(), "outputsteps": sampledataFinal.count()}}
        return json.dumps(list)

    #except:
    except OSError as err:
        print("OS error: {0}".format(err))
        # FailureMessage = {"status": "Failure",
        #                   "message": "Something went wrong, Please check the input parameters!"}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        # FailureMessageJson = json.dumps(FailureMessage)
        # print(FailureMessageJson)
        # return json.dumps(FailureMessage)

samplingDTRequestdef(1,"PublicSchoolNan","ss01","1", "115","2", "random", 500)
