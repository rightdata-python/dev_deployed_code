import pyspark
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
from deltalake import DeltaTable
import json
import numpy as np

spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
    .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
    .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE") \
    .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
    .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
    .config("spark.databricks.delta.optimizeWrite.enabled", "true") \
    .config("spark.databricks.delta.retentionDurationCheck.enabled", "false") \
    .enableHiveSupport() \
    .getOrCreate()
print("spark session Started")


def movecol(df, cols_to_move=[], ref_col=''):
    cols = df.columns.tolist()
    seg1 = cols[:list(cols).index(ref_col) + 1]
    seg2 = cols_to_move
    seg1 = [i for i in seg1 if i not in seg2]
    seg3 = [i for i in cols if i not in seg1 + seg2]
    return (df[seg1 + seg2 + seg3])

def splitColumndef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,split, columnName,byCondition,newColumnNames):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Reading Universal data")
    try:
        #Data = spark.read.format("delta").load(inputDeltaTablepath)
        df = DeltaTable(inputDeltaTablepath).to_pandas()
        print(df.head())
        #columnNamesdata = Data.columns
        print("No of Columns and Column names in input data: ", len(df.columns), ";", df.columns)
        print("len: ", len(df))
        if split == "delimiter":
            # delimiterslist = []
            # for x in byCondition:
            #     delimiterslist.append(x['delimiter'])
            #     print("delimter list:", delimiterslist)
            delimiter = byCondition["delimiter"]
            print("delimiter: ",delimiter)
            # if delimiter == ".":
            #     delimiter = "\."
            newcolumns = []
            for x in newColumnNames:
                newcolumns.append(x['column'])
                print("newcolumns list:", newcolumns)

            # df[[newcolumns[0], newcolumns[1]]] = df[columnName].astype(str).str.split(delimiter,n=1, expand=True)
            #
            # if len(newcolumns) > 2:
            #     newcolumnList = newcolumns[2:]
            #     for i in newcolumnList:
            #         df[i] = np.nan
            #newcolumns = [col1,col2,col3,col4]
            # new data frame with split value columns
            new = df[columnName].astype(str).str.split(" ", expand=True)
            #
            if len(newcolumns) > new.shape[1]:
                for index in range(len(newcolumns) - new.shape[1], len(newcolumns)):
                    new.loc[:, newcolumns[index]] = np.NaN
            df.loc[:, newcolumns] = new.iloc[:, :len(newcolumns)-1]



            # for i in range(len(newcolumns)):
            #      df[newcolumns[i]] = new[i]
            #     try:
            #         df[newcolumns[i]] = new[i]
            #     except:
            #         df[i] = np.nan
            #         df[i].replace('None', np.nan, inplace=True)

            df = movecol(df, cols_to_move=newcolumns, ref_col=columnName)
            # print("delimiter0:", delimiterslist[0])
            # # df1 = Data.topandas()
            # # df1[newcolumns] = df1[columnName].str.split(delimiterslist[0], expand=True)
            # # df = spark.createDataFrame(df1)
            # if delimiterslist[0] == ".":
            #     delimiterslist[0] = "\."
            # if len(delimiterslist) + 1 == len(newcolumns):
            #     if len(delimiterslist) == 1:
            #         df = Data.withColumn(newcolumns[0], split(col(columnName), delimiterslist[0]).getItem(0)) \
            #             .withColumn(newcolumns[1], split(col(columnName), delimiterslist[0]).getItem(1))
            #         print("Column names of new Delta table: ", df.columns)
            #         print("Column names of new Final Delta table: ", df.columns)
            #         df.show()
            #     elif len(delimiterslist) == 2:
            #         df = Data.withColumn(newcolumns[0], split(col(columnName), delimiterslist[0]).getItem(0)) \
            #             .withColumn("extracolumn", split(col(columnName), delimiterslist[0]).getItem(1))
            #         df = df.withColumn(newcolumns[1], split(col("extracolumn"), delimiterslist[1]).getItem(0)) \
            #             .withColumn(newcolumns[2], split(col("extracolumn"), delimiterslist[1]).getItem(1))
            #         df = df.drop("extracolumn")
            #         print("Column names of new Delta table: ", df.columns)
            # else:
            #     print(
            #         "Delimiters count should less than New column name, For example: for 2 delimiters should provide 3 new column names.")
        elif split == "position":

            # Extracting key-value of dictionary in variables
            #key, val = byCondition.items()[0]
            value = byCondition["startPosition"]
            value1 = byCondition["endPosition"]
            print("split by following position:",value,":",value1)


            column1 = columnName+str(1)
            column2 = columnName+str(2)
            print("new columns after split:",column1,",",column2)

            df[column1] = df[columnName].astype(str).str[:value]
            df[column2] = df[columnName].astype(str).str[value1:]
            df = movecol(df, cols_to_move=[column1, column2], ref_col=columnName)
        #elif split == "regularExpression":

        print(df.head())
        print("No of Columns and Column names in output data: ", len(df.columns), ";", df.columns)
        print("len: ", len(df))
        spark_df = spark.createDataFrame(df)

        outputDeltaTablepath = path + "/" + outputDeltaTable
        spark_df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully splited the selected column by given condition",
               "data": {"inputsteps": len(df), "outputsteps": spark_df.count()}}
        return json.dumps(list)
    # except OSError as err:
    #     print("OS error: {0}".format(err))

    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

#splitColumndef("1","newPSNAN","s01","3", "222","1", "delimiter","LATITUDE", {"delimiter":"."},[{"column":"column1"},{"column":"column2"}])
#splitColumndef("1","newPSNAN","s01","3", "222","1", "position","DISTRICTID", {"startPosition":2, "endPosition":5},[None])