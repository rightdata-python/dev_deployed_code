import pandas as pd
import math
import json
import numpy as np
import pandas as pd
import pyspark
from deltalake import DeltaTable
from properties import path
from data_quality_index import technical_quality_score
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
# spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
#      .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
#      .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
#      .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
#      .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
#      .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
#      .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
#      .enableHiveSupport() \
#      .getOrCreate()
# print("spark session Started")



def removeMismatchDatatype(projectId, reciepeId, sessionId, inputStepId, currentStepId, inputResultId, columnName, dataType):
    if sessionId == None:
        inputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    inputDeltaTablepath = path + "/" + inputDeltaTable
    outputDeltaTablepath = path + "/" + outputDeltaTable
    try:
        print("Reading data")
        data = DeltaTable(inputDeltaTablepath).to_pandas()
        print(data[columnName].dtypes)
        data[columnName].iloc[-1] = "hello"
        data = data.replace({'nan': np.NaN, 'NaN': np.NaN})

        print(data)
        if dataType == "IntegerType" or dataType == "ShortType" or dataType == "LongType" :
            dataType = "IntegerType"
        elif dataType == "FloatType" or dataType == "DoubleType":
            dataType = "FloatType"
        elif dataType == "BooleanType" or dataType == "BinaryType":
            dataType = "BooleanType"
        else:
            dataType = dataType

        datatypeDict = {columnName: dataType}
        df_mask = technical_quality_score(data[[columnName]], datatypeDict, method='mask')
        print(df_mask)
        print(type(df_mask))
        data = data[df_mask[columnName].fillna(True)]
        #df = data.loc[df_mask[columnName]]
        #print(data[columnName].dtype)
        print(data)
        # print(type(df_mask))
        # REMOVE
        #df = data.loc[df_mask[columnName]]
        # data[columnName] = data.loc[df_mask[columnName]]
        # print(data)
        #df = data.loc[df_mask[columnName] == True, :]
        #df1 = data.loc[df.values, :]
        # df.loc[:,"FT_TEACHER"]
        #print(df)
        # data.loc[~df_mask[columnName], columnName] = toReplace
        # data.loc[:, columnName]
        # print(data)

        # print("Output Table with path:", outputDeltaTablepath)
        # spark_df = spark.createDataFrame(df)
        # spark_df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        # print("saved in given location")
        # list = {"status": "Success", "message": "Successfully completed Remove Mismatch DataType",
        #     "data": {"inputsteps": len(data), "outputsteps": spark_df.count()}}
        #
        #
        # print(list)
        # return json.dumps(list)

    # except OSError as err:
    #           print("OS error: {0}".format(err))
    except OSError as err:
        print(err)
        print("OS error: {0}".format(err))

        FailureMessage = {"status": "Failure",
                  "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

removeMismatchDatatype("1","newPSNAN","s01","3", "444","1", "FT_TEACHER","IntegerType")
