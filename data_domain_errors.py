import numpy as np
import pandas as pd
import pycountry
import re, math


class DataDomainErrors():
    """
    A class for get the error in the data domain selected
    """

    def __init__(self,
                 link='(https?:\/\/(?:[-\w.]|(?:%[\da-fA-F]{2}))+)|(www\.(?:[-\w.]|(?:%[\da-fA-F]{2}))+)',
                 email='[\w\.]+@[\w\-]+\.[\w]+',
                 telephone=r"^(\+?\([0-9]{3}\) ?|[0-9]{3}-)[0-9]{3}-[0-9]{4}$",
                 address='(\d{1,5}\s[A-Z]+)|([A-Z]+\s\d{1,5})',
                 coordinates='^[+\-]?[0-9]{1,3}\.[0-9]{3,}$'
                 ):
        """
        When the model is instantiated create some useful variables.

        """
        # Create a variables that give the possible data domain
        self.possibles_data_domain = ['Gender', 'Email', 'Date', 'Time', 'Month', 'Day', 'Links', 'Telephone',
                                      'Country', 'Currency', 'Languages', 'Address', 'Coordinates',
                                      'Boolean', 'Boolean, string', 'Boolean, decimal', 'Boolean, integer',
                                      'Integer, quantity', 'Decimal, quantity',
                                      'Integer, secuential number',
                                      'Integer, categorical',
                                      'Integer, unique number', 'Decimal, unique number',
                                      'String, categorical columns', 'String, secuential number', 'String, unique',
                                      'String']
        self.link = link
        self.email = email
        self.telephone = telephone
        self.address = address
        self.coordinates = coordinates

    # STRING OR NUMBER BOOLEAN DETECTOR
    def binaries_values(self, serie, domain_pred):
        """
        Function to detect the percentage of invalid data given a data domain.
        Detect the errors in the folowing columns: ['Boolean string', 'Boolean, decimal', 'Boolean, integer', 'Gender']

        Parameters
        ----------
        serie: pandas serie
            Serie with the column to analyze.
        domain_pred: string
            The domain selected to analize

        Returns
        -------
        number_invalid_data: float
            Percentage of invalid data for this column.
        """

        # Define a simple function to check if value is boolean
        def is_bool(x):
            possible_bool = [True, False, 'True', 'False']
            if (x in possible_bool) & (pd.api.types.is_integer(x) != True) & (pd.api.types.is_float(x) != True):
                return True
            return False

        # Instance the final dict
        final_dict = {}
        # Keep the columns with only two values
        clean_serie = serie.dropna()
        if clean_serie.nunique() != 2:
            number_invalid_data = 100
            return number_invalid_data
        # Check the nulls in boolean
        if domain_pred == 'Boolean':
            valid_data = clean_serie.apply(is_bool)
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100
        # Detect the boolean string
        elif domain_pred == 'Boolean, string':
            valid_data = ~clean_serie.apply(pd.api.types.is_number)
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100
        # Check the invalid type in boolean decimal
        elif domain_pred == 'Boolean, decimal':
            valid_data = clean_serie.apply(pd.api.types.is_float)
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100
        # Check the invalid type in boolean integer
        elif domain_pred == 'Boolean, integer':
            valid_data = clean_serie.apply(pd.api.types.is_integer)
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100
        # Check the invalid type in boolean gender
        elif domain_pred == 'Gender':
            valid_data = clean_serie.str.lower().isin(
                ['female', 'male', 'm', 'f', 'ladies', 'gentlemen', 'hombre', 'mujer'])
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100
        else:
            return 'Invalid data domain'
        return number_invalid_data

        # NUMBER INVALID DATA DOMAIN

    # Check if value is float
    def is_float(self, value):
        """
        Funtion to check if value is float or not.

        Parameters
        ----------
        value: str.
            Value to analize.

        Returns
        -------
        True if value is float or False is not.
        """
        try:
            float_value = float(value)
            if math.isnan(float_value):
                return False
            return True
        except ValueError:
            return False

    # Check if value is integer
    def is_integer(self, value):
        """
        Funtion to check if value is integer or not.

        Parameters
        ----------
        value: str.
            Value to analize.

        Returns
        -------
        True if value is integer or False is not.
        """
        try:
            float_value = float(value)
            if float_value.is_integer():
                return True
            return False
        except ValueError:
            return False

    # NUMBER DETECTOR
    def number_type(self, serie, domain_pred):
        """
        Function to get the data domain error in the folowing columns:
        ['Integer, quantity', 'Decimal, quantity', 'Integer, secuential number',
         'Integer, unique number', 'Decimal, unique number', 'Integer, unique number', 'Decimal, unique number']

        Parameters
        ----------
        serie: pandas series
            series to analyze.
        domain_pred: dict
            Domain select for the column

        Returns
        -------
        number_invalid_data: float
            Percentage of invalid data.
        """
        # Create the final dict
        list_numbers = []

        # Eliminate nulls
        clean_serie = serie.dropna()

        # Count unique values
        value_numbers = clean_serie.nunique()

        if (domain_pred == 'Integer, unique number') & (value_numbers == 1):
            valid_data = clean_serie.apply(self.is_integer)
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100

        elif (domain_pred == 'Decimal, unique number') & (value_numbers == 1):
            valid_data = clean_serie.apply(self.is_float)
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100

        elif (domain_pred == 'Integer, secuential number') & (serie.shape[0] == serie.nunique()) & (
                clean_serie.dtypes == int):
            number_invalid_data = 0

        elif domain_pred == 'Integer, quantity':
            valid_data = clean_serie.apply(self.is_integer)
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100

        elif domain_pred == 'Decimal, quantity':
            valid_data = clean_serie.apply(self.is_float)
            number_invalid_data = (clean_serie.shape[0] - valid_data.sum()) / clean_serie.shape[0] * 100

        elif (domain_pred in ['Integer, unique number', 'Decimal, unique number']) & (
                clean_serie.dtypes in [int, float]):
            if value_numbers == 1:
                number_invalid_data = 0
            else:
                valid_data = clean_serie.value_counts().iloc[0]
                number_invalid_data = (clean_serie.shape[0] - valid_data) / clean_serie.shape[0] * 100
        else:
            return 'Invalid data domain'

        return number_invalid_data

    # REGEX DOMAIN INVALID DATATYPES
    def domain_detector(self, serie, str_regex):
        """
        Function to check if serie is an telephone domain
        Parameters
        ----------
        df: pandas serie
            serie with the columns to analyze.

        Returns
        -------
        If the columns is telephone return the proportion of the data that is null, if not return numpy nan
        """
        # Eliminate null values
        clean_series = serie.dropna()
        # If the domain selected was finding return the proportion of invalid domain selected of the series
        domain_serie = clean_series.str.contains(str_regex, regex=True)
        # Return the invalid data
        return (clean_series.shape[0] - domain_serie.sum()) / clean_series.shape[0] * 100

    def domain_type(self, df, str_regex):
        """
        Function to get the error in the data domain selected given a regex.
        Parameters
        ----------
        df: pandas dataframe
            dataframe with the columns to analyze.
        str_regex: string
            regex to analyze.

        Returns
        -------
        df_domains: float
            Percentage of invalid data.
        """
        # Apply the function domain dectector for select the columns that have the domain selected
        df_domains = df.apply(lambda x: self.domain_detector(x, str_regex)).dropna().round(2)

        return df_domains

    # INVALID DATE AND TIME DETECTOR
    def get_date(self, series, time_infer='date'):
        """
        Function to check if series is a datetime series
        Parameters
        ----------
        series: pandas series
            series with the columns to analyze.

        Returns
        -------
        Return the proportion of the data that is null
        """
        new_series = series.dropna()
        # if the value is null, the value is not a date. Create a mask series that will be true if the value is date else will be false
        if time_infer == 'date':
            series_date = ~ pd.to_datetime(new_series, errors='coerce', infer_datetime_format=True).isna()
        else:
            series_date = ~ pd.to_timedelta(new_series, errors='coerce').isna()

        # Return the invalid value
        return (new_series.shape[0] - series_date.sum()) / new_series.shape[0] * 100

    def get_dict_dates(self, df, time_infer):
        """
        Function to check if string columns refers to a date
        Parameters
        ----------
        df: pandas dataframe
            dataframe with the columns to analyze.

        Returns
        -------
        dict_bool: float
            Percentage of invalid data.
        """
        df_valid_date = df.apply(lambda x: self.get_date(x, time_infer)).round(2)

        return df_valid_date

    # INVALID MONTH AND DAY DETECTOR
    def wich_date(self, series, time_evaluate='Month'):
        """
        Function to count the invalid month or day in a series
        Parameters
        ----------
        series: pandas series
            series with the columns to analyze.
        time_evaluate: string
            The time to evalute. Can be month or day

        Returns
        -------
        invalid_percentage: float
            The percentage of invalid month or date
        """
        clean_series = series.dropna()

        months = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october',
                  'november', 'december',
                  'jun', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
        days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday',
                'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']

        if time_evaluate == 'Month':
            valid = clean_series.str.lower().apply(lambda x: x in months).sum()
        elif time_evaluate == 'Day':
            valid = clean_series.str.lower().apply(lambda x: x in days).sum()
        else:
            return 'Invalid time to evaluate'

        invalid_percentage = (clean_series.shape[0] - valid) / clean_series.shape[0] * 100

        return invalid_percentage

    def get_month_day(self, df, time_evaluate='Month'):
        """
        Function to count the invalid month or day
        Parameters
        ----------
        df: pandas dataframe
            dataframe with the columns to analyze.
        time_evaluate: string
            The time to evalute. Can be month or day

        Returns
        -------
        invalid_month_day: Pandas series
            Series with the invalid month or day data
        """

        invalid_month_day = df.apply(lambda x: self.wich_date(x, time_evaluate))

        return invalid_month_day

    # COUNTRY INVALID DATA DETECTOR
    def is_country(self, serie):
        """
        Function to check if value of a serie is a country or not.

        Parameters
        ----------
        serie: pandas serie
            serie with the values to analyze.

        Returns
        -------
        If the series has country return the percentage of invalid countries. If not return np nan.
        """
        clean_serie = serie.dropna()
        # Take the len of the string to check if the string refers to a country code
        len_values = clean_serie.str.len().value_counts()
        most_value = len_values.index[0]
        # If the most common len is in 2 or 3 and the frecuency is more than 80% of the data check the code
        if (most_value in [2, 3]) & (len_values.iloc[0] > clean_serie.shape[0] * 0.8):
            contries_serie = clean_serie.apply(lambda x: pycountry.countries.get(**{'alpha_' + str(most_value): x}))
        # Else check if the string is a country
        else:
            contries_serie = clean_serie.apply(lambda x: pycountry.countries.get(name=x))

        return (clean_serie.shape[0] - contries_serie.dropna().shape[0]) / clean_serie.shape[0] * 100

    def get_country(self, df):
        """
        Function to check the invalid country data domain values
        Parameters
        ----------
        df: pandas dataframe
            dataframe with the columna to analyze.

        Returns
        -------
        df_country: pandas series
            Serie with the invalid country data domain.
        """
        # Pass the dataframe to string
        df_string = df.astype(str)
        # Detect the countries
        df_country = df_string.apply(self.is_country).round(2)
        return df_country

    # CURRENCIES AND LENGUAGES INVALID DATA DOMAIN DETECTOR
    def is_domain(self, serie, patron):
        """
        Function to check if value of a serie belongs to a certain domain.
        Parameters
        ----------
        serie: pandas serie
            serie with the values to analyze.

        Returns
        -------
        If the series belongs to a certain domain return the percentage of invalid domain values.
        If not return np nan.
        """
        clean_serie = serie.dropna()

        domain_serie = clean_serie.apply(lambda x: pycountry.currencies.get(**{patron: x}))
        # Return the invalid data
        return (clean_serie.shape[0] - domain_serie.dropna().shape[0]) / clean_serie.shape[0] * 100

    def get_domain_pycountry(self, df, domain, patron):
        """
        Function to get the invalid currency o lenguage data domain values
        Parameters
        ----------
        df: pandas dataframe
            dataframe with the columns to analyze.

        Returns
        -------
        df_domain: pandas series.
            Series with the data domain errors
        """
        # Pass to string the dataframe
        df_string = df.astype(str)
        # Check the invalid data domain
        df_domain = df_string.apply(lambda x: self.is_domain(x, patron)).round(2)

        return df_domain

    # COORDINATES INVALID DATA DETECTOR
    def is_coordinates(self, serie, str_regex):
        clean_serie = serie.dropna()
        # See if the coordinates are in the column
        mask_serie = clean_serie.astype(str).str.contains(str_regex)
        # Check the invalid data
        invalid_data = (clean_serie.shape[0] - mask_serie.sum()) / clean_serie.shape[0] * 100

        return invalid_data

    def get_coordinates(self, df, str_regex):
        """
        Function to get the invalid coordinates data domain values.
        Parameters
        ----------
        df: pandas dataframe
            dataframe with the columns to analyze.

        Returns
        -------
        df_domains: pandas serie
            Series with the errors in the data domain coordinates.
        """
        # Apply the function domain dectector for select the columns that have the domain selected
        df_domains = df.apply(lambda x: self.is_coordinates(x, str_regex)).round(2)

        return df_domains

    # UNIQUE STRING INVALID DATA DOMAIN DETECTOR
    def get_unique_string(self, serie):
        """
        Function to get the invalid unique string data domain values
        Parameters
        ----------
        df: pandas dataframe
            dataframe with the columns to analyze.

        Returns
        -------
        invalid_data: float
            Invalid data domain.
        """

        clean_serie = serie.dropna()

        nvalues = clean_serie.value_counts()

        most_frequent = nvalues.iloc[0]

        # Get the nulls values of this columns
        invalid_data = (clean_serie.shape[0] - most_frequent) / clean_serie.shape[0] * 100

        return round(invalid_data, 2)

    # CATEGORICAL INVALID DATA DOMAIN DETECTOR
    def number_string(self, value):
        """
        Function to check if value is number or not
        """
        try:
            float(value)
            return True
        except:
            return False

    # CATEGORICAL INVALID DATA DOMAIN DETECTOR
    def get_categorical_columns(self, serie):
        """
        Function to get the invalid string or categorical data domain values
        Parameters
        ----------
        data: pandas dataframe
            dataframe with the columns to analyze.

        Returns
        -------
        invalid_data: float.
            Invalid data domain.
        """

        clean_serie = serie.dropna()

        string_values = ~clean_serie.apply(self.number_string)

        invalid_data = (clean_serie.shape[0] - string_values.sum()) / clean_serie.shape[0] * 100

        return invalid_data

    def transform(self, data, select_domain):
        """
        Function to apply all the data domain detector to a dataframe
        Parameters
        ----------
        df: pandas dataframe
            dataframe with the columns to analyze.
        select_domain: dict
            Dictionary with keys with the column name and the values as the domain selected

        Returns
        -------
        dict_bool: Dictionary
            Dictionary with the columns with his data domain.
        """

        dict_domain = {}

        # Fix the datatypes
        data.loc[:, data.select_dtypes(int).columns] = data.select_dtypes(int).astype(int)

        # Select the columns that was predicted as boolean
        bool_gender = ['Boolean', 'Boolean, string', 'Boolean, decimal', 'Boolean, integer', 'Gender']
        bool_columns = [key for (key, value) in select_domain.items() if value in bool_gender]
        # Filter from the data and check the nulls
        invalid_data_bool = data.filter(bool_columns).apply(lambda x: self.binaries_values(x, select_domain[x.name]))
        dict_domain.update(invalid_data_bool.to_dict())

        # Select the columns that was predicted as number
        numbers_values = ['Integer, quantity', 'Decimal, quantity', 'Integer, secuential number',
                          'Integer, unique number', 'Decimal, unique number']
        # Filter from the data and check the nulls
        numbers_columns = [key for (key, value) in select_domain.items() if value in numbers_values]
        invalid_data_number = data.filter(numbers_columns).apply(lambda x: self.number_type(x, select_domain[x.name]))
        dict_domain.update(invalid_data_number.to_dict())

        # Select the columns that was predicted as link
        links_columns = [key for (key, value) in select_domain.items() if 'Links' in value]
        # Detect invalid data in links columns
        dict_link = self.domain_type(data.filter(links_columns), self.link)
        dict_domain.update(dict_link.to_dict())

        # Select the columns that was predicted as email
        email_columns = [key for (key, value) in select_domain.items() if 'Email' in value]
        # Detect invalid data in email columns
        dict_email = self.domain_type(data.filter(email_columns), self.email)
        dict_domain.update(dict_email.to_dict())

        # Select the columns that was predicted as Telephone
        telephone_columns = [key for (key, value) in select_domain.items() if 'Telephone' in value]
        # Detect invalid data in Telephone columns
        dict_telephone = self.domain_type(data.filter(telephone_columns), self.telephone)
        dict_domain.update(dict_telephone.to_dict())

        # Detect address columns
        address_columns = [key for (key, value) in select_domain.items() if 'Address' in value]
        # Detect invalid data in Address columns
        dict_address = self.domain_type(data.filter(address_columns), self.address)
        dict_domain.update(dict_address.to_dict())

        # Select the columns that was predicted as Date
        date_columns = [key for (key, value) in select_domain.items() if 'Date' in value]
        # Detect the invalid date data
        dic_date = self.get_dict_dates(data.filter(date_columns), 'Date')
        dict_domain.update(dic_date.to_dict())

        # Select the columns that was predicted as Time
        time_columns = [key for (key, value) in select_domain.items() if 'Time' in value]
        # Detect the invalid time data
        dic_time = self.get_dict_dates(data.filter(time_columns), 'Time')
        dict_domain.update(dic_time.to_dict())

        # Select the columns that was predicted as Month
        month_columns = [key for (key, value) in select_domain.items() if 'Month' in value]
        # Detect the invalid motnh data
        dic_month = self.get_month_day(data.filter(month_columns), 'Month')
        dict_domain.update(dic_month.to_dict())

        # Select the columns that was predicted as day
        day_columns = [key for (key, value) in select_domain.items() if 'Day' in value]
        # Detect the invalid day data
        dic_day = self.get_month_day(data.filter(day_columns), 'Day')
        dict_domain.update(dic_day.to_dict())

        # Select the columns that was predicted as Country
        country_columns = [key for (key, value) in select_domain.items() if 'Country' in value]
        dict_country = self.get_country(data.filter(country_columns))
        dict_domain.update(dict_country.to_dict())

        # Select the columns that was predicted as Currency'
        currency_columns = [key for (key, value) in select_domain.items() if 'Currency' in value]
        # Detect the invalid currency data domain
        dict_currency = self.get_domain_pycountry(data.filter(currency_columns), 'Currency', 'alpha_3')
        dict_domain.update(dict_currency.to_dict())

        # Select the columns that was predicted as Telephone
        lenguages_columns = [key for (key, value) in select_domain.items() if 'Languages' in value]
        # Detect invalid lenguages data domain
        dict_lenguages = self.get_domain_pycountry(data.filter(lenguages_columns), 'Languages', 'name')
        dict_domain.update(dict_lenguages.to_dict())

        # Detect coordinates columns
        coordinates_columns = [key for (key, value) in select_domain.items() if 'Coordinates' in value]
        # Detect invalid coordinates data domain
        dict_coordinates = self.get_coordinates(data.filter(coordinates_columns), self.coordinates)
        dict_domain.update(dict_coordinates.to_dict())

        # Detect unique string columns
        unique_string_columns = [key for (key, value) in select_domain.items() if 'String, unique' in value]
        # Detect invalid unique string data domain
        dict_unique_string = data.filter(unique_string_columns).apply(self.get_unique_string)
        dict_domain.update(dict_unique_string.to_dict())

        # Detect string categorical columns
        categorical_columns = [key for (key, value) in select_domain.items() if
                               value in ['String, categorical columns', 'String']]
        # Detect invalid categorical columns data domain
        dic_categorical = data.filter(categorical_columns).apply(self.get_categorical_columns)
        dict_domain.update(dic_categorical.to_dict())

        rest_columns = [col for col in data.columns if col not in dict_domain.keys()]
        dict_rest = dict.fromkeys(rest_columns, 'Invalid data domain')
        dict_domain.update(dict_rest)

        return dict_domain