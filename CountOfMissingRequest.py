import json
import pyspark
from pyspark.sql.functions import col,isnan, when
import pyspark.sql.functions as f
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def CountOfMissingdef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId
    InputTable = path + "/" + inputDeltaTable
    print("Reading data")
    try:
        Data = spark.read.format("delta").load(InputTable)

        columnNames = Data.columns
        # print(columnNames)
        from pyspark.sql.functions import count
        invalidlist = Data.select(
            [count(when((col(c) == '') | col(c).isNull() | isnan(c) | f.isnull(c), c)).alias(c) for c in
             Data.columns]).collect()

        dictF = {"RowValues": []}
        for i in columnNames:
            dictU = {}
            dictU["column_name"] = i
            # count of records
            count = Data.select(col(i)).count()
            dictU["No. of values"] = count
            # invalid data count
            nan_count = invalidlist[0][i]
            dictU["Missing_Value_Count"] = nan_count
            print("Rowdetails for column ", i, " is :", dictU)
            dictF["RowValues"].append(dictU)

        print(dictF)
        # Serializing json
        json_object = json.dumps(dictF, indent=4)
        list = {"status": "Success", "message": "Successfully Completed count of missing values Request",
                "data": json_object}
        return json.dumps(list)
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the input parameters!"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)
        return json_object

#CountOfMissingdef("1","abc123","s01","1","3","2")