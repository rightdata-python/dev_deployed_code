import pyspark
import json
from pyspark.sql.functions import lit, col, isnan, count
from properties import path
from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev

spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def replaceMissingdef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,parameter,customValue,columnName,replaceColumn,valueToReplace):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    DeltaTable = path + "/" + inputDeltaTable
    print("Reading Universal data")
    try:
        Data = spark.read.format("delta").load(DeltaTable)
        # columnNames = Data.columns
        print("Column names in Delta table: ", Data.columns)
        print("No. of records in Data:", Data.count())

        columnsdtypeint = []
        columnsdtypeother = []
        for y in Data.dtypes:
            if y[1] == 'int' or y[1] == 'double':
                columnsdtypeint.append(y[0])
            else:
                columnsdtypeother.append(y[0])

        if parameter == "CustomValue":
            df = Data.fillna({columnName: valueToReplace})
            df.show()

        elif parameter == "Predefined values":
            if customValue == "Mean":
                if columnName in columnsdtypeint:
                    uData_stats = Data.select(_mean(col(columnName)).alias('mean')).collect()
                    mean = uData_stats[0]['mean']
                    df = Data.na.fill(value=mean, subset=[columnName])
                else:
                    df = Data.na.fill(value="null", subset=[columnName])
                df.show()

            elif customValue == "Median":
                if columnName in columnsdtypeint:
                    from pyspark.sql.types import IntegerType
                    data_df = Data.withColumn(columnName, Data[columnName].cast(IntegerType()))
                    median = data_df.approxQuantile(columnName, [0.5], 0.25)
                    df = Data.na.fill(value=median[0], subset=[columnName])
                else:
                    df = Data.na.fill(value="null", subset=[columnName])
                df.show()

            elif customValue == "Mode":
                mode = Data.groupby(columnName).count().orderBy("count", ascending=False).first()[0]
                df = Data.na.fill(value=mode, subset=[columnName])
                df.show()

            elif customValue == "Std Deviation":
                if columnName in columnsdtypeint:
                    uData_stats = Data.select(_stddev(col(columnName)).alias('std')).collect()
                    std = uData_stats[0]['std']
                    df = Data.na.fill(value=std, subset=[columnName])
                else:
                    df = Data.na.fill(value="null", subset=[columnName])
                df.show()

            elif customValue == "Copy from column":
                from pyspark.sql.functions import coalesce
                df = Data.withColumn(columnName, coalesce(columnName, replaceColumn))
                df.show()
            else:
                print("Please select appropriate value")

        outputDeltaTablepath = path + "/" + outputDeltaTable
        df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully replaced missing values in selected column",
                "data": {"inputsteps": Data.count(), "outputsteps": df.count()}}
        return json.dumps(list)

    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}#"Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)


