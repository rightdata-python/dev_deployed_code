import pyspark
import pandas as pd
from pandas.api.types import is_numeric_dtype
from properties import path

import json
from deltalake import DeltaTable

from Regex import replace_regex
import pyspark.sql.functions as f
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def customRemovedef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,customValue,columnName,value, columnToMatch):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Reading data")
    try:
        #df = spark.read.format("delta").load(DeltaTable)
        df = DeltaTable(inputDeltaTablepath).to_pandas()
        # columnNames = Data.columns
        print("No of Columns and Column names in Delta table: ", len(df.columns),";",df.columns)
        print("No. of records in Data:", len(df))
        print(df)

        print("Datatype of column:",df.dtypes[columnName])
        if customValue == "lessthanWithValue":
            print("customValue",customValue)
            print(value[0])
            try:
                if is_numeric_dtype(df[columnName]):
                    df3 = df[~(df[columnName] <= value[0])]
                    print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                    print("No. of records in Dataframe:", len(df3))
                    print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Less Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        if customValue == "lessthanWithColumn":
            print("customValue",customValue)
            print(columnToMatch[0])
            try:
                if is_numeric_dtype(df[columnName]):

                    df3 = df.loc[~df[columnName] <= df[columnToMatch[0]]]
                    #df3 = df[~(df[columnName] <= df[columnToMatch[0]])]
                    print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                    print("No. of records in Dataframe:", len(df3))
                    print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Less Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)


        elif customValue == "greaterthanWithValue":
            print("customValue", customValue)
            try:
                if is_numeric_dtype(df[columnName]):
                    df3 = df[~(df[columnName] >= value[0])]
                    print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                    print("No. of records in Dataframe:", len(df3))
                    print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Greater Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "greaterthanWithColumn":
            print("customValue", customValue)
            try:
                if is_numeric_dtype(df[columnName]):
                    df3 = df.loc[~df[columnName] >= df[columnToMatch[0]]]
                    print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                    print("No. of records in Dataframe:", len(df3))
                    print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Greater Than is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "inbetweenWithValue":
            # inbetween
            print("customValue", customValue)
            try:
                if is_numeric_dtype(df[columnName]):
                    df3 = df[~((df[columnName] > value[0]) & (df[columnName] < value[1]))]
                    # if value[0] < value[1]:
                    #     df1 = df[~(df[columnName] <= value[0])]
                    #     df3 = df1[~(df1[columnName] >= value[1])]
                    # else:
                    #     df1 = df[~(df[columnName] <= value[1])]
                    #     df3 = df1[~(df1[columnName] >= value[0])]

                    print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                    print("No. of records in Dataframe:", len(df3))
                    print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "In between is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "inbetweenWithColumn":
            # inbetween
            print("customValue", customValue)

            try:
                if is_numeric_dtype(df[columnName]):
                    print("columns", df[columnToMatch[0]], df[columnToMatch[1]])
                    df1 = df.loc[~df[columnName] <= df[columnToMatch[0]]]
                    df3 = df1.loc[~df1[columnName] >= df1[columnToMatch[0]]]

                    print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                    print("No. of records in Dataframe:", len(df3))
                    print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "In between is not applicable for the column selected"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "distinct":
            print("customValue",customValue)
            print("occurances",value[0])
            #distinct
            try:
                occurances = value[0]
                v = df[columnName].value_counts()
                print(v)

                df3 = df[~df[columnName].isin(v.index[v == occurances])]
                #df3 = df[df[columnName].duplicated()]
                #df3 = df.drop_duplicates(columnName, keep='first')
                print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                print("No. of records in Dataframe:", len(df3))
                print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "contains":#value should always be in form ["3"] even for the int columns
            print("customValue",customValue)
            #df[columnName] = df[columnName].astype(str)
            containsvalue = value[0]
            print("containsvalue",containsvalue)
            #contains
            #df3 = df.drop(df.index[df[columnName] == value])
            try:
                df3 = df[~df[columnName].astype(str).str.contains(containsvalue)]
                #df3 = df[~df[columnName].isin(value)]
                #df3 = df[df[columnName].contains(containsvalue)==False]
                print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                print("No. of records in Dataframe:", len(df3))
                print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "startswith":#value should always be in form ["3"] even for the int columns
            print("customValue",customValue)
            try:
                df3 = df[~df[columnName].astype(str).str.startswith(value[0])]
                print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                print("No. of records in Dataframe:", len(df3))
                print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "endswith":#value should always be in form ["3"] even for the int columns
            print("customValue", customValue)
            #endswith
            try:
                df3 = df[~df[columnName].astype(str).str.endswith(value[0])]
                print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                print("No. of records in Dataframe:", len(df3))
                print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        elif customValue == "missing":
            print("customValue", customValue)
            # endswith
            try:
                # import numpy as np
                # df6 =df[columnName].replace(' ', np.nan, inplace=True)
                # df5 =df6[columnName].replace('NAN', np.nan, inplace=True)
                # df4 =df5[columnName].replace('Null', np.nan, inplace=True)
                # df2 =df4[columnName].replace('None', np.nan, inplace=True)
                df3 = df[~(df[columnName] == ' ')]
                df3 = df3[~(df3[columnName] == 'nan')]
                df3 = df3[~(df3[columnName] == 'NULL')]
                df3 = df3[~(df3[columnName] == 'NAN')]
                df3 = df3[~(df3[columnName] == 'Null')]
                df3 = df3[~(df3[columnName] == 'None')]
                df3 = df3.dropna(subset=[columnName], how='all')
                print("No of Columns and Column names in Dataframe: ", len(df3.columns), ";", df3.columns)
                print("No. of records in Dataframe:", len(df3))
                print(df3)
            except:
                FailureMessage = {"status": "Failure",
                                  "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)
        elif customValue == "REstartswith" or customValue == "REendswith" or customValue == "REcontains":
            print("customValue",customValue)
            print(value[0])
            regex_str = value[0]
            try:
                if ('{' in regex_str) | ('}' in regex_str):
                    print("pattern: ",regex_str)
                #if ('{' in regex_str) | ('}' in regex_str):
                    df3 = replace_regex(columnName, regex_str, "valueToReplace", df, method='remove', pattern=True)
                else:
                    print("regex: ", regex_str)
                    df3 = replace_regex(columnName, regex_str, "valueToReplace", df, method='remove', pattern=False)
                print(df3)

            except:
                FailureMessage = {"status": "Failure",
                                      "message": "Some thing went wrong please check the inputs"}
                FailureMessageJson = json.dumps(FailureMessage)
                print(FailureMessageJson)
                return json.dumps(FailureMessage)

        df1 = spark.createDataFrame(df3)
        df1.show()
        outputDeltaTablepath = path + "/" + outputDeltaTable
        df1.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully removed the missing data in selected column",
                "data": {"inputsteps": len(df), "outputsteps": df1.count()}}
        return json.dumps(list)
    # except OSError as err:
    #     print("OS error: {0}".format(err))
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Delta Table entered doesnt exists, Please enter a valid Delta Table."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

#customRemovedef("1","newPSNAN","s01","3", "222","1","missing","FT_TEACHER",["70"])