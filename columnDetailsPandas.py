import pandas as pd
import json
import re # amazing text cleaning for decode issues..
from ftfy import fix_text
from deltalake import DeltaTable
import numpy as np
from scipy.sparse import csr_matrix
#import sparse_dot_topn.sparse_dot_topn as ct
from deltalake import DeltaTable
from properties import path
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"


def columnDetailsdef(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId, columnName):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + sessionId + "_" + inputStepId + "_" + inputResultId
    inputDeltaTablepath = path + "/" + inputDeltaTable

    try:
        print("Reading Universal data")
        pandasDF = DeltaTable(inputDeltaTablepath).to_pandas()
        print("No of Columns and Column names in Delta table: ", len(pandasDF.columns), ";", pandasDF.columns)
        print("No. of records in Data:", len(pandasDF))
        try:
            invalidlist = Data.select(
                [count(when(col(c).isNull() | isnan(c), c)).alias(c) for c in Data.columns]).collect()
            emptylist = Data.select([count(when((col(c) == ''), c)).alias(c) for c in Data.columns]).collect()
            countRecords = Data.select(col(columnName)).count()
            Invalid = invalidlist[0][ind]
            Valid = countRecords - Invalid
            valid_percentage = (Valid / countRecords) * 100
            invalid_perentage = (Invalid / countRecords) * 100
            empty = emptylist[0][ind]
            empty_percentage = (empty / countRecords) * 100
            nullValues = pandasDF[columnName].isna().sum()
            countRecords = len(pandasDF)
            Valid=
            valid_percentage=
            Invalid= len(pandasDF[pandasDF['EPS'].notnull()])
            Invalid_percentage= (Invalid/countRecords) *100

            empty=
            empty_percentage=
            # DQList = [{"Total Rows": countRecords, "Valid Values": Valid, "InValid Values": Invalid, "Empty": empty}]
            DQList = {"Data Quality": [{"Total Rows": {"value": countRecords, "percentage": 100},
                                        "Valid Values": {"value": Valid, "percentage": valid_percentage},
                                        "InValid Values": {"value": Invalid, "percentage": Invalid_percentage},
                                        "Empty": {"value": empty, "percentage": empty_percentage}}]}
            DQListJSON = json.dumps(DQList)
            print("Data Quality list:", DQListJSON)

            #######Unique values and Stats#############
            distinct_ids = [x[columnName] for x in Data.select(columnName).distinct().collect()]
            # print(distinct_ids)
            UVList = {"unique Values": []}
            for i in distinct_ids:
                countdistint = Data.select().where((Data[columnName] == i)).count()
                Unique_Value = {i: countdistint}
                UVList["unique Values"].append(Unique_Value)
            # print("Unique Value List", UVList)
            UVListJSON = json.dumps(UVList)
            print("Unique Value List:", UVListJSON)
            # distinct_column_val = Data.select(columnName).distinct().collect()
            # distinct_column_vals = [v[columnName] for v in distinct_column_val]
            # print("unique values",distinct_column_vals)
            # UVList = []
            # for i in distinct_column_vals:
            #     #unique = dict(zip(i, [Data.select().where((Data[columnName] == i)).count()]))
            #     #print(unique)
            #     UVList.append(unique)
            # print("unique values list: ", UVList)
            # Statistics ={"Statistics info":[]}
            dictu = {}
            # max
            df = Data.dropna(subset=[columnName])
            rowvalue = df.agg({columnName: "max"}).collect()[0]
            maxvalue = rowvalue[f"max({columnName})"]
            # dictu["Max"] = maxvalue
            # min
            rowminvalue = df.agg({columnName: "min"}).collect()[0]
            minvalue = rowminvalue[f"min({columnName})"]
            # dictu["Min"] = minvalue
            distinct1 = Data.select(col(columnName)).distinct().count()
            # dictu["Distinct"] = distinct1
            # Get All column names and it's types
            columnsdtypeint = []
            columnsdtypeother = []
            for y in Data.dtypes:
                if y[1] == 'int' or y[1] == 'double':
                    columnsdtypeint.append(y[0])
                else:
                    columnsdtypeother.append(y[0])
            if columnName in columnsdtypeint:
                # mean & standard diviation
                uData_stats = Data.select(_mean(col(columnName)).alias('mean'),
                                          _stddev(col(columnName)).alias('std')).collect()
                mean = uData_stats[0]['mean']
                std = uData_stats[0]['std']
                # dictu["Mean"] = mean
                # dictu["Standard Deviation"] = std
                # median
                from pyspark.sql.types import IntegerType
                data_df = Data.withColumn(columnName, Data[columnName].cast(IntegerType()))
                median = data_df.approxQuantile(columnName, [0.5], 0.25)
                # dictu["Median"] = median[0]
                # dictu["Mode"] = "N/A"
                Statistics = [
                    {"Min": minvalue, "Max": maxvalue, "Mean": mean, "Median": median[0], "Standard Deviation": std,
                     "Mode": "N/A", "Distinct": distinct1}]
            else:
                # dictu["Mean"] = "N/A"
                # dictu["Standard Deviation"] = "N/A"
                # median
                # dictu["Median"] = "N/A"
                mode = Data.groupby(columnName).count().orderBy("count", ascending=False).first()[0]
                dictu["Mode"] = mode
                Statistics = {"Statistics": [
                    {"Min": minvalue, "Max": maxvalue, "Mean": "N/A", "Median": "N/A", "Standard Deviation": "N/A",
                     "Mode": mode, "Distinct": distinct1}]}
            # Statistics["Statistics info"].append(dictu)

            StatisticsJSON = json.dumps(Statistics)
            print("Statistics: ", StatisticsJSON)
            ############Distribution###########################
            distinctvalues = Data.select(col(columnName)).distinct().count()
            Cardianlity = (distinctvalues / countRecords) * 100
            if Cardianlity <= 80:
                df = Data.groupBy(columnName).count()
                dfpandas = df.toPandas()
                sorted_df = dfpandas.sort_values(["count"], ascending=False)
                result = sorted_df.to_json(orient="index")
                parsed = json.loads(result)
                Disjsonobject = json.dumps(parsed, indent=4)
                Disjson = {"Distribution": [Disjsonobject]}
                Disjson = json.dumps(Disjson)
                print("Distribution:", Disjson)
            else:
                if columnName in columnsdtypeint:
                    df = Data.toPandas()
                    df['quantile1'] = pd.qcut(df[columnName], q=10, precision=0)
                    quartile = df['quantile1'].value_counts()
                    Disjson = quartile.to_json()
                    # quartiledf = quartile.DataFrame({'List': quartile.index, 'Count': quartile.values})
                    # result = quartiledf.to_json(orient="values")
                    # parsed = json.loads(result)
                    # Disjsonobject = json.dumps(parsed, indent=4)
                    # Disjson = {"Distribution": Disjsonobject}
                    # Disjson = json.dumps(Disjson)
                    # top10List = sorted_df.to_json(orient = "split")
                    print("Distribution:", Disjson)
                else:
                    Disjson = {"Distribution": "N/A"}
                    Disjson = json.dumps(Disjson)
                    print("Distribution chart not applicable to this column")
            # #pattern
            # PandasDF = Data.toPandas()
            # a = len(PandasDF)
            # # The code to generate the matrix of TF-IDF values for each is shown below.
            # org_names = PandasDF[columnName].unique()
            # vectorizer = TfidfVectorizer(min_df=1, analyzer=ngrams)
            # # print('Vecorizing completed...')
            # tf_idf_matrix = vectorizer.fit_transform(org_names)
            #
            # matches = awesome_cossim_top(tf_idf_matrix, tf_idf_matrix.transpose(), 10, 0.7)
            #
            # matches_df = get_matches_df(matches, org_names, top=a)
            # matches_df = matches_df[matches_df['similairity'] < 0.99999]  # Remove all exact matches
            #
            # matches_df1 = matches_df.sort_values(['similairity'], ascending=False)
            #
            # matchedcount = matches_df1['left_side'].value_counts()
            # # a = matches_df['left_side'].unique()
            # # b = a.nlargest(10, ['left_side'])
            # top10matchedcount1 = matchedcount.nlargest(5, keep='all')
            # top10matchedcount = top10matchedcount1.head()
            # top10matchedcountDF = pd.DataFrame({'List': top10matchedcount.index, 'Count': top10matchedcount.values})
            # result = top10matchedcountDF.to_json(orient="values")
            # parsed = json.loads(result)
            # patternJson = json.dumps(parsed, indent=4)
            # patternJson = {"Patterns": patternJson}
            # print("patternJson", patternJson)

            final_list = [DQListJSON, UVListJSON, Disjson, StatisticsJSON]  # patternJson
            json_object = json.dumps(final_list, indent=4, separators=(',', ':'))
            #lreturn fina_listJson  # [DQListJSON, UVListJSON, Disjsonobject, StatisticsJSON]
            list = {"status": "Success", "message": "Successfully completed column details request",
                    "data": json_object}
            return json.dumps(list)
        except:
            FailureMessage = {"status": "Failure",
                              "message": "Please check the column selected, Cannot retrieve column details if column doesnt exists or contains all null values."}
            FailureMessageJson = json.dumps(FailureMessage)
            print(FailureMessageJson)
            return json.dumps(FailureMessage)
            return json_object

    except:
        FailureMessage = {"status": "Failure",
                          "message": "Delta Table entered doesnt exists, Please enter a valid Delta Table!"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)
        return json_object

#columnDetailsdef("1","newPSNAN","s01","3", "4","1", "ENROLLMENT")
#result = columnDetailsdef("1","abc123","s01","3", "4","1", "LEVEL")
#print(result)
def ngrams(string, n=3):
    string = str(string)
    string = fix_text(string) # fix text
    string = string.encode("ascii", errors="ignore").decode() #remove non ascii chars
    string = string.lower()
    chars_to_remove = [")","(",".","|","[","]","{","}","'"]
    rx = '[' + re.escape(''.join(chars_to_remove)) + ']'
    string = re.sub(rx, '', string)
    string = string.replace('&', 'and')
    string = string.replace(',', ' ')
    string = string.replace('-', ' ')
    string = string.title() # normalise case - capital at start of each word
    string = re.sub(' +',' ',string).strip() # get rid of multiple spaces and replace with a single
    string = ' '+ string +' ' # pad names for ngrams...
    string = re.sub(r'[,-./]|\sBD',r'', string)
    ngrams = zip(*[string[i:] for i in range(n)])
    return [''.join(ngram) for ngram in ngrams]


def awesome_cossim_top(A, B, ntop, lower_bound=0):
    # force A and B as a CSR matrix.
    # If they have already been CSR, there is no overhead
    A = A.tocsr()
    B = B.tocsr()
    M, _ = A.shape
    _, N = B.shape

    idx_dtype = np.int32

    nnz_max = M * ntop

    indptr = np.zeros(M + 1, dtype=idx_dtype)
    indices = np.zeros(nnz_max, dtype=idx_dtype)
    data = np.zeros(nnz_max, dtype=A.dtype)

    # ct.sparse_dot_topn(
    #     M, N, np.asarray(A.indptr, dtype=idx_dtype),
    #     np.asarray(A.indices, dtype=idx_dtype),
    #     A.data,
    #     np.asarray(B.indptr, dtype=idx_dtype),
    #     np.asarray(B.indices, dtype=idx_dtype),
    #     B.data,
    #     ntop,
    #     lower_bound,
    #     indptr, indices, data)

    return csr_matrix((data, indices, indptr), shape=(M, N))

def get_matches_df(sparse_matrix, name_vector, top=100):
    non_zeros = sparse_matrix.nonzero()

    sparserows = non_zeros[0]
    sparsecols = non_zeros[1]

    if top:
        nr_matches = top
    else:
        nr_matches = sparsecols.size

    left_side = np.empty([nr_matches], dtype=object)
    right_side = np.empty([nr_matches], dtype=object)
    similairity = np.zeros(nr_matches)

    for index in range(0, nr_matches):
        left_side[index] = name_vector[sparserows[index]]
        right_side[index] = name_vector[sparsecols[index]]
        similairity[index] = sparse_matrix.data[index]

    return pd.DataFrame({'left_side': left_side,
                         'right_side': right_side,
                         'similairity': similairity})