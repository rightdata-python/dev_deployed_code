import pandas as pd
from sklearn.preprocessing import OrdinalEncoder
import numpy as np
import json
from properties import path

from pyspark.sql.types import StringType
from pyspark.sql.functions import udf

import pyspark
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def ordinal_function(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,col_name,col_name_ordinal,scale_mapper):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    # col_name = "make_code"
    # col_name_ordinal = "Type 1"..............
    # scale_mapper = {"Grass": 1, "Fire": 2, "Water": 3,"Bug":4}
    # col_name_ordinal = col_name_ordinal["mapper"]
    InputTable = path + "/" + inputDeltaTable
    print("Reading Delta Table")
    try:
        Data = spark.read.format("delta").load(InputTable)

        def translate(mapping):
            def translate_(col):
                return mapping.get(col)

            return udf(translate_, StringType())

        df = Data.withColumn(col_name_ordinal, translate(scale_mapper)(col_name))

        # dfPandas =Data.toPandas()
        # #scale_mapper = {"Low": 1, "Medium": 2, "High": 3}
        # dfPandas[col_name_ordinal] = dfPandas[col_name].replace(scale_mapper)
        # SparkDF = spark.createDataFrame(dfPandas)
        print("Column names in Delta table: ", df.columns)
        print(df.show())
        outputDeltaTablepath = path + "/" + outputDeltaTable
        df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully completed Ordinal Encoded for the selected column",
                "data": {"inputsteps": Data.count(), "outputsteps": df.count()}}
        return json.dumps(list)

    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)


