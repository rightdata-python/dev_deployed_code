import json
import pyspark
import pandas as pd
from deltalake import DeltaTable
from pyspark.sql.functions import lit, col,isnan, count, mean as _mean, stddev as _stddev
from sklearn.model_selection import StratifiedShuffleSplit
from pyspark.sql.functions import lit, rand, when
#from properties import path
path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def JoinDatasetsPandas(projectId, reciepeId, sessionId, stepId, leftInputDetails, rightInputDetails, columnMapping, currentStepId, mergeType):

    leftStepID_ResultId = stepId[0]
    rightStepID_ResultId = stepId[1]#print("leftStepID_ResultId",leftStepID_ResultId)#print("rightStepID_ResultId",rightStepID_ResultId)

    leftStepID = leftStepID_ResultId["inputStepId"]
    rightStepID = rightStepID_ResultId["inputStepId"]#print("leftStepID", leftStepID)#print("rightStepID", rightStepID)

    leftResultID = leftStepID_ResultId["inputResultId"]
    rightResultID = rightStepID_ResultId["inputResultId"]#print("leftResultID", leftResultID)#print("rightResultID", rightResultID)

    if leftInputDetails["inputStepId"] == leftStepID:
        leftblendcolumns = leftInputDetails["blendColumns"]
        print("leftblendcolumns", leftblendcolumns)
    elif leftInputDetails["inputStepId"] == rightStepID:
        leftblendcolumns = leftInputDetails["blendColumns"]
        print("leftblendcolumns", leftblendcolumns)
    else:
        return "Please enter a valid StepId"

    if rightInputDetails["inputStepId"] == rightStepID:
        rightblendcolumns = rightInputDetails["blendColumns"]
        print("rightblendcolumns", rightblendcolumns)
    elif rightInputDetails["inputStepId"] == leftStepID:
        rightblendcolumns = rightInputDetails["blendColumns"]
        print("rightblendcolumns", rightblendcolumns)
    else:
        return "Please enter a valid StepId"

    # leftblendcolumns = []
    # for a in leftInputDetails:
    #     leftblendcolumns.append(a['blendColumns'])
    # rightblendcolumns = []
    # for b in rightInputDetails:
    #     rightblendcolumns.append(b['blendColumns'])
    # print("leftblendcolumns", leftblendcolumns)
    # print("rightblendcolumns", rightblendcolumns)

    DcolumnNames = []
    RcolumnNames = []
    for x in columnMapping:
        DcolumnNames.append(x['leftColumn'])
    for y in columnMapping:
        RcolumnNames.append(y['rightColumn'])
    print("DcolumnNames", DcolumnNames)
    print("RcolumnNames", RcolumnNames)

    if sessionId == None:
        dDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + leftStepID + "_" + leftResultID
        rDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + rightStepID + "_" + rightResultID
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        dDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + sessionId + "_" + leftStepID + "_" + leftResultID
        rDeltaTable = "R_" + str(
            projectId) + "_" + reciepeId + "_" + sessionId + "_" + rightStepID + "_" + rightResultID
        outputDeltaTable = "R_" + str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    InputTableD = path + "/" + dDeltaTable
    InputTableR = path + "/" + rDeltaTable
    outputDeltaTablepath = path + "/" + outputDeltaTable
    print("InputTableDriver:",InputTableD)
    print("InputTableReference:", InputTableR)
    print("outputDeltaTable:", outputDeltaTablepath)

    try:
        print("Reading Driver data")
        DData = DeltaTable(InputTableD).to_pandas()
        print("Reading Reference data")
        RData = DeltaTable(InputTableR).to_pandas()

        print("No of records and column names of Driver dataset:", len(DData), DData.columns)
        print("No of records and column names of Reference dataset:", len(RData), RData.columns)

        JoinDF_pandas = pd.merge(DData, RData, left_on=DcolumnNames, right_on=RcolumnNames, how = mergeType)
        print(JoinDF_pandas.head())
        JoinDF = spark.createDataFrame(JoinDF_pandas)
        JoinDF.show()
        # df = df1.join(df2, (df1.A1 == df2.B1) & (df1.A2 == df2.B2))
        # print(DcolumnNames[0])
        # print(RcolumnNames[0])
        # Dcolumn = DcolumnNames[0]
        # Rcolumn = RcolumnNames[0]
        # cond = []
        # for i in len(DcolumnNames):
        #     a = 'DData.'+DcolumnNames[i]+'=='+'RData.'+RcolumnNames[i]
        #     cond.append(a)
        # print(cond)
        # cond = [df.name == df3.name, df.age == df3.age]
        # JoinDF = DData.join(RData, DData[DcolumnNames[0]] == RData[RcolumnNames[0]], mergeType)
        # print(JoinDF.count(), len(JoinDF.columns))  # .show(truncate=False)
        # print("Join complete")
        # if Dcolumn == Rcolumn:
        #     #RDataprim = RData.withColumnRenamed(Rcolumn, "join_RcolumnName")
        #     #JoinDF = DData.join(RDataprim, DData[Dcolumn] == RDataprim["join_RcolumnName"], mergeType).drop("join_RcolumnName")
        #     JoinDF = DData.join(RDataprim, left_on = DcolumnNames, right_on = RcolumnNames, how = mergeType)
        #     # JoinDF= JoinDF.drop("join_RcolumnName")
        #     print(JoinDF.count(), len(JoinDF.columns))  # .show(truncate=False)
        #     print("Join complete")
        # else:
        #     JoinDF = DData.join(RData, DData[DcolumnNames[0]] == RData[RcolumnNames[0]], mergeType)
        #     print(JoinDF.count(), len(JoinDF.columns))  # .show(truncate=False)
        #     print("Join complete")

        # JoinDF.show()
        # JoinDF.write.format("delta").save(outputDeltaTable)
        #
        outputDeltaTablepath = path + "/" + outputDeltaTable
        JoinDF.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        return "Successfully saved the Join Delta Table"

    except:
        print("Delta Table entered doesnt exists, Plese enter a valid Delta Table")
        return "Delta Table entered doesnt exists, Plese enter a valid Delta Table"


result = JoinDatasetsPandas("1", "PublicSchoolNan", "s01",
                      [{"inputStepId": "1", "inputResultId": "2"}, {"inputStepId": "3", "inputResultId": "1"}],
                      {"inputStepId": "1", "blendColumns": [
                          {"name": "LEVEL", "alias": "LEVEL"}, {"name": "NAME", "alias": "NAME"}]},
                      {"inputStepId": "3", "blendColumns": [
                          {"name": "LEVEL", "alias": "LEVEL1"}, {"name": "NAME", "alias": "NAME1"}]},
                      [{"leftColumn": "LEVEL", "rightColumn": "LEVEL"}, {"leftColumn": "NAME", "rightColumn": "NAME"}],
                      "999", "outer")
print(result)