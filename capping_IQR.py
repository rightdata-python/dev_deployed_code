import pandas as pd
from sklearn.preprocessing import OrdinalEncoder
import numpy as np
from pyspark.sql.functions import lit, rand, when
import pyspark
import json
from properties import path
from deltalake import DeltaTable
#path = "C:/Users/kisho/PycharmProjects/DataWrangling/Sampling_DT"
from pyspark.sql.functions import lit, col,isnan, when, count, mean as _mean, stddev as _stddev
spark = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0") \
     .config("spark.sql.warehouse.dir", "/home/ubuntu/warehouse/DELTA_WAREHOUSE")\
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
     .config("spark.databricks.delta.optimizeWrite.enabled", "true")\
     .config("spark.databricks.delta.retentionDurationCheck.enabled", "false")\
     .enableHiveSupport() \
     .getOrCreate()
print("spark session Started")

def iqr_for_all_columns(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnvalues):
    # col_vals = ["HP"]

    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    InputTable = path + "/" + inputDeltaTable
    print("Reading Delta Table")
    try:
        Data = spark.read.format("delta").load(InputTable)
        columnname = columnvalues[0]
        print("selected column:", columnname)
        df = Data.toPandas()
        df1 = df.copy()
        df = df._get_numeric_data()
        columnvalues = []
        # q1 = df[columnname].quantile(0.25)
        # q3 = df[columnname].quantile(0.75)
        q1 = df.quantile(0.25)
        q3 = df.quantile(0.75)

        iqr = q3 - q1

        lower_bound = q1 - (1.5 * iqr)
        upper_bound = q3 + (1.5 * iqr)

        print(lower_bound)
        print(upper_bound)
        # df_no_outlier = iqr[(iqr.col_vals>lower_bound)&(iqr.col_vals<upper_bound)]
        # df.loc[df[columnname] < lower_bound, columnname] = lower_bound
        # df.loc[df[columnname] > upper_bound, columnname] = upper_bound
        for col in columnvalues:
            for i in range(0, len(df[col])):
                if df[col][i] < lower_bound[col]:
                    df[col][i] = lower_bound[col]

                if df[col][i] > upper_bound[col]:
                    df[col][i] = upper_bound[col]

        for col in columnvalues:
            df1[col] = df[col]
        print("-------------------", df1)
        df3 = spark.createDataFrame(df1)
        df3.show()
        outputDeltaTablepath = path + "/" + outputDeltaTable
        df3.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        list = {"status": "Success", "message": "Successfully completed IQR capping for selected column",
                "data": {"inputsteps": Data.count(), "outputsteps": df3.count()}}
        return json.dumps(list)
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

def remove_outliers(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,column):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    print("Input Table:", inputDeltaTable)
    print("Output Table:", outputDeltaTable)
    InputTable = path + "/" + inputDeltaTable
    print("Reading Delta Table")
    try:
        Data = spark.read.format("delta").load(InputTable)
        df = Data.toPandas()
        df = df.copy()
        #df = df._get_numeric_data()
        print(df.columns)
        q1 = df[column].quantile(0.25)
        q3 = df[column].quantile(0.75)
        iqr = q3 - q1
        bha = df[column] >= q1 - 1.5 * iqr
        print("********************************")
        print(bha)
        vis = df[column] <= q3 + 1.5 * iqr
        print("vis",vis)
        # Apply filter with respect to IQR, including optional whiskers
        filter = (df[column] >= q1 - 1.5 * iqr) & (df[column] <= q3 + 1.5 * iqr)
        print("filter:",filter)
        df2 = df.loc[filter]
        print("finaldf:",df2)
        df1 = spark.createDataFrame(df2)
        df1.show()
        outputDeltaTablepath = path + "/" + outputDeltaTable
        print(outputDeltaTablepath)
        df1.write.format("delta").mode("overwrite").option("overwriteSchema", "true").save(outputDeltaTablepath)
        print("saved in given location")
        list = {"status": "Success", "message": "Successfully removed outliers in selected column",
                "data": {"inputsteps": Data.count(), "outputsteps": df1.count()}}
        print(list)
        return json.dumps(list)
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}  # "Delta Table entered doesnt exists, Plese enter a valid Delta Table"}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)
    
def display_outliers(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,column):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
    print("Input Table:", inputDeltaTable)
    InputTable = path + "/" + inputDeltaTable
    print("Reading Delta Table")
    try:
        Data = spark.read.format("delta").load(InputTable)
        df = Data.toPandas()
        df = df.copy()
        df = df._get_numeric_data()
        q1 = df[column].quantile(0.25)
        q3 = df[column].quantile(0.75)
        iqr = q3 - q1
        bha = df[column] >= q1 - 1.5 * iqr
        print("********************************")
        print(bha)
        vis = df[column] <= q3 + 1.5 * iqr
        print(vis)
        display = df[np.logical_or(df[column] < (q1 - 1.5 * iqr), df[column] > (q3 + 1.5 * iqr))]
        print("=====================================")
        outliercount = len(display)
        print(outliercount)
        list = {"status": "Success", "message": "Count of outliers is completed",
                "data": outliercount}
        # json_object = json.dumps(list, indent=2, sort_keys=True)
        return json.dumps(list)
    except:
        FailureMessage = {"status": "Failure",
                          "message": "Something went wrong, Please check the inputs."}
        FailureMessageJson = json.dumps(FailureMessage)
        print(FailureMessageJson)
        return json.dumps(FailureMessage)

#iqr_for_all_columns("1","newPSNAN","s01","3", "4","1", ["ENROLLMENT"])

################################Percentile_Outlier#########################################

def Cap_Percentile_Outlier(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnname,maxthreshold,minthreshold):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Tablename--->",inputDeltaTable)
    print("Tablename--->",inputDeltaTablepath)
    print("Reading data")
    try:
        #df = spark.read.format("delta").load(DeltaTable)
        df = DeltaTable(inputDeltaTablepath).to_pandas()
        # columnNames = Data.columns
        print("No of Columns and Column names in Delta table: ", len(df.columns),";",df.columns)
        print("No. of records in Data:", len(df))
        print(df)
        print("Datatype of column:",df.dtypes[columnname])
        df.head()
        #for col in columnname:
        lower_bound = df[columnname].quantile(0.05)
        print(lower_bound)
        upper_bound = df[columnname].quantile(0.95)
        print(upper_bound)
        df[columnname] = np.where(df[columnname]>upper_bound,upper_bound,np.where(df[columnname]<lower_bound,lower_bound,df[columnname]))
        #sun = df.to_csv(outputfile)
        print(df)
        result = df.to_json(orient="split")
        parsed = json.loads(result)
        json.dumps(parsed, indent=4)
        print(parsed)
        list = {"status": "Success", "message": "Count of outliers is completed",
                "data": parsed}
        # json_object = json.dumps(list, indent=2, sort_keys=True)
        return json.dumps(list)
    except OSError as err:
        print("OS error: {0}".format(err))
        

def Remove_Percentile_Outlier(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,columnname,maxthreshold,minthreshold):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Reading data")
    try:
        #df = spark.read.format("delta").load(DeltaTable)
        df = DeltaTable(inputDeltaTablepath).to_pandas()
        # columnNames = Data.columns
        print("No of Columns and Column names in Delta table: ", len(df.columns),";",df.columns)
        print("No. of records in Data:", len(df))
        print(df)
        print("Datatype of column:",df.dtypes[columnname])
        df.head()
        #qunatile will returns the value at the first quartile(q1) of the dataset data .
        max_thresold = df[columnname].quantile(maxthreshold)
        print(max_thresold)
        perc_outlier = df[df[columnname]>max_thresold]
        min_thresold = df[columnname].quantile(minthreshold)
        print(min_thresold)
        print(df[df[columnname]<min_thresold])
        print(perc_outlier)
        rmv_outliers = df[(df[columnname]<max_thresold) & (df[columnname]>min_thresold)]
        print("-----------------",rmv_outliers)
        list = {"status": "Success", "message": "Count of outliers is completed",
                "data": perc_outlier}
        # json_object = json.dumps(list, indent=2, sort_keys=True)
        return json.dumps(list)
    except OSError as err:
        print("OS error: {0}".format(err))
        
##############################################RuleBased##############################################
def np_encoder(object):
    if isinstance(object, np.generic):
        return object.item()
        
def Rulebased_Outlier(projectId,reciepeId,sessionId,inputStepId, currentStepId,inputResultId,lowerbound, upperbound, columnnames):
    if sessionId == None:
        inputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + inputStepId + "_" + inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId + "_" + currentStepId + "_1"
    else:
        inputDeltaTable = "R_"+str(projectId)+"_"+reciepeId+"_"+sessionId+"_"+inputStepId+"_"+inputResultId
        outputDeltaTable = "R_"+str(projectId) + "_" + reciepeId+"_"+sessionId + "_" + currentStepId + "_1"
    inputDeltaTablepath = path + "/" + inputDeltaTable
    print("Reading data")
    try:
        #df = spark.read.format("delta").load(DeltaTable)
        df = DeltaTable(inputDeltaTablepath).to_pandas()
        # columnNames = Data.columns
        print("No of Columns and Column names in Delta table: ", len(df.columns),";",df.columns)
        print("No. of records in Data:", len(df))
        print(df)
        print("Datatype of column:",df.dtypes[columnnames])
        #df = df[columnnames].astype(float)
        #print("------------------------------Datatype of column:----------------->",df.dtypes[columnnames])
        df.head()
        df[columnnames] = df[columnnames].clip(lower=int(lowerbound), upper=int(upperbound))

        print(df)
        list = {"status": "Success", "message": "Count of outliers is completed",
                "data": df}
        # json_object = json.dumps(list, indent=2, sort_keys=True)
        return json.dumps(list,default=np_encoder)
    except OSError as err:
        print("OS error: {0}".format(err))